!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> Program to compute and save the gradient $\nabla u_h$ of P1Galerkin function 
!> given a potential $u_h$ and a grid $\tau_h$.
!> 
!>
!> REVISION HISTORY:
!> 20180326 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM eval_gradient
  use Globals
  use AbstractGeometry
  use P1Galerkin
  use TimeInputs
  use TimeOutputs
  implicit none

  type(file) :: fgrid
  type(file) :: fpot
  type(file) :: fgrad    
  type(abs_simplex_mesh) :: grid
  type(p1gal):: p1
  type(TimeData) :: pot
  type(TDOut)  :: grad

  logical  :: rc,endfile1,endreading
  integer  :: res
  character(len=256) :: input,path_grid, path_pot, path_grad

  integer :: stderr,stdout,debug , narg
  integer :: i,j, itria,ninputs,dim,ndata,itime

  real(kind=double) :: time

  stderr=6
  stdout=6


  !>----------------------------------------------------------------------------
  
  !>----------------------------------------------------------------------------
  !> Geometry info
  !> Read original grid
  ! no renumbering
  ! no connection of second level
  call getarg(1,input)
  read(input,'(a)') path_grid 
  narg=1
  call  get_command_argument(narg, path_grid,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  narg=narg+1
  call  get_command_argument(narg, path_pot,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)


  narg=narg+1
  call  get_command_argument(narg, path_grad,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  


  !
  ! open files
  !
  call fgrid%init(stderr,etb(path_grid),10,'in')
  call fpot%init(stderr,etb(path_pot),11,'in')
  
  !
  ! init/read grid
  !
  call grid%read_mesh(stderr,fgrid)
  call grid%build_normal_cell(stderr)


  !
  ! init/ p1
  !
  call p1%init(stderr,grid)
  
  
  !
  ! int pot varaible
  !
  call pot%init(stderr, fpot, 1, grid%nnode)

  !
  ! init grad and associated file
  !
  call fgrad%init(stderr,etb(path_grad),13,'out')
  dim = 3
  ndata = grid%ncell
  call grad%init(stderr,dim , ndata)
  write(fgrad%lun,*) dim,grad%ndata,' ! dim data' 
  
  !
  ! read data and write grad
  !
  endreading=.false.
  time  = pot%TDtime(1)
  itime = 0
  do while ( .not. endreading )
     !
     ! read potential
     !
     call pot%set(stderr, fpot, time, endfile1)

     !
     ! compute gradient
     !
     call p1%eval_grad(pot%TDactual(1,:), grad%TDactual(:,:))
     grad%time=time
          
     !
     ! if both file are in steady state 
     !
     if ( pot%steadyTD ) then
        endreading=.True.
        grad%steadyTD=.True.
     end if
     !
     ! write rhs_subgrid_integrated
     ! 
     call grad%write2dat(fgrad%lun)     

     !
     ! exit if one file reached the there is no time exit
     !
     
     if ( endfile1 ) then
        exit
     end if

     time = pot%TDtime(2)
   end do


   call pot%kill(stderr)
   call grad%kill(stderr)

  call p1%kill(stderr)
  call grid%kill(stderr)
  call fgrid%kill(stderr)
  call fpot%kill(stderr)
  call fgrad%kill(stderr)


end PROGRAM eval_gradient

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./eval_gradient.out <grid> <p1data> <gradient>'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid     (in ) : triangulation in ascii'
  write(lun_err,*) ' p1data   (in ) : piecewise-linear data defined on node grid'
  write(lun_err,*) ' gradient (out) : piecewise constant gradient of p1data'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
