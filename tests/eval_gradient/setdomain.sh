#!/bin/bash

domain=$1

if [ "$domain" == "2d" ] || [ "$domain" == "3d" ] || [ "$domain" == "Surface" ] ;
then
    sed -i "s|use Geometry.*|use Geometry$domain|g" *.f90
    sed -i "s|use P1Galerkin.*|use P1Galerkin$domain|g" *.f90
else
    echo "Not admissible Domain"
    echo "Use 2d, 3d or Surface"
    echo "Passed" $domain
fi
