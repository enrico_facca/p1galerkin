!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> Program to compute error in the divergence constrin
!> div(velocity)=forcing
!> 
!>
!> REVISION HISTORY:
!> 20180326 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM eval_divergence_constrain
  use Globals
  use AbstractGeometry
  use P1Galerkin
  use TimeInputs
  use TimeOutputs
  implicit none

  ! input files
  type(file) :: fgrid
  type(file) :: fvel
  type(file) :: frhs
  !
  type(abs_simplex_mesh) :: grid
  type(TimeData) :: vel
  type(TimeData) :: rhs
  !
  type(p1gal):: p1
  
  

  logical  :: rc,endfile1,endfile2,endreading
  integer  :: res
  character(len=256) :: input,path_grid, path_vel, path_rhs

  integer :: stderr,stdout,debug , narg
  integer :: i,j, inode, iloc,icell,ninputs,dim,ndata,itime

  real(kind=double) :: time,g_p1(3),v_cell(3)
  real(kind=double) :: ddot, dnrm2
  real(kind=double),allocatable  :: difference(:)

  

  stderr=6
  stdout=6


  !>----------------------------------------------------------------------------
  
  !>----------------------------------------------------------------------------
  !> Geometry info
  !> Read original grid
  ! no renumbering
  ! no connection of second level
  call getarg(1,input)
  read(input,'(a)') path_grid 
  narg=1
  call  get_command_argument(narg, path_grid,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  narg=narg+1
  call  get_command_argument(narg, path_vel,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)


  narg=narg+1
  call  get_command_argument(narg, path_rhs,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  
  !
  ! init/read grid
  !
  call fgrid%init(stderr,etb(path_grid),10,'in')
  call grid%read_mesh(stderr,fgrid)
  call grid%build_size_cell(stderr)
  call grid%build_normal_cell(stderr)



  !
  ! open files
  !
  call fvel%init(stderr,etb(path_vel),11,'in')
  call frhs%init(stderr,etb(path_rhs),10,'in')
  
  !
  ! init inputs daat
  !
  call vel%init(stderr, fvel, 3, grid%ncell)
  call rhs%init(stderr, frhs, 1, grid%nnode)
  allocate(difference(grid%nnode),stat=res)
  if ( res .ne. 0) rc = IOerr(stderr, err_alloc, 'main', &
       ' work array difference',res)

  !
  ! init/ p1
  !
  call p1%init(stderr,grid)
    
  !
  ! read data and write diff
  !
  endreading=.false.
  time  = max(vel%TDtime(1),rhs%TDtime(1))
  itime = 0
  do while ( .not. endreading )
     !
     ! read potential
     !
     call vel%set(stderr, fvel, time, endfile1)
     call rhs%set(stderr, frhs, time, endfile2)

     !
     ! compute diffient
     !
     difference = zero
     do icell = 1, grid%ncell
        v_cell=vel%TDactual(:,icell)
        
        do iloc =  1, grid%nnodeincell
           inode = grid%topol(iloc,icell) 
           call p1%get_gradbase(iloc,icell,g_p1)
           difference(inode) = difference(inode) + &
                ddot(3,g_p1,1,v_cell,1)*grid%size_cell(icell) 
        end do
     end do
     difference = difference + rhs%TDactual(1,:)
     write(*,*) time, dnrm2(grid%nnode,difference,1)

     !
     ! if both file are in steady state 
     !
     if ( ( vel%steadyTD ) .and. (rhs%steadyTD) ) then
        endreading=.True.
     end if
        

     !
     ! exit if one file reached the there is no time exit
     !
     
     if ( ( endfile1 ) .or. ( endfile2 ) ) then
        exit
     end if

     time = min(vel%TDtime(2),rhs%TDtime(2))
   end do

   deallocate(difference,stat=res)
   if ( res .ne. 0) rc = IOerr(stderr, err_dealloc, 'main', &
        ' work array difference',res)


   call vel%kill(stderr)
   call rhs%kill(stderr)

   call p1%kill(stderr)
   call grid%kill(stderr)
   call fgrid%kill(stderr)
   call fvel%kill(stderr)
   call frhs%kill(stderr)
  

end PROGRAM eval_divergence_constrain

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./eval_divergence_constrain.out <grid> <velocity> <rhs>'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid     (in ) : triangulation in ascii'
  write(lun_err,*) ' velocity (in ) : 3, ncell velocity field'
  write(lun_err,*) ' rhs      (in)  : data with rhs integrated w.r.t to p1-basis functions'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
