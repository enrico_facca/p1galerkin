!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> The main program compute the integral w.r.t. to the base functions of
!> the P1-Galerkin basis.
!> INPUT: 
!>  grid ( 2d, 3d, surface)   
!>  forcing term ( timedata data defined on grid cell)
!> OUTPUT:
!>  rhs_integrated ( time-data defiend on grid nodes such that)
!>  
!>  rhs(i) = int_{\Domain} forcing \Psi_i 
!>  
!>  where \Psi_i is the base function associated to the node i
!>
!> REVISION HISTORY:
!> 18 June 2019 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM forcing2rhs
  use Globals
  use AbstractGeometry
  use TimeInputs
  use TimeOutputs
  
  implicit none
  character(len=256) :: file_grid, file_forcing, file_rhs
  type(file) :: fgrid, fforcing, frhs
  
  type(abs_simplex_mesh)     :: grid
  type(TimeData) :: forcing
  type(TDOut)    :: rhs
  

  integer  :: nref
  logical  :: rc
  integer  :: res


  integer :: stderr,stdout,debug !,res
  integer :: i, icell
  integer :: ngrids

  real(kind=double) :: time,imbalance
  real(kind=double),allocatable :: size_node(:),ones(:)

  logical :: steady
  logical :: end_forcing

  stderr=6
  stdout=6

  !
  ! get path
  !
  
  call get_command_argument(1,file_grid,status=res)
  if ( res .ne. 0) then
     call print_help_message(stderr)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument grid wrong res= ',res)
  end if

  call get_command_argument(2,file_forcing,status=res)
  if ( res .ne. 0) then
     call print_help_message(stderr)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument cell_data wrong res= ',res)
  end if

  call get_command_argument(3,file_rhs,status=res)
  if ( res .ne. 0) then
     call print_help_message(stderr)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument rhs wrong res= ',res)
  end if


  !
  ! init I/O files
  !
  call fgrid%init(stderr,file_grid,10,'in')
  call fforcing%init(stderr,file_forcing,11,'in')
  call frhs%init(stderr,file_rhs,12,'out')
  
  ! read  grid
  ! no renumbering
  ! no connection of second level
  call grid%read_mesh(stderr,fgrid)
  !
  call grid%build_size_cell(stderr)
  
  allocate(ones(grid%ncell),size_node(grid%nnode))
  ones = one
  call assembly_rhs_grid_integrated(grid,&
          ones,&
          size_node)


  !
  ! init timedata forcing term
  ! 
  call forcing%init(stderr, fforcing, 1, grid%ncell)
  
  !
  ! init output rhs
  !
  call rhs%init(stderr, 1,grid%nnode)
  ! build
  end_forcing=.false.
  time=forcing%TDtime(1)
  steady=.false.
  write(frhs%lun,*) 1,grid%nnode,' ! dim data' 
  do while ( (time .eq. forcing%TDtime(1) ) .or. &
       ( (.not. steady ) .and. ( .not. end_forcing  )  ) ) 
     ! read inputs
     call forcing%set(stderr, fforcing,time,end_forcing)
     
     steady = forcing%steadyTD

     !
     ! assembly rhs grid
     !
     rhs%time=time
     call assembly_rhs_grid_integrated(grid,&
          forcing%TDactual,&
          rhs%TDactual)
     rhs%steadyTD=steady
     
     
     ! write rhs_subgrid_integrated
     call rhs%write2dat(frhs%lun)
     do i=1,grid%nnode
        write(555,*) rhs%TDactual(1,i)
     end do     

     ! next time
     time=forcing%TDtime(2)
  end do

    
  call forcing%kill(stderr)
  !call grid%kill(stderr)

  !
  ! init I/O files
  !
  call fgrid%kill(stderr)
  call fforcing%kill(stderr)
  call frhs%kill(stderr)

contains
    subroutine print_help_message(lun)
      implicit none
      integer, intent(in) :: lun
      !local
      character(len=256) :: msg
      msg=' Usage: ./forcing2rhs.out grid data rhs '
      write(*,*) etb(msg )
      
      msg=' - grid    : grid  wheredata is defined '
      write(*,*) etb(msg)
      
      msg=' - data    : time varying data '
      write(*,*) etb(msg) 
      
      msg=' - rhs     : integral w.r.t. p1 basis funcitons'     
      write(*,*) etb(msg) 
    end subroutine print_help_message


end PROGRAM forcing2rhs


!>---------------------------------------------------------
!> Assembly rhs_subgrid of elliptic equation given forcing and Neumann terms
!>----------------------------------------------------------
subroutine assembly_rhs_grid_integrated(grid,&
     forcing,rhs_grid_forcing)
    use Globals
    use AbstractGeometry
    implicit none
    type(abs_simplex_mesh), intent(in ) :: grid
    real(kind=double),      intent(in ) :: forcing(grid%ncell)
    real(kind=double),      intent(out) :: rhs_grid_forcing(grid%nnode)

    !local 
    integer :: inode, icell, iloc
    real(kind=double) :: factor

    factor=one/grid%nnodeincell

    rhs_grid_forcing  = zero
    do icell = 1, grid%ncell
       do iloc = 1,grid%nnodeincell
          inode = grid%topol(iloc,icell)
          rhs_grid_forcing(inode) =  rhs_grid_forcing(inode) + &
               factor * ( forcing(icell) ) * &
               grid%size_cell(icell)
       end do
    end do
  end subroutine assembly_rhs_grid_integrated
