!>---------------------------------------------------------------------
!> 
!> Convergence test for $-\Delta u = f(x)$ on the unit square
!> with $f(x,y)=-5/4 \pi^2 \sin(\pi x) \cos(\pi/2 \; y)$
!> zero Dirichlet conditions. The solution is:
!> $u(x,y) = \sin(\pi x) \cos(\frac{\pi}{2}y)$
!> 
!> starts from a simple grid of 9 nodes and 8 triangles and then
!> refines nref times.
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> The main program 
!> 
!>
!> REVISION HISTORY:
!> 20190327 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program laplace
  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  use Eigenv
  use BackSlashSolver
  use DenseMatrix
  
  use TimeInputs, only : write_steady
    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       stdin=>input_unit, &
       stdout=>output_unit, &
       stderr=>error_unit  
  implicit none
  
  type(file)     :: fgrid,fgridout,fctrl,fout
  type(abs_simplex_mesh)     :: grid0,grid,subgrid

  type(p1gal)    :: lapl_p1
  type( agmg_inv) :: multigrid_inverse

  ! 
  real(kind=double), allocatable :: diffusion_coeff(:),rhs(:),sol(:)
  
  ! drichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  !  p1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) ,gradsol(:,:)

  !  exact solution and its  gradient
  real(kind=double), allocatable :: pot(:), gradpot(:,:)

  !  error pot and its gradient
  real(kind=double), allocatable :: err_pot(:), err_gradpot(:,:), err_nrmgradpot(:)

  real(kind=double),allocatable :: eigenvalues(:),eigenvectors(:,:)
  real(kind=double),allocatable :: inverse_eigenvalues(:),inverse_eigenvectors(:,:)
    
  
  ! errror norm
  real(kind=double)  :: l2err_pot, l2err_gradpot ,l2err_pot_before, l2err_gradpot_before 

  ! euclidian norm
  real(kind=double) :: dnrm2,ddot

  character(len=256) :: input,fngrid,fnctrl,fnctrl_in,fname,tail


  type(scalmat),target :: identity
  type(spmat)    :: stiff_matrix
  type(input_solver) :: ctrl
  type(output_solver):: info,info1
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff
  type(inv), target :: iterative_solver
  type(densemat), target :: ones
  type(TransposeMatrix), target :: onesT
  type(pair_linop), target :: WWT
  type(pair_linop), target :: pseudo_inverse
  type(pair_linop), target :: orthogonal_projector
  type(array_linop) :: list(3)
  real(kind=double) :: alphas(3)
  
  
  ! local vars
  logical :: rc
  integer :: res,nterm,ndof,i,j,inod,ndir,infoin
  integer :: nnode, ncell
  integer :: nref
  integer :: igrid,id_test_read,id_test

  !
  ! read number of refinements
  !
  CALL getarg(1, input)
  read(input,*,iostat=res) nref
  if (res.ne.0) nref=0

  !
  ! read id_test
  !
  id_test=1
  CALL getarg(2, input)
  read(input,*,iostat=res) fngrid
 

  !
  ! read linear solver controls
  !
  fnctrl='inputs/linear_solver.ctrl'
!!$  CALL getarg(3, input)
!!$  read(input,'(a)',iostat=res) fnctrl_in 
!!$  if (res.ne.0) then
!!$     fnctrl=fnctrl_in
!!$  end if
  call fctrl%init(stderr,etb(fnctrl),8,'in')
  call ctrl%read(stderr,fctrl)
  read(fctrl%lun,*) ctrl%approach 
  call fctrl%kill(stderr)
  
  write(stdout,*) '*****************TEST CASE ***********************'
  write(stdout,*) 'id_test    = ', id_test
  write(stdout,*) etb(example_description(id_test))
  write(stdout,*) 'total nref =',nref
  write(stdout,*) '**************************************************'

  write(stdout,*) '*************INFO LINEAR SOLVER ******************'
  call ctrl%info(0)
  write(stdout,*) '**************************************************'
  
  
  call fgrid%init(6,etb(fngrid),20,'in')
  call fgrid%info(0)
  call grid0%read_mesh(0, fgrid)
  call grid0%build_size_cell(0)
  call grid0%build_normal_cell(0)
  call grid0%build_edge_connection(0)
  call grid0%build_nodebc()
  call grid0%build_bar_cell()
  call grid0%info(0)

  grid=grid0
  write(*,*) 'nref|        mesh par      l2err_pot  l2err_gradpot '
  
  do igrid = 1, nref+1
     write(*,*) ' ref $ ',igrid,' nnode= ',grid%nnode, ' ncell =', grid%ncell
     write(tail,'(a,I0.3,a)') 'ref',igrid-1,'.dat'
     fname=etb('grid'//etb(tail))

!!$     write(*,*) ' write grid'
!!$     call fgridout%init(stderr,fname,8,'out')
!!$     call grid%write_mesh(stderr, fgridout)
!!$     call fgridout%kill(stderr)

     nnode=grid%nnode
     ncell=grid%ncell

     allocate(diffusion_coeff(ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'array diffusion_coeff',res)
     ! give a value to cond
     diffusion_coeff=one

     ! p1 galerkin for laplace
     write(*,*) ' build_p1'
     call lapl_p1%init(6, grid)

     nterm = lapl_p1%nterm_csr
     ndof = lapl_p1%grid%nnode

     call stiff_matrix%init(6,&
          ndof, ndof, nterm,&
          storage_system='csr',&
          is_symmetric=.true.)


     !
     ! allocate space for rhs, sol, gradsol
     !
     allocate(rhs(ndof),sol(ndof),temp(ndof),gradsol(grid%logical_dimension,ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays rhs,sol,gradsol ',res)


     
     write(*,*) ' build stiff'
     call lapl_p1%build_stiff(6, 'csr', diffusion_coeff, stiff_matrix)

     allocate(eigenvalues(4),eigenvectors(ndof,4),&
          inverse_eigenvalues(4),inverse_eigenvectors(ndof,4))
     call arpack_wrap( stiff_matrix,6,&
           4,min(100,ndof),'LM', 300,1d-12,&
           eigenvalues, eigenvectors,&
           i)
     write(*,*) 'eigvalues:=',eigenvalues(1:4)
     write(*,*) eigenvectors(1:2,1)
     write(*,*) eigenvectors(ndof-2:ndof,1)

     ctrl%tol_sol=1e-16
     ctrl%approach='ITERATIVE'
     ctrl%scheme='PCG'
     write(*,*) 'inverse starts'
     call ones%init(6,&
          ndof, 1,&
          ! optional arguments
          is_symmetric=.False.)
     ones%coeff=one
     ones%coeff=ones%coeff/&
          dnrm2(ndof,&
          ones%coeff(1:ndof,1),1)
     

     call onesT%init(ones)
     ones%name='W'

     onesT%name='WT'
     call identity%eye(ndof)
     
     
     list(1)%linop => ones 
     list(2)%linop => onesT
     call WWT%init(infoin,6, 2, list,'LP')
     call WWT%info(6)
     WWT%name='WWT'


     alphas(1)=one
     list(1)%linop => identity 

     alphas(2)=-one/&
          ddot(ndof,&
          ones%coeff(1:ndof,1),1,&
          ones%coeff(1:ndof,1),1)
     list(2)%linop => WWT
     !write(*,*) alphas(2),ndof/2
     call orthogonal_projector%init(infoin,6,2,list,'LC',alphas)
     orthogonal_projector%name='P'


     rhs=zero
     do i=1,ndof/2
        rhs(i) = one
     end do
     do i=1+ndof/2,2*(ndof/2)
        rhs(i) = -one
     end do
     write(*,*) 'scal',ddot(ndof, rhs,1,ones%coeff(1:ndof,1),1)
     
     !sol=zero
     !call onesT%Mxv(rhs,alphas(1))
     !write(*,*) 'sol(1:oneT%ncol)',onesT%nrow,sol(1:onesT%nrow)

     !call ones%Mxv(alphas(1),sol)
     !write(*,*) 'nrom sol', dnrm2(ndof,sol,1)

!!$     rhs=zero
!!$     do i=1,ndof/2
!!$        rhs(i) = one
!!$     end do
!!$     do i=1+ndof/2,2*(ndof/2)
!!$        rhs(i) = -one
!!$     end do
!!$     
!!$
!!$     call  WWT%Mxv(rhs,sol)
!!$     write(*,*) 'difff',  sol(1),sol(ndof)


!!$     do i=1,30
!!$        call random_number(rhs)
!!$        call  orthogonal_projector%Mxv(rhs,sol)
!!$        call  orthogonal_projector%Mxv(sol,temp)
!!$        write(*,*) 'scal',ddot(ndof, sol,1,ones%coeff(1:ndof,1),1)
!!$     end do
!!$     write(*,*) 'difff', dnrm2(ndof,rhs-sol,1)
     
     ctrl%tol_sol=1e-14
     ctrl%print_info_solver = .False.
     call iterative_solver%init( stiff_matrix,ctrl,&
          ortogonalization_matrix=WWT)
     iterative_solver%is_symmetric=.True.
     iterative_solver%name='invM'
     call iterative_solver%info(6)

     list(1)%linop => orthogonal_projector 
     list(2)%linop => iterative_solver
     list(3)%linop => orthogonal_projector

     
     call pseudo_inverse%init(infoin,6, 3, list,'LP')
     pseudo_inverse%name='pinvM'

     call arpack_wrap( &
          pseudo_inverse ,6,&
          !iterative_solver ,6,&
           4,min(30,ndof),'SM', 200,1d-12,&
           inverse_eigenvalues, inverse_eigenvectors,&
           i)
     write(*,*) 'inverse', i
     write(*,*) one/inverse_eigenvalues(1:4)
     write(*,*) eigenvalues(1:4)
     write(*,*) dnrm2(ndof,eigenvectors(1:ndof,4)-inverse_eigenvectors(1:ndof,2),1)
     write(*,*) dnrm2(ndof,eigenvectors(1:ndof,3)-inverse_eigenvectors(1:ndof,3),1)
     write(*,*) dnrm2(ndof,eigenvectors(1:ndof,2)-inverse_eigenvectors(1:ndof,4),1)
     deallocate(eigenvalues,eigenvectors,inverse_eigenvectors,inverse_eigenvalues)


!!$     write(*,*) ' write stiff'
!!$     open(9,file='stiffness_matrix.out')
!!$     call stiff_matrix%write(9)
!!$     close(9)



     rhs=zero
     sol=one
     gradsol=zero
     !
     ! create rhs $rhs(i)=\int f \phi_i$
     !
     do i=1,ndof
        rhs(i) = lapl_p1%basis_integral(i)* &
             forcing(id_test,grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
     end do
     
     !
     ! set dirichlet boundary conditions
     !
     ndir=grid%nnode_bc
     allocate(noddir(ndir),soldir(ndir),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays noddir soldir',res)
     noddir=grid%node_bc
     do i=1,grid%nnode_bc
        j = grid%node_bc(i)
        soldir(i) = pot_exact(id_test, grid%coord(1,j),grid%coord(2,j),grid%coord(3,j))
     end do

     !
     ! preprocess stiffness_matrix rhs and solution 
     ! to obtain dirichlet boundary condition
     !
     call lapl_p1%dirichlet_bc(stderr,&
          stiff_matrix,rhs,sol,&
          ndir,noddir,soldir)
     sol=zero

     open(9,file='stiffness_correct.')
     call stiff_matrix%write(9)
     close(9)


     open(9,file='rhs.out')
     do i=1,nnode
        write(9,*) i, rhs(i)
     end do
     close(9)

     write(*,*) ' solve linear system',etb(ctrl%approach)
     sol=zero

     select case (ctrl%approach)
     case('MG') 
        call multigrid_inverse%init(stderr,stiff_matrix,ctrl)
        call multigrid_inverse%Mxv(rhs,sol)
        call multigrid_inverse%info_solver%info(6)
        call multigrid_inverse%kill(stderr)
     case ('ITERATIVE')
        !write(*,*) 'call sum'
        stiff_matrix%name='A'
        !call sum1%info(6)
        !write(*,*) sum1%type,sum1%is_symmetric
        call ctrl_prec%init(stderr,'IC',n_fillin=30,tol_fillin=1d-3)
        call prec_stiff%init(stderr, i,ctrl_prec,ndof, stiff_matrix)
        call info%init()
        call linear_solver(stiff_matrix,rhs,sol,info,ctrl,prec_stiff)
        call ctrl_prec%kill()
        call prec_stiff%kill(stderr)
        call info%info(stdout)
        call info%time(stdout)
        call info%kill()

     end select
     
     write(*,*) ' write solution'
     fname=etb('pot'//etb(tail))
     call fout%init(0,fname,10000,'out')
     call write_steady(0, 10000, ndof, sol)
     call fout%kill(0)
    
     !
     ! allocate space for reference solution and work arrays 
     !
     allocate(&
          pot(ndof),&
          gradpot(grid%logical_dimension,ncell),&
          err_pot(ndof),&
          err_gradpot(grid%logical_dimension,ncell),&
          err_nrmgradpot(ncell),&
          stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays pot gradpot',res)

     call stiff_matrix%mxv(sol,pot)
     pot=pot-rhs

     !
     ! compute gradient of the solution 
     ! exact solution its gradient
     ! errors arrays
     !
     call lapl_p1%eval_grad(sol,gradsol)
     do i=1,ndof
        pot(i) = pot_exact(id_test, grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
     end do
     do i=1,ncell
        call gradpot_exact(id_test,&
             grid%bar_cell(1,i),&
             grid%bar_cell(2,i),&
             grid%bar_cell(3,i),&
             grid%logical_dimension, gradpot(1:grid%logical_dimension,i))
     end do
     fname=etb('exact_pot'//etb(tail))
     call fout%init(0,fname,10000,'out')
     call write_steady(0, 10000, ndof, pot)
     call fout%kill(0)

     
     err_pot=sol-pot
     err_gradpot=gradsol-gradpot
     do i=1,grid%ncell
        err_nrmgradpot(i)=dnrm2(grid%logical_dimension,err_gradpot(1:grid%logical_dimension,i),1)
     end do

     !
     ! compute l2 and print error w.r.t exact solution 
     !
     if (igrid >1 ) then
        l2err_pot_before = l2err_pot 
        l2err_gradpot_before = l2err_gradpot
     end if
     err_pot=err_pot**2
     l2err_pot=sqrt(ddot(ndof, lapl_p1%basis_integral, 1, err_pot,1))
     l2err_gradpot=grid%normp_cell(2.0d0,err_nrmgradpot)
     write(stdout,'(I4,a,3(1pe15.5))') igrid-1, ' | ',grid%meshpar(0), &
          l2err_pot, l2err_gradpot

     if (igrid >1 ) then
        write(stdout,'(I4,a,3(1pe15.5))') igrid-1, ' | ',grid%meshpar(0), &
          one/(l2err_pot/l2err_pot_before), one/(l2err_gradpot/l2err_gradpot_before)
     end if

     !
     ! free memory 
     !
     deallocate(noddir,soldir,stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays noddir soldir',res)

     deallocate(&
          temp,&
          pot,&
          gradpot,&
          err_pot,&
          err_gradpot,&
          err_nrmgradpot,&
          stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays pot gradpot',res)

     deallocate(diffusion_coeff,rhs,sol,gradsol,stat=res)
     if(res .ne. 0) rc = IOerr(6, err_dealloc , 'Laplace main',&
          'arrays diffusion_coeff, rhs, sol',res)
     call stiff_matrix%kill(6)
     call lapl_p1%kill(6)

     if (igrid<nref + 1) then
        write(*,*) ' build refinement'
        call subgrid%refine(stderr,grid)!flag_reorder=1)
        call subgrid%RCM_node_reorder(stderr)
        call subgrid%build_size_cell(0)
        call subgrid%build_normal_cell(0)
        call subgrid%build_edge_connection(0)
        call subgrid%build_nodebc()
        call subgrid%build_bar_cell()

!!$        call fgridout%init(stderr,'subgrid.out',8,'out')
!!$        call grid%write_mesh(stderr, fgridout)
!!$        call fgridout%kill(stderr)
     
        write(*,*) ' subgrid kill'
        grid=subgrid
        call subgrid%kill(stderr)
     end if
  end do
  call grid%kill(6)

  call grid0%kill(6)
  call fgrid%kill(6)

contains

  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if(id_test.eq.1) str='Rectangle [0,1][0,1] with zero dirichlet bc'
  
  end function example_description

  function forcing(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    if(id_test.eq.1) func=pigreco**2 * sin(pigreco*x) * cos(pigreco*y*onehalf)*5.0d0*onefourth
  
  end function forcing

  function pot_exact(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    if(id_test.eq.1) func=sin(pigreco*x) * cos(pigreco*y*onehalf)

  end function pot_exact

  subroutine gradpot_exact(id_test,x,y,z,logical_dimension,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    integer,           intent(in):: logical_dimension
    real(kind=double), intent(out) :: func(logical_dimension)

    if ( id_test .eq. 1 ) then
       func(1)=pigreco*cos(pigreco*x) * cos(pigreco*y*onehalf)
       func(2)=-pigreco*onehalf*sin(pigreco*x) * sin(pigreco*y*onehalf)
       if (logical_dimension .eq. 3 ) func(3)=zero
    end if

  end subroutine gradpot_exact
  
  
end PROGRAM laplace






