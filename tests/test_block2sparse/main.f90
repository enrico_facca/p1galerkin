!>---------------------------------------------------------------------
!> 
!> Convergence test for $-\Delta u = f(x)$ on the unit square
!> with $f(x,y)=-5/4 \pi^2 \sin(\pi x) \cos(\pi/2 \; y)$
!> zero Dirichlet conditions. The solution is:
!> $u(x,y) = \sin(\pi x) \cos(\frac{\pi}{2}y)$
!> 
!> starts from a simple grid of 9 nodes and 8 triangles and then
!> refines nref times.
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> The main program 
!> 
!>
!> REVISION HISTORY:
!> 20190327 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program laplace
  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  
  use TimeInputs, only : write_steady
    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       stdin=>input_unit, &
       stdout=>output_unit, &
       stderr=>error_unit  
  implicit none
  
  type(file)     :: fgrid,fgridout,fctrl,fout
  type(abs_simplex_mesh)     :: grid,subgrid

  type(p1gal)    :: lapl_p1
  type( agmg_inv) :: multigrid_inverse

  ! 
  real(kind=double), allocatable :: diffusion_coeff(:),rhs(:),sol(:)
  
  ! drichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  !  p1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) ,gradsol(:,:)

  !  exact solution and its  gradient
  real(kind=double), allocatable :: pot(:), gradpot(:,:)

  !  error pot and its gradient
  real(kind=double), allocatable :: err_pot(:), err_gradpot(:,:), err_nrmgradpot(:)

  ! errror norm
  real(kind=double)  :: l2err_pot, l2err_gradpot ,l2err_pot_before, l2err_gradpot_before 

  ! euclidian norm
  real(kind=double) :: dnrm2,ddot

  character(len=256) :: input,fngrid,fnctrl,fnctrl_in,fname,tail


  type(array_linop) :: list(6)
  integer ::  block_structure(3,6)
  type(block_linop) :: block_matrix
  type(spmat),target    :: stiff_matrix
  type(spmat),target    :: explicit_matrix
  
  type(scalmat),target :: zeromat1,zeromat2
  type(scalmat),target :: eye1,eye2
  type(diagmat) ,target :: diagonal
  type(input_solver) :: ctrl
  integer :: info
  type(output_solver):: info1
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! local vars
  logical :: rc
  integer :: res,nterm,ndof,i,j,inod,ndir
  integer :: nnode, ncell
  integer :: nref
  integer :: igrid,id_test_read,id_test

 
  !
  ! read id_test
  !
  id_test=1
  fngrid='grid0.dat'


  !
  ! read linear solver controls
  !  
  call fgrid%init(6,etb(fngrid),20,'in')
  call grid%read_mesh(0, fgrid)
  call fgrid%kill(6)
  call grid%build_size_cell(0)
  call grid%build_normal_cell(0)
  call grid%build_edge_connection(0)
  call grid%build_nodebc()
  call grid%build_bar_cell()
  call grid%info(0)
  call lapl_p1%init(6, grid)
  ncell=grid%ncell
  ndof = grid%nnode
  allocate(diffusion_coeff(grid%ncell),stat=res)
  if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
       'array diffusion_coeff',res)
  ! give a value to cond
  diffusion_coeff=one
  call lapl_p1%build_stiff(6, 'csr', diffusion_coeff, stiff_matrix)

  write(*,*) ndof
  call zeromat1%init(ndof,zero)
  call zeromat2%init(ndof,zero)
  call eye1%eye(ndof)
  call eye2%eye(ndof)
  call diagonal%init(6,ndof)
  diagonal%diagonal = two
  call diagonal%info(6)

  list(1)%linop => stiff_matrix
  list(2)%linop => stiff_matrix
  list(3)%linop => stiff_matrix
  list(4)%linop => eye1
  list(5)%linop => stiff_matrix

  
  block_structure(:,1) = (/1,1,1/)
  block_structure(:,2) = (/2,1,2/)
  block_structure(:,3) = (/3,1,3/)
  block_structure(:,4) = (/4,2,1/)
  block_structure(:,5) = (/5,2,3/)
  
  call block_matrix%init(stderr,&
       5,list,&
       2,3, &
       5, block_structure, &
       is_symmetric=.True.)

  call stiff_matrix%info(6)

  call fgrid%init(6,etb('stiff.dat'),20,'out')
  call stiff_matrix%write(20,'matlab')
  call fgrid%kill(6)
  
  call explicit_matrix%form_block(info,stderr,block_matrix)
  call fgrid%init(6,etb('block.dat'),20,'out')
  call explicit_matrix%write(20,'matlab')
  


     
  call grid%kill(6)

  call grid%kill(6)
  

contains

  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if(id_test.eq.1) str='Rectangle [0,1][0,1] with zero dirichlet bc'
  
  end function example_description

  function forcing(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    if(id_test.eq.1) func=pigreco**2 * sin(pigreco*x) * cos(pigreco*y*onehalf)*5.0d0*onefourth
  
  end function forcing

  function pot_exact(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    if(id_test.eq.1) func=sin(pigreco*x) * cos(pigreco*y*onehalf)

  end function pot_exact

  subroutine gradpot_exact(id_test,x,y,z,logical_dimension,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    integer,           intent(in):: logical_dimension
    real(kind=double), intent(out) :: func(logical_dimension)

    if ( id_test .eq. 1 ) then
       func(1)=pigreco*cos(pigreco*x) * cos(pigreco*y*onehalf)
       func(2)=-pigreco*onehalf*sin(pigreco*x) * sin(pigreco*y*onehalf)
       if (logical_dimension .eq. 3 ) func(3)=zero
    end if

  end subroutine gradpot_exact
  
  
end PROGRAM laplace






