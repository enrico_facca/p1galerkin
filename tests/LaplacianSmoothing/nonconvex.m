function [y] = nonconvex(x,order)
% a=0.25;
% b=-2/3;
% c=-5/2;
% d=6;
a=1;
b=-8/3;
c=-2;
d=8;
if (order==0) 
    y=d.*x +c.*x.^2 + b.*x.^3 + a.*x.^4;
end