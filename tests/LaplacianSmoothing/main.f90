!>----------------------------------------------------------
!> Test for Laplacian Smoothing approach by Osher
!> to find minima of non-convex function via gradient
!> descent dynamics.
!>
!> \author{Enrico Facca}
!>
!> DESCRIPTION: 
!> The main program 
!> 
!>
!> REVISION HISTORY:
!> 20190327 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program laplace
  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  use TimeInputs
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       stdin=>input_unit, &
       stdout=>output_unit, &
       stderr=>error_unit  
  implicit none
  
  type(file)     :: fgrid,fgridout,fctrl,fout,fmatrix
  type(abs_simplex_mesh)     :: grid,subgrid

  type(p1gal)    :: p1
  type( agmg_inv) :: multigrid_inverse

  ! 
  real(kind=double), allocatable :: diffusion_coeff(:),rhs(:),increment(:)
  
  ! drichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  !  p1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) ,gradsol(:,:), FNEWTON(:)

  !  exact solution and its  gradient
  real(kind=double), allocatable :: pot(:),scr(:),scr2(:), gradpot(:,:),gdvar(:),gdvar_old(:),opt1(:),opt2(:),gradient(:)

  !  error pot and its gradient
  real(kind=double), allocatable :: err_pot(:), err_gradpot(:,:), err_nrmgradpot(:)

  ! errror norm
  real(kind=double)  :: l2err_pot, l2err_gradpot ,l2err_pot_before, l2err_gradpot_before 
    real(kind=double)  :: sigma,deltat,current_var
  
  ! euclidian norm
  real(kind=double) :: dnrm2,ddot,time

  character(len=256) :: input,fngrid,fnctrl,fnctrl_in,fname,tail,fnout,msg,msg2,str
  integer :: info,nfull,ntdens



  type(spmat)    :: stiff_matrix,matrix2solve
  type(input_solver) :: ctrl
  type(output_solver):: info_solver,info1
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! local vars
  logical :: rc,save_test
  integer :: res,nterm,ndof,i,j,inod,ndir,inode,int_now,int_before
  integer :: nnode, ncell
  integer :: nref,initial,time_freq
  integer :: igrid,id_test_read,id_test
  integer :: max_time_steps,info_prec

  type(scalmat) :: identity
  type(diagmat) :: diag_weigth
  type(spmat) :: mass
  type(spmat), target  :: hessian,hessian_total

  real(kind=double) ::  tolerance_nonlinear
  real(kind=double) :: absolute_growth_factor
  real(kind=double) :: relative_growth_factor
  
  real(kind=double) :: alpha
  real(kind=double) :: fnewton_norm,initial_fnewton_norm,previuous_fnewton_norm
  
  integer :: flag
  integer :: current_newton_step,info_newton
  integer :: timestepping
  
  !
  ! read grid path352
  
  !
  CALL getarg(1, input)
  read(input,*,iostat=res) fngrid
  if (res.ne.0) nref=0


  


  !
  ! read id_test
  !
  id_test=1


  CALL getarg(2, input)
  read(input,*,iostat=res) fnout
  if (res.ne.0) nref=0

  CALL getarg(3, input)
  read(input,*,iostat=res) max_time_steps

  CALL getarg(4, input)
  read(input,*,iostat=res) deltat

  CALL getarg(5, input)
  read(input,*,iostat=res) initial

   CALL getarg(6, input)
  read(input,*,iostat=res) sigma


  timestepping=1
  

  
  
  !
  ! read linear solver controls
  !
  fnctrl='inputs/linear_solver.ctrl'
!!$  CALL getarg(3, input)
!!$  read(input,'(a)',iostat=res) fnctrl_in 
!!$  if (res.ne.0) then
!!$     fnctrl=fnctrl_in
!!$  end if
  call fctrl%init(stderr,etb(fnctrl),8,'in')
  call ctrl%read(stderr,fctrl)
  read(fctrl%lun,*) ctrl%approach 
  call fctrl%kill(stderr)
  
  write(stdout,*) '*****************TEST CASE ***********************'
  write(stdout,*) 'id_test    = ', id_test
  write(stdout,*) '**************************************************'

  write(stdout,*) '*************INFO LINEAR SOLVER ******************'
  call ctrl%info(0)
  write(stdout,*) '**************************************************'
  
  
  call fgrid%init(6,etb(fngrid),20,'in')
  call fgrid%info(0)
  call grid%read_mesh(0, fgrid)
  call grid%build_size_cell(0)
  call grid%build_normal_cell(0)
  call grid%build_edge_connection(0)
  call grid%build_nodebc()
  call grid%build_bar_cell()
  call grid%info(0)

  write(*,*) 'nref|        mesh par      l2err_pot  l2err_gradpot '
  
  
  
  nnode=grid%nnode
  ncell=grid%ncell

  allocate(diffusion_coeff(ncell),stat=res)
  if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
       'array diffusion_coeff',res)
  ! give a value to cond
  diffusion_coeff=one

  ! p1 galerkin for laplace
  write(*,*) ' build_p1'
  call p1%init(6, grid)

  nterm = p1%nterm_csr
  ndof = p1%grid%nnode

  call stiff_matrix%init(6,&
       ndof, ndof, nterm,&
       storage_system='csr',&
       is_symmetric=.true.)
  
  write(*,*) ' build stiff'
  call p1%build_stiff(6, 'csr', diffusion_coeff, stiff_matrix)


  call identity%eye(ndof)
  call p1%build_mass(stderr,'csr', diffusion_coeff,mass)
  
  !
  ! allocate space for rhs, sol, gradsol
  !
  allocate(&
       FNEWTON(ndof),&
       pot(ndof),&
       gdvar(ndof),&
       gdvar_old(ndof),&
       rhs(ndof),&
       opt1(ndof),&
       opt2(ndof),&
       increment(ndof),&
       gradient(ndof),&
       stat=res)
  if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
       'arrays rhs,sol,gradsol ',res)
  
  
  rhs=zero
  increment=one

  !
  ! init gdvar
  !
  select case (initial)
  case (1)
     gdvar=3.0d0
  case(2)
     gdvar=-3.0d0
  case (3)
     gdvar=1.1d0
  case (4)
     gdvar=0.9d0
  case (5)
     do inode=1,grid%nnode
        if (grid%coord(1,inode) > 0.5) then
           gdvar(inode)=3.0d0
        else
           gdvar(inode)=-3.0d0
        end if
     end do
  case (6)
     do inode=1,grid%nnode
        if (grid%coord(1,inode) > 0.5) then
           gdvar(inode)=1.1d0
        else
           gdvar(inode)=0.9d0
        end if
     end do
  case (7)
     call random_number(gdvar)
     gdvar=1.10d0+2*gdvar
  end select

  time_freq=0.
     
  opt1=-one
  opt2=two

  tolerance_nonlinear=1.0d-10
  absolute_growth_factor=1.0d4
  relative_growth_factor=1.0d4
  

  !
  ! init (I-sigma L)
  !
  !sigma=1.0d0
  matrix2solve=mass
  !call matrix2solve%aMpN(sigma,stiff_matrix)
  matrix2solve%coeff=mass%coeff+sigma*stiff_matrix%coeff

  write(*,*) 'sigma=',sigma
  write(fname,'(a,1pe8.2,a)') 'matrix',sigma,'.dat'
  call fmatrix%init(stderr, fname, 12344, 'out')
  call matrix2solve%write(12344)
  call fmatrix%kill(stderr)

  time=zero
  call fout%init(stderr, fnout, 1234, 'out')
  call writearray2file(stderr, 'head',&
       time,ndof, gdvar, &
       fout%lun, fout%fn)


  
  do i=1, max_time_steps
     select case (timestepping)
        !
        ! explicit euler
        !
     case(1)

        !
        ! eval gradient
        !
        do inode=1,ndof
           call nonconvex(gdvar(inode), 1, gradient(inode))
        end do
        write(*,*) minval(gradient),maxval(gradient)
        rhs=-gradient*p1%basis_integral
        write(*,*) 'rhs norm',dnrm2(ndof,rhs,1)



        !
        ! use gauss7 to compute rhs_i=-\int p'(u)\Psi_{i}
        !
        call integrate_gradient(grid,p1,gdvar,rhs)
        rhs=-rhs
        write(*,*) 'rhs norm',dnrm2(ndof,rhs,1)
        write(*,*) 'diff norm',dnrm2(ndof,rhs+gradient*p1%basis_integral,1)


        if (dnrm2(ndof,rhs,1)< 1e-10) exit


        !
        ! eval increment
        !  
        write(*,*) ' solve linear system',etb(ctrl%approach)
        increment=zero
        select case (ctrl%approach)
        case('MG') 
           call multigrid_inverse%init(stderr,matrix2solve,ctrl)
           call multigrid_inverse%Mxv(rhs,increment)
           call multigrid_inverse%info_solver%info(6)
           call multigrid_inverse%kill(stderr)
        case ('ITERATIVE')
           call ctrl_prec%init(stderr,'IC',n_fillin=30,tol_fillin=1d-3)
           call prec_stiff%init(stderr, info_prec,ctrl_prec,ndof, stiff_matrix)
           call info_solver%init()
           call linear_solver(matrix2solve,rhs,increment,info_solver,ctrl,prec_stiff)
           call ctrl_prec%kill()
           call prec_stiff%kill(stderr)
           call info_solver%kill()

        end select

        !
        ! check residua
        !
        call matrix2solve%mxv(increment,pot)
        pot=pot-rhs
        write(*,*) ' residua =', dnrm2(ndof,pot,1)

      
        
        !
        ! update solution
        !
        gdvar=gdvar + deltat * increment
        time = time + deltat

     case(2)

        flag=1
        info_newton=0
        current_newton_step=0
        gdvar_old=gdvar
        do while ( ( flag .gt. 0 ) .and. (info_newton .eq. 0) )
           select case ( flag)
           case (1)
              !
              ! assembly Fnewton=(I-deltat L) (gdvar-gdvar_old) + gradient(f(u))
              !
              call integrate_gradient(grid,p1,gdvar,rhs)
              
            
              FNEWTON(ndof+1:nfull) = &
                   - 1/deltat 
              scr=gdvar-gdvar_old/deltat
              call matrix2solve%Mxv(scr,scr2)
              
              FNEWTON=scr2+rhs

           case (2)
              !
              ! compute norm of F 
              !
              fnewton_norm=dnrm2(ndof+ntdens,FNEWTON,1)
              !
              ! print info
              !
              write(msg,'(I2,a,(1pe12.3))') &
                   current_newton_step,&
                   ' | NEWTON FUNC=',fnewton_norm
              write(stdout,*) etb(msg)
           case (3)
              !
              ! 1 - assembly jacobian J 
              ! 2 - solve J s = -F 
              !
              call compute_hessian(grid,p1,gdvar,hessian)

              hessian_total%coeff=1/deltat*matrix2solve%coeff+hessian%coeff
              


              !
              ! set controls for linear solver
              !
              ctrl%tol_sol=1.0d-4
              info_newton=info

              !
              ! print info
              !
              call info_solver%info2str(str) 
              write(msg,'(I2,a,a)') &
                   current_newton_step, ' | LINEAR SOLVER :  ',etb(str)
              write(stdout,*) etb(msg)
              
              !
              ! set newton incremental step thus that
              ! block 2,2 of Jacobian Matrix is stricly positive/negative 
              !
              alpha=one
              !test_cycle=.True.
              !do while (test_cycle)  
              !end do
              write(msg,'(I2,a,1pe9.2)') current_newton_step,&
                   ' | Alpha Dumping =', alpha 
              write(stdout,*) etb(msg)
             
           end select
           call newton_raphson_reverse(&
                flag,info_newton, &
                current_newton_step, nfull, gdvar, increment, alpha, &
                fnewton_norm, initial_fnewton_norm, previuous_fnewton_norm,&          
                30,&
                tolerance_nonlinear,absolute_growth_factor, relative_growth_factor)

        end do



     end select
        write(*,*) &
          'Step ',i, &
          'Time=',time,&
          'Norm grad= ', &
          dnrm2(ndof, gradient,1)
     write(*,*)  minval(gdvar),'<=gdvar<=',maxval(gdvar)
     write(*,*) 'distance from first  equilibrium  = -1 :: ', &
          dnrm2(ndof,opt1-gdvar,1)
     write(*,*) 'distance from second equilibrium  =  2 :: ', &
          dnrm2(ndof,opt2-gdvar,1)

     
     current_var=dnrm2(ndof,rhs,1)

     !
     !
     ! save when variation change digit the last one
     !
     save_test = .False.
     if ( ( i .ne. 0) ) then 
        int_now=int(current_var*10.0d0**(-int(log10(current_var))+1))
        if ( (int_now .ne. int_before) ) then
           int_before=int_now
           save_test = .true.
        end if
     else
        save_test = .True.
     end if
     write(*,*) save_test,current_var
     if (save_test) then
        call writearray2file(stderr, 'body',&
             time,&
             ndof, gdvar,&
             fout%lun, fout%fn)
     end if
     
  end do
  !
  ! close file
  !
  call writearray2file(stderr, 'tail',&
       time,&
       ndof, gdvar,&
       fout%lun, fout%fn)
  
  !
  ! free memory 
  !
  deallocate(diffusion_coeff,rhs,increment,stat=res)
  if(res .ne. 0) rc = IOerr(6, err_dealloc , 'Laplace main',&
       'arrays diffusion_coeff, rhs, sol',res)
 

  call stiff_matrix%kill(6)
  call p1%kill(6)
  
  call grid%kill(6)
  call fgrid%kill(6)

end PROGRAM laplace

subroutine nonconvex(x,derivative_order,y)
  use Globals
  implicit none
  real(kind=double), intent(in) :: x
  integer,           intent(in) :: derivative_order
  real(kind=double), intent(inout):: y
  !local
  real(kind=double) :: a,b,c,d
  a=1.0d0
  b=-8.0d0/3.0d0
  c=-2.d0
  d=8.0d0
    
  if (derivative_order .eq. 0)  y=d*x   +c*x**2 + b*x**3   + a*x**4
  if (derivative_order .eq. 1)  y=d     +2*c*x  + 3*b*x**2 + 4*a*x**3
  if (derivative_order .eq. 2)  y=0.0d0 +2*c    + 2*3*b*x  + 3*4*a*x**2

end subroutine nonconvex

subroutine integrate_gradient(grid,p1,gdvar,integrated_gradient)
  
  use Globals
  use AbstractGeometry
  use P1Galerkin
  implicit none
  type(abs_simplex_mesh),intent(in) :: grid
  type(p1gal),intent(in)    :: p1
  real(kind=double),  intent(in   ) :: gdvar(grid%nnode)
  real(kind=double),  intent(inout) :: integrated_gradient(grid%nnode)
  ! local
  integer :: icell, inode, iloc,iqn,jnode,jloc
  real(kind=double) :: xv(3),yv(3),qsx(7),qsy(7),qswg(7),barcoord(3,7),feval(7)
  real(kind=double) :: gdinq,pprimeinq ,base_fun
  real(kind=double) :: a_loc,b_loc,c_loc,int_loc
  
  call compute_barcoord(7,barcoord)

  integrated_gradient=zero
  do icell =1, grid%ncell
     do iloc= 1,3
        inode=grid%topol(iloc,icell)
        xv(iloc)=grid%coord(1,inode)
        yv(iloc)=grid%coord(2,inode)
     end do
     call gauss7 (7, grid%size_cell(icell), xv, yv, qsx, qsy, qswg)

     do iqn=1,7
        !
        ! compute -p'(u(x,y)) on quadrature points
        !
        gdinq=zero
        do jloc=1,3
           jnode = grid%topol(jloc,icell)
           gdinq = gdinq+barcoord(jloc,iqn)*gdvar(jnode)
        end do
        call nonconvex(gdinq, 1, feval(iqn))
        !feval(iqn)=-feval(iqn)
     end do

     do iloc = 1,3
        inode = grid%topol(iloc,icell)
        a_loc = p1%coeff_basis(1,iloc,icell)
        b_loc = p1%coeff_basis(2,iloc,icell)
        c_loc = p1%coeff_basis(3,iloc,icell)
        !write(*,*) 'test',iloc,a_loc+b_loc*xv(iloc)+c_loc*yv(iloc)
        !write(*,*) 'test',mod(iloc,3)+1  ,a_loc+b_loc*xv(mod(iloc  ,3)+1)+c_loc*yv(mod(iloc  ,3)+1)
        !write(*,*) 'test',mod(iloc+1,3)+1,a_loc+b_loc*xv(mod(iloc+1,3)+1)+c_loc*yv(mod(iloc+1,3)+1)


        int_loc = zero
        do iqn=1,7      
           !
           ! compute basis function on xq,yq
           !
           base_fun=barcoord(iloc,iqn)

           !
           ! add integral contribution
           !
           int_loc=int_loc+qswg(iqn)*feval(iqn)*base_fun
        end do
        !
        ! add local contribution to $\int_{T_loc} f \phi_{iloc}
        integrated_gradient(inode)=integrated_gradient(inode)+int_loc
     end do
        
     
  end do
  
end subroutine integrate_gradient
  
subroutine compute_hessian(grid,p1,gdvar,hessian)
  use Globals
  use AbstractGeometry
  use P1Galerkin
  use SparseMatrix
  implicit none
  type(abs_simplex_mesh),intent(in) :: grid
  type(p1gal),intent(in)    :: p1
  real(kind=double),  intent(in   ) :: gdvar(grid%nnode)
  type(spmat),  intent(inout) :: hessian
  ! local
  integer :: icell, inode, iloc,iqn,jnode,jloc,ind
  real(kind=double) :: xv(3),yv(3),qsx(7),qsy(7),qswg(7),barcoord(3,7),feval(7)
  real(kind=double) :: gdinq,pprimeinq ,base_fun
  real(kind=double) :: a_loc,b_loc,c_loc,int_loc
  
  call compute_barcoord(7,barcoord)

  hessian%coeff=zero
  do icell =1, grid%ncell
     do iloc= 1,3
        inode=grid%topol(iloc,icell)
        xv(iloc)=grid%coord(1,inode)
        yv(iloc)=grid%coord(2,inode)
     end do
     call gauss7 (7, grid%size_cell(icell), xv, yv, qsx, qsy, qswg)

     do iqn=1,7
        !
        ! compute -p''(u(x,y)) on quadrature points
        !
        gdinq=zero
        do jloc=1,3
           jnode = grid%topol(jloc,icell)
           gdinq = gdinq+barcoord(jloc,iqn)*gdvar(jnode)
        end do
        call nonconvex(gdinq, 2, feval(iqn))
     end do

     do iqn=1,7
        !
        ! cycle all quadrature points
        !

        
        do iloc = 1,3
           inode = grid%topol(iloc,icell)
           do jloc=1,3
              jnode=grid%topol(jloc,icell)
              ind=p1%assembler_csr(iloc,jloc,icell)
              !
              ! cycle all non-null node interaction
              !
              ! u(xq,yq)=\sum_{k=1}^{3} lambda(k,iq) u(x(topol(k,icell),y(topol(k,icell)) 
              hessian%coeff(ind)=hessian%coeff(ind)+&
                   barcoord(iloc,iqn)*feval(iqn)*& ! p''(u(xq,xq))*d/d_{u_iloc} u(xq,yq)
                   barcoord(jloc,iqn)& ! 
                   *qswg(iqn)    ! integra weight
           end do
           
        end do

     end do
        
     
  end do
  
end subroutine compute_hessian





  subroutine gauss7 (nqn, area, xv, yv, qsx, qsy, qswg)
!      purpose
!      =======
!      compute the nodes and the weights of the quadrature scheme
!      with 7 nodes and degree 5
         use Globals
         implicit none 

         integer, intent(in) :: nqn
         
         real(kind=double), intent(in ) :: area
         real(kind=double), intent(in ) :: xv(3), yv(3)
         real(kind=double), intent(out) :: qsx(nqn), qsy(nqn), qswg(nqn)
         !local
         real(kind=double)  a1,a2,a3,a4,a5,a6,radi
         
         parameter(radi= 3.87298334620741702d0, &
              a1  = (6.d0 - radi)/21.d0, &
              a2  = a1,&
              a3  = (9.d0 + 2.d0*radi)/21.d0, &
              a4  = (6.d0 + radi)/21.d0, &
              a5  = a4,&
              a6  = (9.d0 - 2.d0*radi)/21.d0)
         
         qsx(7) = (xv(1)    + xv(2)    + xv(3))/3.d0
         qsx(1) =  xv(1)*a1 + xv(2)*a2 + xv(3)*a3
         qsx(2) =  xv(1)*a2 + xv(2)*a3 + xv(3)*a1
         qsx(3) =  xv(1)*a3 + xv(2)*a1 + xv(3)*a2
         qsx(4) =  xv(1)*a4 + xv(2)*a5 + xv(3)*a6
         qsx(5) =  xv(1)*a5 + xv(2)*a6 + xv(3)*a4
         qsx(6) =  xv(1)*a6 + xv(2)*a4 + xv(3)*a5
         
         qsy(7) = (yv(1)    + yv(2)    + yv(3))/3.d0
         qsy(1) =  yv(1)*a1 + yv(2)*a2 + yv(3)*a3
         qsy(2) =  yv(1)*a2 + yv(2)*a3 + yv(3)*a1
         qsy(3) =  yv(1)*a3 + yv(2)*a1 + yv(3)*a2
         qsy(4) =  yv(1)*a4 + yv(2)*a5 + yv(3)*a6
         qsy(5) =  yv(1)*a5 + yv(2)*a6 + yv(3)*a4
         qsy(6) =  yv(1)*a6 + yv(2)*a4 + yv(3)*a5
         
         qswg(7) = (9.d0/40.d0)*area
         qswg(1) = ((155.d0 - radi)/1200.d0)*area
         qswg(2) = ((155.d0 - radi)/1200.d0)*area
         qswg(3) = ((155.d0 - radi)/1200.d0)*area
         qswg(4) = ((155.d0 + radi)/1200.d0)*area
         qswg(5) = ((155.d0 + radi)/1200.d0)*area
         qswg(6) = ((155.d0 + radi)/1200.d0)*area
         
       end subroutine gauss7

       subroutine compute_barcoord(nqn, barcoord)
          use Globals
         implicit none 

         integer, intent(in) :: nqn
         real(kind=double), intent(out) :: barcoord(3,nqn)
         !local
         real(kind=double)  a1,a2,a3,a4,a5,a6,radi
         
         parameter(radi= 3.87298334620741702d0, &
              a1  = (6.d0 - radi)/21.d0, &
              a2  = a1,&
              a3  = (9.d0 + 2.d0*radi)/21.d0, &
              a4  = (6.d0 + radi)/21.d0, &
              a5  = a4,&
              a6  = (9.d0 - 2.d0*radi)/21.d0)

         barcoord(:,7) = one/3.d0
         barcoord(:,1) =  (/a1,a2,a3/)
         barcoord(:,2) =  (/a2,a3,a1/)
         barcoord(:,3) =  (/a3,a1,a2/)
         barcoord(:,4) =  (/a4,a5,a6/)
         barcoord(:,5) =  (/a5,a6,a4/)
         barcoord(:,6) =  (/a6,a4,a5/)
         
       end subroutine compute_barcoord
