!>---------------------------------------------------------------------
!> 
!> Convergence test for $-\Delta u = f(x)$ on the unit square
!> with $f(x,y)=-5/4 \pi^2 \sin(\pi x) \cos(\pi/2 \; y)$
!> zero Dirichlet conditions. The solution is:
!> $u(x,y) = \sin(\pi x) \cos(\frac{\pi}{2}y)$
!> 
!> starts from a simple grid of 9 nodes and 8 triangles and then
!> refines nref times.
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> The main program 
!> 
!>
!> REVISION HISTORY:
!> 20190327 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program laplace
  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  
  use TimeInputs, only : write_steady
    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       stdin=>input_unit, &
       stdout=>output_unit, &
       stderr=>error_unit  
  implicit none
  
  type(file)     :: fgrid,fgridout,fctrl,fout
  type(abs_simplex_mesh)     :: grid0,grid,subgrid,cellgraph

  type(p1gal)    :: lapl_p1
  type( agmg_inv) :: multigrid_inverse

  ! 
  real(kind=double), allocatable :: diffusion_coeff(:),rhs(:),sol(:)
  
  ! drichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  !  p1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) ,gradsol(:,:)

  !  exact solution and its  gradient
  real(kind=double), allocatable :: pot(:), gradpot(:,:)

  !  error pot and its gradient
  real(kind=double), allocatable :: err_pot(:), err_gradpot(:,:), err_nrmgradpot(:)

  ! errror norm
  real(kind=double)  :: l2err_pot, l2err_gradpot ,l2err_pot_before, l2err_gradpot_before 

  ! euclidian norm
  real(kind=double) :: dnrm2,ddot

  character(len=256) :: input,fngrid,fnctrl,fnctrl_in,fname,tail,graphname



  type(spmat)    :: stiff_matrix
  type(input_solver) :: ctrl
  type(output_solver):: info,info1
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! local vars
  logical :: rc
  integer :: res,nterm,ndof,i,j,inod,ndir
  integer :: nnode, ncell
  integer :: nref
  integer :: igrid,id_test_read,id_test


  !
  ! read grid path
  !
  CALL getarg(1, input)
  read(input,*,iostat=res) fngrid
  if (res.ne.0) nref=0


  
  !
  ! read number of refinements
  !
  CALL getarg(2, input)
  read(input,*,iostat=res) nref
  if (res.ne.0) nref=0


  !
  ! read id_test
  !
  id_test=1

  
  !
  ! read linear solver controls
  !
  fnctrl='inputs/linear_solver.ctrl'
  call fctrl%init(stderr,etb(fnctrl),8,'in')
  call ctrl%read(stderr,fctrl)
  read(fctrl%lun,*) ctrl%approach 
  call fctrl%kill(stderr)
  
  write(stdout,*) '*****************TEST CASE ***********************'
  write(stdout,*) 'id_test    = ', id_test
  write(stdout,*) etb(example_description(id_test))
  write(stdout,*) 'total nref =',nref
  write(stdout,*) '**************************************************'

  write(stdout,*) '*************INFO LINEAR SOLVER ******************'
  call ctrl%info(0)
  write(stdout,*) '**************************************************'
  
  
  call fgrid%init(6,etb(fngrid),20,'in')
  call fgrid%info(0)
  call grid0%read_mesh(0, fgrid)
  call grid0%build_size_cell(0)
  call grid0%build_normal_cell(0)
  call grid0%build_edge_connection(0)
  call grid0%build_nodebc()
  call grid0%build_bar_cell()

  grid=grid0
  write(*,*) 'nref|        mesh par      l2err_pot  l2err_gradpot '
  
  do igrid = 1, nref+1
     write(*,*) ' ref $ ',igrid,' nnode= ',grid%nnode, ' ncell =', grid%ncell
     write(tail,'(a,I0.3,a)') 'ref',igrid-1,'.dat'
     fname=etb('grid'//etb(tail))

     call fout%init(0,fname,10000,'out')
     call grid%write_mesh(stderr, fout)
     call fout%kill(0)
     

     

     nnode=grid%nnode
     ncell=grid%ncell

     allocate(diffusion_coeff(ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'array diffusion_coeff',res)
     ! give a value to cond
     diffusion_coeff=one
     

     write(*,*) ' build cell-to-cell connection graph'
     call grid%build_boundary_cell(stderr)
     call grid%build_edge_connection(stderr)
     call grid%build_bar_cell(stderr)
     write(*,*) 'grid%nboundary_cell', grid%nboundary_cell
          
     call cellgraph%build_cellcell_graph(stderr,grid)

     write(tail,'(a,I0.3,a)') 'ref',igrid-1,'.dat'
     graphname=etb('graph'//etb(tail))
     
     call fout%init(0,graphname,10000,'out')
     call cellgraph%write_mesh(stderr, fout)
     call fout%kill(0)
     
     call cellgraph%info(6)

     call cellgraph%build_size_cell(0)     
     cellgraph%nnode_bc = grid%nboundary_cell
     cellgraph%node_bc = grid%boundary_cell

     ndof=cellgraph%nnode

    

     call fout%init(0,graphname,10000,'out')
     call cellgraph%write_mesh(stderr, fout)
     call fout%kill(0)


     !
     ! allocate space for rhs, sol, gradsol
     !
     allocate(rhs(ndof),sol(ndof),&
          gradsol(grid%logical_dimension,ncell),&
          stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays rhs,sol,gradsol ',res)

     fname=etb('boundary_cell'//etb(tail))
     call fout%init(0,fname,10000,'out')
     sol=zero
     do i=1,grid%nboundary_cell
        write(*,*) i, grid%boundary_cell(i)
        sol(grid%boundary_cell(i))=one
     end do
     call write_steady(0, 10000, grid%ncell, sol)
     call fout%kill(0)

     
     
!!$     
!!$    
!!$     rhs=zero
!!$     sol=one
!!$     gradsol=zero
!!$     !
!!$     ! create rhs $rhs(i)=\int f \phi_i$
!!$     !
!!$     do i=1,ndof
!!$        rhs(i) = forcing(id_test,&
!!$             cellgraph%bar_cell(1,i),&
!!$             cellgraph%bar_cell(2,i),&
!!$             cellgraph%bar_cell(3,i))
!!$     end do
!!$     
!!$     !
!!$     ! set dirichlet boundary conditions
!!$     !
     ndir=cellgraph%nnode_bc
     allocate(noddir(ndir),soldir(ndir),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays noddir soldir',res)
     noddir=grid%node_bc
     do i=1,grid%nnode_bc
        j = grid%node_bc(i)
        soldir(i) = pot_exact(id_test,&
             grid%coord(1,j),grid%coord(2,j),grid%coord(3,j))
     end do
!!$
!!$     !
!!$     ! preprocess stiffness_matrix rhs and solution 
!!$     ! to obtain dirichlet boundary condition
!!$     !
!!$     call lapl_p1%dirichlet_bc(stderr,&
!!$          stiff_matrix,rhs,sol,&
!!$          ndir,noddir,soldir)
!!$     sol=zero
!!$
!!$     open(9,file='stiffness_correct.')
!!$     call stiff_matrix%write(9)
!!$     close(9)
!!$
!!$
!!$     open(9,file='rhs.out')
!!$     do i=1,nnode
!!$        write(9,*) i, rhs(i)
!!$     end do
!!$     close(9)
!!$
!!$     write(*,*) ' solve linear system',etb(ctrl%approach)
!!$     sol=zero
!!$
!!$     select case (ctrl%approach)
!!$     case('MG') 
!!$        call multigrid_inverse%init(stderr,stiff_matrix,ctrl)
!!$        call multigrid_inverse%Mxv(rhs,sol)
!!$        call multigrid_inverse%info_solver%info(6)
!!$        call multigrid_inverse%kill(stderr)
!!$     case ('ITERATIVE')
!!$        !write(*,*) 'call sum'
!!$        stiff_matrix%name='A'
!!$        !call sum1%info(6)
!!$        !call zeromat1%info(6)
!!$        !write(*,*) sum1%type,sum1%is_symmetric
!!$        call ctrl_prec%init(stderr,'IC',n_fillin=30,tol_fillin=1d-3)
!!$        call prec_stiff%init(stderr, i,ctrl_prec,ndof, stiff_matrix)
!!$        call info%init()
!!$        call linear_solver(stiff_matrix,rhs,sol,info,ctrl,prec_stiff)
!!$        call ctrl_prec%kill()
!!$        call prec_stiff%kill(stderr)
!!$        call info%info(stdout)
!!$        call info%time(stdout)
!!$        call info%kill()
!!$
!!$     end select
!!$     
!!$     write(*,*) ' write solution'
!!$     fname=etb('pot'//etb(tail))
!!$     call fout%init(0,fname,10000,'out')
!!$     call write_steady(0, 10000, ndof, sol)
!!$     call fout%kill(0)
!!$    
     !
     ! allocate space for reference solution and work arrays 
     !
     allocate(&
          pot(ndof),&
          gradpot(grid%logical_dimension,ncell),&
          err_pot(ndof),&
          err_gradpot(grid%logical_dimension,ncell),&
          err_nrmgradpot(ncell),&
          stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays pot gradpot',res)
!!$
!!$     call stiff_matrix%mxv(sol,pot)
!!$     pot=pot-rhs
!!$
!!$     write(*,*) ' residua =', dnrm2(ndof,pot,1)
!!$
!!$     !
!!$     ! compute gradient of the solution 
!!$     ! exact solution its gradient
!!$     ! errors arrays
!!$     !
!!$     call lapl_p1%eval_grad(sol,gradsol)
!!$     do i=1,ndof
!!$        pot(i) = pot_exact(id_test, &
!!$             grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
!!$     end do
!!$     do i=1,ncell
!!$        call gradpot_exact(id_test,&
!!$             grid%bar_cell(1,i),&
!!$             grid%bar_cell(2,i),&
!!$             grid%bar_cell(3,i),&
!!$             grid%logical_dimension, gradpot(1:grid%logical_dimension,i))
!!$     end do
!!$     fname=etb('exact_pot'//etb(tail))
!!$     call fout%init(0,fname,10000,'out')
!!$     call write_steady(0, 10000, ndof, pot)
!!$     call fout%kill(0)
!!$
!!$     
!!$     err_pot=sol-pot
!!$     err_gradpot=gradsol-gradpot
!!$     do i=1,grid%ncell
!!$        err_nrmgradpot(i)=dnrm2(grid%logical_dimension,err_gradpot(1:grid%logical_dimension,i),1)
!!$     end do
!!$
!!$     !
!!$     ! compute l2 and print error w.r.t exact solution 
!!$     !
!!$     if (igrid >1 ) then
!!$        l2err_pot_before = l2err_pot 
!!$        l2err_gradpot_before = l2err_gradpot
!!$     end if
!!$     err_pot=err_pot**2
!!$     l2err_pot=sqrt(ddot(ndof, lapl_p1%basis_integral, 1, err_pot,1))
!!$     l2err_gradpot=grid%normp_cell(2.0d0,err_nrmgradpot)
!!$     write(stdout,'(I4,a,3(1pe15.5))') igrid-1, ' | ',grid%meshpar(0), &
!!$          l2err_pot, l2err_gradpot
!!$
!!$     if (igrid >1 ) then
!!$        write(stdout,'(I4,a,3(1pe15.5))') igrid-1, ' | ',grid%meshpar(0), &
!!$          one/(l2err_pot/l2err_pot_before), one/(l2err_gradpot/l2err_gradpot_before)
!!$     end if

     !
     ! free memory 
     !
     deallocate(noddir,soldir,stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays noddir soldir',res)

     deallocate(&
          pot,&
          gradpot,&
          err_pot,&
          err_gradpot,&
          err_nrmgradpot,&
          stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays pot gradpot',res)

     deallocate(diffusion_coeff,rhs,sol,gradsol,stat=res)
     if(res .ne. 0) rc = IOerr(6, err_dealloc , 'Laplace main',&
          'arrays diffusion_coeff, rhs, sol',res)

     if (igrid<nref + 1) then
        write(*,*) ' build refinement'
        call subgrid%refine(stderr,grid)!flag_reorder=1)
        call subgrid%RCM_node_reorder(stderr)
        call subgrid%build_size_cell(0)
        call subgrid%build_normal_cell(0)
        call subgrid%build_edge_connection(0)
        call subgrid%build_nodebc()
        call subgrid%build_bar_cell()
     
        write(*,*) ' subgrid kill'
        grid=subgrid
        call subgrid%kill(stderr)
     end if
  end do
  call grid%kill(6)

  call grid0%kill(6)
  call fgrid%kill(6)

contains

  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if(id_test.eq.1) str='Rectangle [0,1][0,1] with zero dirichlet bc'
  
  end function example_description

  function forcing(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    if(id_test.eq.1) func=pigreco**2 * sin(pigreco*x) * cos(pigreco*y*onehalf)*5.0d0*onefourth
  
  end function forcing

  function pot_exact(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    if(id_test.eq.1) func=sin(pigreco*x) * cos(pigreco*y*onehalf)

  end function pot_exact

  subroutine gradpot_exact(id_test,x,y,z,logical_dimension,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    integer,           intent(in):: logical_dimension
    real(kind=double), intent(out) :: func(logical_dimension)

    if ( id_test .eq. 1 ) then
       func(1)=pigreco*cos(pigreco*x) * cos(pigreco*y*onehalf)
       func(2)=-pigreco*onehalf*sin(pigreco*x) * sin(pigreco*y*onehalf)
       if (logical_dimension .eq. 3 ) func(3)=zero
    end if

  end subroutine gradpot_exact
  
  
end PROGRAM laplace



subroutine build_cell_laplacian(lun_err,graph,grad_operator,div_operator,cell_laplacian)
  use Globals
  use SparseMatrix
  use AbstractGeometry
  implicit none
  integer,                intent(in   ) :: lun_err
  type(abs_simplex_mesh), intent(in   ) :: graph
  type(spmat),            intent(inout) :: grad_operator
  type(spmat),            intent(inout) :: div_operator
  type(spmat),            intent(inout) :: cell_laplacian
  !local
  logical :: rc
  integer :: res,i
  real(kind=double),allocatable :: invW(:)

  !
  ! build the laplacian as the weigthed laplacian L
  ! of the graph describing the cell-cell connection
  ! the Laplacian matrix is 
  ! L=G^T W^-1 G
  ! with
  ! G=signed incidence matrix of graph (is the gradient operator)
  ! G^T=divergence operator
  ! W =diagonal matrix with t
  !
  !call graph%build_cellcell_graph(lun_err,this%grid_tdens)
  ! build G
  call graph%build_connection_matrix(lun_err,grad_operator)
  do i=1,graph%ncell
     grad_operator%coeff(grad_operator%ia(i))=one
     grad_operator%coeff(grad_operator%ia(i)+1)=-one
  end do

  ! build G^T
  div_operator=grad_operator
  call div_operator%transpose(lun_err)
  ! build W^-1
  allocate(invW(graph%ncell),stat=res)
  if (res.ne. 0) rc = IOerr(lun_err, err_alloc,&
       ' build_cell_laplacian', &
       ' temp array invW' ) 

  invW=one/graph%size_cell

  ! build laplacian
  call cell_laplacian%mult_MDN(lun_err,&
       div_operator,grad_operator,&
       20,20*graph%ncell,invW,&
       div_operator)

  deallocate(invW,stat=res)
  if (res.ne. 0) rc = IOerr(lun_err, err_dealloc,&
       ' build_cell_laplacian', &
       ' temp array invW' )
  call grad_operator%kill(lun_err)
  call div_operator%kill(lun_err)




end subroutine build_cell_laplacian



