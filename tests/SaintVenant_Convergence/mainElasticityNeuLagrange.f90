!>---------------------------------------------------------------------
!> 
!> DESCRIPTION: 
!> Convergence test for Saint Venant Equation $-\Div \sigma (u)  = f(x)$
!> on the unit square $[0,1] \times [0,1]$ with homogeneous Neumann 
!> Boundary conditions. The zero average constraint on the solution is
!> imposed using the method of Lagrange multipliers. The saddle point
!> system that arise from the discretization of the continuous mixed 
!> formulation is solved using the BiConjugate Stabilized iterative 
!> method. 
!> The algorithm starts from a given grid and then refines nref times.
!>
!> \author{Enrico Facca and Nicolò Crescenzio}
!>
!> REVISION HISTORY:
!> 20201107 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program elasticityNeuLagrange

  use omp_lib
  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  use SaddlePointMatrix
  
  use TimeInputs, only : write_steady
    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       		    stdin  => input_unit, &
       		    stdout => output_unit, &
       		    stderr => error_unit  

  implicit none
  
  ! 00modGlobals.f90
  type(file) :: fgrid, fgridout, fctrl, fout

  ! 49modAbstractGeometry.f90
  type(abs_simplex_mesh), target     :: grid0, grid, subgrid

  ! 63modP1Galerkin.f90
  type(p1gal)    :: P1GALERKIN

  ! dagmg_wrap.f90
  type(agmg_inv) :: multigrid_inverse

  ! Diffusion, right hand side and solution arrays 
  real(kind=double), allocatable :: diffusion_coeff(:), rhsx(:), rhsy(:), rhs(:), rotx(:),&
	  roty(:), solx(:), soly(:), sol(:)
  
  ! Dirichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  ! P1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) , gradsolx(:,:), gradsoly(:,:)

  ! Exact solution and its gradient
  real(kind=double), allocatable :: potx(:), gradpotx(:,:), poty(:), gradpoty(:,:)

  ! Error pot and its gradient
  real(kind=double), allocatable :: err_potx(:), err_gradpotx(:,:), err_nrmgradpotx(:)
  real(kind=double), allocatable :: err_poty(:), err_gradpoty(:,:), err_nrmgradpoty(:)

  ! Errror norm
  real(kind=double)  :: l2errx_pot, l2errx_gradpot ,l2errx_pot_before, l2errx_gradpot_before,&
	  		h1errx_pot, h1errx_pot_before, rateL2x, rateH1x
  real(kind=double)  :: l2erry_pot, l2erry_gradpot ,l2erry_pot_before, l2erry_gradpot_before,&
	  		h1erry_pot, h1erry_pot_before, rateL2y, rateH1y

  ! Euclidian norm
  real(kind=double) :: dnrm2, ddot

  ! Character variables
  character(len=256) :: input, fngrid, fnctrl, fnctrl_in, fname, gridname, tail

  ! 22modSparseMatrix.f90
  type(spmat)    :: stiff_matrix
  type(spmat)    :: B
  type(spmat)    :: BT
  
  ! 57modSaddlePoint.f90
  type(saddlemat) :: saddle
  
  ! 20modSimpleMatrix.f90
  type(zeromat) :: zeromat1,zeromat2

  type(add_linop) :: sum1,sum2
  
  ! 12modLinearSolver.f90
  type(input_solver) :: ctrl
  type(output_solver):: info,info1
  
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! Variabili nuovo pattern
  integer :: nz
  integer :: dofsxnode = 2

  ! Local variables
  logical :: rc
  integer :: edge(2)
  integer :: res,nterm,ndof,i,j,inod,ndir, k
  integer :: nnode, ncell, iter, nnodeincell
  integer :: nref, icell, iloc, gridnumber
  integer :: igrid,id_test_read,id_test
  integer, pointer :: e1, e2, inode
  real(kind=double), pointer :: xn1, xn2, yn1, yn2
  real(kind=double) :: ledge, q, time
  real(kind=double) :: mu=1, lambda=1
  real(kind=double) :: f(2), u_ex(2), gradu_ex(2,2), forceboundary(4,2)
  real(kind=double), allocatable :: uno(:), due(:)

  ! --------------------------------------------------------------

  ! Read number of refinements
  call getarg(1,input)
  read(input,*,iostat=res) nref
  if (res.ne.0) nref = 0

  ! Read id starting grid
  call getarg(2,input)
  read(input,*,iostat=res) gridnumber
  if (res.ne.0) gridnumber = 0

  ! Read id_test
  call getarg(3,input)
  read(input,*,iostat=res) id_test
  if (id_test.ne.1) then
     id_test = 1
  end if

  print*
  write(stdout,*) '**************** TEST CASE ***********************'
  write(stdout,*) 'Domain: [0,1]x[0,1] unit square'
  write(stdout,*) 'PDE: Elasticity Equation'
  write(stdout,*) 'Boundary conditions: Neumann BCs'
  write(stdout,*) 'Solution obtained using the method of Lagrange Multipliers'
  write(stdout,'(1X,A,I1)') 'Starting from grid: ', gridnumber
  write(stdout,'(1X,A,I0.2)') 'Test ID = ', id_test
  write(stdout,'(1X,A,I0.2)') 'Total mesh refinements = ',nref
  write(stdout,*) '**************************************************'

  ! --------------------------------------------------------------

  ! File name with linear solver controls
  fnctrl = 'inputs/linear_solver.ctrl'
  ! Read from file 'linear_solver.ctrl'
  call fctrl%init(stderr,etb(fnctrl),8,'in')
  ! read_input_solver
  call ctrl%read(stderr,fctrl)
  ! Linear solver approach
  read(fctrl%lun,*) ctrl%approach 
  ! Kill variable file
  call fctrl%kill(stderr)
  
  print*
  write(stdout,*) '************ INFO LINEAR SOLVER ******************'
  call ctrl%info(0)
  write(stdout,*) '**************************************************'
  print*

  ! --------------------------------------------------------------

  ! Read mesh data from file 
  write(gridname,'(A,I1,A)') 'mesh',gridnumber,'.dat'
  fngrid = etb('inputs/'//etb(gridname))
  call fgrid%init(6,etb(fngrid),20,'in')
  !call fgrid%info(0)
  call grid0%read_mesh(0, fgrid)
  call grid0%build_size_cell(0)
  call grid0%build_normal_cell(0)
  call grid0%build_edge_connection(0)
  call grid0%build_nodebc()
  call grid0%build_bar_cell()
  !call grid0%info(0)

  grid = grid0

  ! --------------------------------------------------------------

  ! At each mesh refinement write data
  print*
  write(*,*) 'nref|   #nodes   #cells       mesh par       &
	  rate-L2x       rate-L2y       rate-H1x       rate-H1y    iter'
  write(stdout,'(1X,106("-"))') 
 
  ! Loop over nref
  do igrid = 1, nref+1

     write(tail,'(a,I0.3,a)') 'ref',igrid-1,'.dat'
     
     fname = etb('grid'//etb(tail))
     call fgridout%init(stderr,fname,8,'out')
     call grid%write_mesh(stderr, fgridout)
     call fgridout%kill(stderr)

     ! Number of nodes grid
     nnode = grid%nnode

     ! Number of cells grid
     ncell = grid%ncell

     ! Number of node in cell
     nnodeincell = grid%nnodeincell

     ! Allocate space for diffusion coefficient 
     allocate(diffusion_coeff(grid%ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'array diffusion_coeff',res)
     ! Give a value to cond
     diffusion_coeff = one

     ! P1 galerkin for Laplace Equation
     call P1GALERKIN%init(6, grid)

     ! Number of degrees of freedom 
     ! (= matrix rows/cols and unknowns)
     ndof = dofsxnode*nnode

     ! Number of non-zero terms
     nterm = P1GALERKIN%nterm_saint_venant_csr

     ! Initialization stiffness matrix (type SPMAT)
     call stiff_matrix%init(6,ndof, ndof, nterm,&
          storage_system='csr',is_symmetric=.true.)

     ! Build stiffness matrix saint venant equation
     call P1GALERKIN%build_stiff_saint_venant2D(6,&
	     diffusion_coeff,mu,lambda,stiff_matrix)

     !open(9,file='stiffness.dat')
     !call stiff_matrix%write(9,'matlab')
     !close(9)

  ! --------------------------------------------------------------

     ! Allocate space for rhs
     allocate(rhs(ndof+3),rhsx(nnode),rhsy(nnode),stat=res)
     if (res.ne.0) STOP "errore allocazione rhs"

     ! Initialize array
     rhs  = zero
	  rhsx = zero
	  rhsy = zero
     
     ! Create right hand side vector $rhs(i)=\int f \phi_i$
     ! basis_integral: integral of the basis function (computed in the constructor)
     ! forcing: forcing function defined in this file
     do i = 1,nnode
	     f = forcing(id_test,mu,lambda,grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
        rhsx(i) = P1GALERKIN%basis_integral(i)*f(1)
        rhsy(i) = P1GALERKIN%basis_integral(i)*f(2)
     end do

  ! --------------------------------------------------------------

     ! Initialization matrices B and BT (constraints for saddle problem)
     call B%init(6, 3, ndof, 2*ndof, &
	     storage_system='csr',is_symmetric=.false.)

     call BT%init(6, 3, ndof, 2*ndof, &
	     storage_system='csr',is_symmetric=.false.)

     B%ia(1) = 1
     B%ia(2) = nnode + B%ia(1)
     B%ia(3) = nnode + B%ia(2)
     B%ia(4) = ndof  + B%ia(3)

     do i = 1,nnode
	     B%ja(i        ) = 2*i-1
	     B%ja(i+  nnode) = 2*i
		  B%ja(i+2*nnode) = i
		  B%ja(i+3*nnode) = i+nnode
     end do

     B%coeff = zero

     allocate(rotx(nnode),roty(nnode),stat=res)
     if (res.ne.0) STOP "errore allocazione rotx e rot y"

     rotx = zero
     roty = zero

     do icell = 1,ncell
	     do iloc = 1,nnodeincell
   	     inode => grid%topol(iloc,icell)
	   	  rotx(inode) = rotx(inode) + &
		        grid%size_cell(icell)*P1GALERKIN%grad_basis(1,iloc,icell)
	   	  roty(inode) = roty(inode) - &
		   	  grid%size_cell(icell)*P1GALERKIN%grad_basis(2,iloc,icell)
        end do
     end do

     do i = 1,nnode
	     B%coeff(i      ) = P1GALERKIN%basis_integral(i)
	     B%coeff(i+nnode) = P1GALERKIN%basis_integral(i)
	     B%coeff(2*i-1+2*nnode) = roty(i)
	     B%coeff(2*i  +2*nnode) = rotx(i)
     end do

     deallocate(rotx,roty,stat=res)
     if (res.ne.0) STOP "errore deallocazione rotx e roty"

     BT = B

     call BT%transpose(6)

     ! Initialization saddle point matrix
     call saddle%init(6,stiff_matrix,BT,B,B1equalB2=.true.)

  ! --------------------------------------------------------------

     ! Allocazione arrays soluzione 
     allocate(sol(ndof+3),solx(nnode),soly(nnode),stat=res)
     if (res.ne.0) STOP "errore allocazione arrays sol"

     ! Initialize sol(x/y)
     sol  = zero
     solx = zero
     soly = zero

  ! --------------------------------------------------------------

     ! Set Neumann boundary conditions
     ! Number boundary nodes
     ndir = dofsxnode*grid%nnode_bc

     ! Loop over boundary edges
     do i = 1,grid%nedge_bc
        e1 => grid%edge_iside(1,i)
        e2 => grid%edge_iside(2,i)
		  xn1 => grid%coord(1,e1)
		  xn2 => grid%coord(1,e2)
		  yn1 => grid%coord(2,e1)
		  yn2 => grid%coord(2,e2)
		  ! Lati orizzontali dominio quadrato
		  if (yn1.eq.yn2) then
	   	  ledge = abs(xn1-xn2)
	   	  call boundary_force(id_test,mu,lambda,xn1,yn1,forceboundary)
	   	  rhsx(e1) = rhsx(e1) + 0.5*ledge*forceboundary(2,1)
	   	  call boundary_force(id_test,mu,lambda,xn2,yn2,forceboundary)
	   	  rhsx(e2) = rhsx(e2) + 0.5*ledge*forceboundary(2,1)
		  end if
		  ! Lati verticali dominio quadrato
		  if (xn1.eq.xn2) then
	     	  ledge = abs(yn1-yn2)
	     	  call boundary_force(id_test,mu,lambda,xn1,yn1,forceboundary)
	     	  rhsy(e1) = rhsy(e1) + 0.5*ledge*forceboundary(1,2)
	     	  call boundary_force(id_test,mu,lambda,xn2,yn2,forceboundary)
	     	  rhsy(e2) = rhsy(e2) + 0.5*ledge*forceboundary(1,2)
	  	  end if
     end do

	  do i = 1,nnode
		  rhs(2*i-1) = rhsx(i)
		  rhs(2*i  ) = rhsy(i)
	  end do

     open(9,file='rhsLAG.dat')
	  write(9,*) ndof
     do i = 1,nnode
	     write(9,'(F25.20)') rhsx(i)
     end do
     do i = 1,nnode
	     write(9,'(F25.20)') rhsy(i)
     end do
     close(9)

	  deallocate(rhsx,rhsy,stat=res)
	  if (res.ne.0) STOP "errore deallocazione rhsx rhsy"

  ! --------------------------------------------------------------

     ! Solution of the linear system 
     call bicgstab(ndof,saddle,rhs,1.0d-12,50000,iter,q,sol)
     
  ! --------------------------------------------------------------

     open(9,file='solElLag.dat')
	  write(9,*) ndof
     do i = 1,ndof
	     write(9,'(F25.20)') sol(i)
     end do
     close(9)

     ! Split solution into x and y component
     do i = 1,nnode
	     solx(i) = sol(2*i-1)
	     soly(i) = sol(2*i)
     end do

     ! Free memory
     deallocate(sol,stat=res)
     if (res.ne.0) STOP "errore deallocazione sol"

  ! --------------------------------------------------------------

     ! Allocazione arrays gradiente soluzione 
     allocate(gradsolx(grid%logical_dimension,ncell),&
	     gradsoly(grid%logical_dimension,ncell),stat=res)
     if (res.ne.0) STOP "errore allocazione arrays gradsol"

     ! Initializes arrays
     gradsolx = zero
     gradsoly = zero

     ! Compute the gradient of the numerical solution
     call P1GALERKIN%eval_grad(solx,gradsolx)
     call P1GALERKIN%eval_grad(soly,gradsoly)

  ! --------------------------------------------------------------

     ! Allocate space for reference solution and work arrays 
     allocate(potx(nnode),poty(nnode),&
          gradpotx(grid%logical_dimension,ncell),&
          gradpoty(grid%logical_dimension,ncell),&
          err_potx(nnode),err_poty(nnode),&
          err_gradpotx(grid%logical_dimension,ncell),&
          err_gradpoty(grid%logical_dimension,ncell),&
          err_nrmgradpotx(ncell),err_nrmgradpoty(ncell),&
          stat=res)
     if (res.ne.0) STOP "errore allocazione arrays errori locali"

     ! Compute the value of the exact solution in the nodes of the grid
     do i = 1,nnode
	u_ex = pot_exact(id_test,mu,lambda,&
		grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
        potx(i) = u_ex(1)
	poty(i) = u_ex(2)
     end do

     ! Compute the exact gradient in the cells of the grid
     ! bar_cell contains the coordinates of the center of mass of the cell
     do i = 1,ncell
        call gradpot_exact(id_test,mu,lambda,grid%bar_cell(1,i),&
             grid%bar_cell(2,i),grid%bar_cell(3,i),gradu_ex) 
        gradpotx(:,i) = gradu_ex(1,:)
        gradpoty(:,i) = gradu_ex(2,:)
     end do

     ! Compute the difference between numerical and exact solution 
     err_potx = solx - potx
     err_poty = soly - poty

     ! Compute the difference between the gradient of numerical and exact solution 
     err_gradpotx = gradsolx - gradpotx
     err_gradpoty = gradsoly - gradpoty

     ! Compute the euclidean norm of the gradient for each cell of the grid
     do i=1,grid%ncell
        err_nrmgradpotx(i) = dnrm2(grid%logical_dimension,&
		err_gradpotx(1:grid%logical_dimension,i),1)
        err_nrmgradpoty(i) = dnrm2(grid%logical_dimension,&
		err_gradpoty(1:grid%logical_dimension,i),1)
     end do

     ! L2 error solution and gradient previous iteration (coarse mesh) 
     if (igrid > 1 ) then
        l2errx_pot_before = l2errx_pot 
        l2erry_pot_before = l2erry_pot 
        l2errx_gradpot_before = l2errx_gradpot
        l2erry_gradpot_before = l2erry_gradpot
        h1errx_pot_before = h1errx_pot 
        h1erry_pot_before = h1erry_pot 
     end if

     ! Compute the L2-norm of the solution error
     err_potx = err_potx**2
     err_poty = err_poty**2
     l2errx_pot = sqrt(ddot(nnode, P1GALERKIN%basis_integral, 1, err_potx,1))
     l2erry_pot = sqrt(ddot(nnode, P1GALERKIN%basis_integral, 1, err_poty,1))
     
     ! Compute the L2-norm of the gradient of the solution error 
     ! normp_cell computes the p-norm of piecewise constant functions over triangles
     l2errx_gradpot = grid%normp_cell(2.0d0,err_nrmgradpotx)
     l2erry_gradpot = grid%normp_cell(2.0d0,err_nrmgradpoty)

     ! Compute the H1-norm of the error 
     h1errx_pot = sqrt(l2errx_pot**2 + l2errx_gradpot**2)
     h1erry_pot = sqrt(l2erry_pot**2 + l2erry_gradpot**2)

     ! Convergence rate
     if (igrid > 1) then
        ! Rate L2 convergence
        rateL2x = log(l2errx_pot_before/l2errx_pot)/log(two)
        rateL2y = log(l2erry_pot_before/l2erry_pot)/log(two)
        ! Rate H1 convergence
        rateH1x = log(h1errx_pot_before/h1errx_pot)/log(two)
        rateH1y = log(h1erry_pot_before/h1erry_pot)/log(two)
     end if

  ! --------------------------------------------------------------

     ! Write output
     if (igrid > 1) then
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,3X,E12.5,4(3X,F12.8),3X,I5.5)') &
        !write(stdout,'(1X,I4.2,2X,I8,1X,I8,5(3X,1PE12.5))') &
	      igrid-1, grid%nnode, grid%ncell, &
	      grid%meshpar(0),rateL2x,rateL2y,rateH1x,rateH1y, iter
        print*
     else
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,3X,E12.5,4(14X,A1),3X,I5.5)') &
	      igrid-1, grid%nnode, grid%ncell, &
	      grid%meshpar(0),'-', '-', '-', '-', iter
        print*
     end if

  ! --------------------------------------------------------------

     ! Deallocate arrays
     deallocate(potx,poty,gradpotx,gradpoty,err_potx,err_poty,&
          err_gradpotx,err_gradpoty,err_nrmgradpotx,err_nrmgradpoty,stat=res)
     if (res.ne.0) STOP "errore deallocazione pot, gradpot e errori"

     ! Deallocate arrays
     deallocate(diffusion_coeff,rhs,solx,soly,gradsolx,gradsoly,stat=res)
     if (res.ne.0) STOP "errore deallocazione rhs, sol e gradsol"
     
     ! Free memory - kill stiffness matrix and lapl_p1 class
     call saddle%kill(6)
     call stiff_matrix%kill(6)
     call B%kill(6)
     call BT%kill(6)
     call P1GALERKIN%kill(6)

  ! --------------------------------------------------------------

     ! Build a new refinement of the grid     
     if (igrid < nref + 1) then
	
        call subgrid%refine(stderr,grid)!flag_reorder=1)
        call subgrid%RCM_node_reorder(stderr)
        call subgrid%build_size_cell(0)
        call subgrid%build_normal_cell(0)
        call subgrid%build_edge_connection(0)
        call subgrid%build_nodebc()
        call subgrid%build_bar_cell()

	! Kill subgrid
        grid = subgrid
        call subgrid%kill(stderr)

     end if

  end do

  ! --------------------------------------------------------------

  ! Kill grid
  call grid%kill(6)
  call grid0%kill(6)
  call fgrid%kill(6)

  ! --------------------------------------------------------------

contains

  ! Description of the problem
  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if((1.le.id_test).and.(id_test.le.5)) then
	    str = 'Rectangle [0,1]x[0,1] with Neumann BCs and Lagrange multipliers'
    else
	    STOP "error: test id must be between 1 and  5"
    end if

  end function example_description

  ! Definition of the forcing function
  function forcing(id_test,mu,lambda,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double), intent(in) :: x,y,z
    real(kind=double) :: func(2)

    select case (id_test)
    case(1) 
       func(1) = 2*(lambda+2*mu)*pigreco*pigreco*sin(pigreco*y)*cos(pigreco*x)
       func(2) = 2*(lambda+2*mu)*pigreco*pigreco*cos(pigreco*y)*sin(pigreco*x)
    case(2) 
       func(1) = 8*mu*pigreco*pigreco*cos(two*pigreco*x)*cos(two*pigreco*y)
       func(2) = 8*mu*pigreco*pigreco*sin(two*pigreco*x)*sin(two*pigreco*y)
    case(3)
       func(1) = pigreco*pigreco*cos(pigreco*x)*(-(lambda+mu)*cos(pigreco*y)+&
	       (lambda+three*mu)*sin(pigreco*y))
       func(2) = pigreco*pigreco*sin(pigreco*x)*(+(lambda+mu)*cos(pigreco*y)+&
	       (lambda+three*mu)*sin(pigreco*y))
    case(4)
       func(1) = -pigreco*pigreco*((lambda+mu)*cos(pigreco*x)*cos(pigreco*y)-&
	       four*(lambda+three*mu)*cos(two*pigreco*x)*cos(two*pigreco*y))
       func(2) = +pigreco*pigreco*((lambda+three*mu)*sin(pigreco*x)*sin(pigreco*y)-&
	       four*(lambda+mu)*sin(two*pigreco*x)*sin(two*pigreco*y))
    case(5) 
       func(1) = + two*(lambda*((y**2)-one)+mu*(two*(y**2)+x**2-three)) -&
	       (lambda+mu)*pigreco*pigreco*cos(pigreco*x)*cos(pigreco*y)
       func(2) = +four*(lambda+mu)*(x*y) +& 
	       (lambda+three*mu)*pigreco*pigreco*sin(pigreco*x)*sin(pigreco*y)
    end select 
  
  end function forcing

  ! Definition of the exact solution
  function pot_exact(id_test,mu,lambda,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: x,y,z
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double) :: func(2)

    select case (id_test)
    case(1) 
       func(1) = cos(pigreco*x) * sin(pigreco*y)
       func(2) = sin(pigreco*x) * cos(pigreco*y)
    case(2) 
       func(1) = cos(two*pigreco*x) * cos(two*pigreco*y)
       func(2) = sin(two*pigreco*x) * sin(two*pigreco*y)
    case(3)
       func(1) = cos(pigreco*x) * sin(pigreco*y)
       func(2) = sin(pigreco*x) * sin(pigreco*y)
    case(4)
       func(1) = cos(two*pigreco*x) * cos(two*pigreco*y)
       func(2) = sin(pigreco*x) * sin(pigreco*y)
    case(5) 
       func(1) = x**2 + y**2 - (x**2) * (y**2) - 1
       func(2) = sin(pigreco*x) * sin(pigreco*y)
    end select

  end function pot_exact

  ! Definition of the gradient of the exact solution
  subroutine gradpot_exact(id_test,mu,lambda,x,y,z,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: x,y,z
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double), intent(out) :: func(2,2)

    select case (id_test)
    case(1) 
       func(1,1) = - pigreco * sin(pigreco*x) * sin(pigreco*y)
       func(1,2) = + pigreco * cos(pigreco*x) * cos(pigreco*y)
       func(2,1) = + pigreco * cos(pigreco*x) * cos(pigreco*y)
       func(2,2) = - pigreco * sin(pigreco*x) * sin(pigreco*y)
    case(2) 
       func(1,1) = - two*pigreco*sin(two*pigreco*x)*cos(two*pigreco*y)
       func(1,2) = - two*pigreco*cos(two*pigreco*x)*sin(two*pigreco*y)
       func(2,1) = + two*pigreco*cos(two*pigreco*x)*sin(two*pigreco*y)
       func(2,2) = + two*pigreco*sin(two*pigreco*x)*cos(two*pigreco*y)
    case(3)
       func(1,1) = - pigreco*sin(pigreco*x)*sin(pigreco*y)
       func(1,2) = + pigreco*cos(pigreco*x)*cos(pigreco*y)
       func(2,1) = + pigreco*cos(pigreco*x)*sin(pigreco*y)
       func(2,2) = + pigreco*sin(pigreco*x)*cos(pigreco*y)
    case(4)
       func(1,1) = - two*pigreco*sin(two*pigreco*x)*cos(two*pigreco*y)
       func(1,2) = - two*pigreco*cos(two*pigreco*x)*sin(two*pigreco*y)
       func(2,1) = + pigreco*cos(pigreco*x)*sin(pigreco*y)
       func(2,2) = + pigreco*sin(pigreco*x)*cos(pigreco*y)
    case(5)
       func(1,1) = + two*x - two*x*(y**2)
       func(1,2) = + two*y - two*(x**2)*y
       func(2,1) = + pigreco*cos(pigreco*x)*sin(pigreco*y)
       func(2,2) = + pigreco*sin(pigreco*x)*cos(pigreco*y)
    end select

  end subroutine gradpot_exact
  
  subroutine boundary_force(id_test,mu,lambda,x,y,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: x,y
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double), intent(out) :: func(4,2)

    func = zero
    select case (id_test)
    case(1) 
       ! Lato x = 0 e x = 1 (verticale)
       func(1,1) = zero
       func(1,2) = - two*mu*pigreco*cos(pigreco*y)
       ! Lato y = 0 e y = 1 (orizzontale)
       func(2,1) = - two*mu*pigreco*cos(pigreco*x)
       func(2,2) = zero
    end select 

  end subroutine boundary_force

SUBROUTINE BICGSTAB(NROW,SADDLE,F,TOLL,ITERMAX,ITER,Q,X)

IMPLICIT NONE

! VARIABILI DI INPUT
INTEGER, INTENT(IN) :: NROW
INTEGER, INTENT(IN) :: ITERMAX
real(kind=double), INTENT(IN) :: TOLL
real(kind=double), DIMENSION(:), INTENT(IN) :: F
!type(spmat), target :: matrix
type(saddlemat) :: saddle

! VARIABILI DI OUTPUT
INTEGER, INTENT(OUT) :: ITER 
REAL(kind=double), INTENT(OUT)  :: Q
REAL(kind=double), DIMENSION(:), INTENT(INOUT) :: X 

! VARIABILI LOCALI
INTEGER :: STATO, I, D
real(kind=double) :: RHO0, RHO1, NB, ALPHA, BETA
real(kind=double) :: W, RN, TT, TS, VR0
real(kind=double), DIMENSION(NROW+3) :: AX0, R0, R, P, V, S, T
real(kind=double) :: TIME, TIMEITER
! PUNTATORI LOCALI
INTEGER, POINTER :: IA(:), JA(:)
real(kind=double), POINTER :: SYSMAT(:)

D = NROW+3
! ----------------------------------------------------------------
! INIZIALIZZO IL VETTORE SOLUZIONE
DO I = 1,D
   X(I) = 0.0
END DO

! PRODOTTO MATRICE - VETTORE
CALL saddle%mxv(X,AX0)

DO I = 1,D
   R(I) = F(I) - AX0(I)
   R0(I) = R(I)
   P(I) = 0.
   V(I) = 0.
END DO

! CALCOLO NORMA^2 RESIDUO INIZIALE
CALL DOTPRODUCTOMP(R0,R,D,RN)
! NORMA^2 DEL RHS
CALL DOTPRODUCTOMP(F,F,D,NB)

RN = SQRT(RN)
NB = SQRT(NB)
Q = RN/NB
RHO0 = 1.
ALPHA = 1.
W = 1.
ITER = 0

! CALCOLO LA SOLUTIONE DEL SISTEMA LINEARE
DO WHILE ((ITER.LT.ITERMAX).AND.(Q.GT.TOLL))

   ! CONTO LE ITERAZIONI
   ITER = ITER + 1
  
   ! PRODOTTO SCALARE (rho)
   CALL DOTPRODUCTOMP(R0,R,D,RHO1)

   ! CALCOLO BETA
   BETA = (RHO1/RHO0)*(ALPHA/W)

   ! AGGIORNAMENTO VETTORE: CALCOLO P
   DO I = 1,D
      P(I) = R(I) + BETA*(P(I) - W*V(I))
   END DO

   ! PRODOTO MATRICE VETTORE (v=A*p)
   CALL saddle%mxv(P,V)

   ! PRODOTTO SCALARE
   CALL DOTPRODUCTOMP(V,R0,D,VR0)
   
   ! CALCOLO ALPHA
   ALPHA = RHO1/VR0

   ! AGGIORNAMENTO VETTORE
   DO I = 1,D
      S(I) = R(I) - ALPHA*V(I)
   END DO

   ! PRODOTTO MATRICE VETTORE (t=A*s)
   CALL saddle%mxv(S,T)

   ! DUE PRODDOTTI SCALARI
   CALL DOTPRODUCTOMP(T,T,D,TT)
   CALL DOTPRODUCTOMP(T,S,D,TS)

   ! OMEGA
   W = TS/TT

   DO I = 1,D
      ! AGGIORNAMENTO VETTORE SOLUZIONE NUMERICA
      X(I) = X(I) + ALPHA*P(I) + W*S(I)
      ! AGGIORNAMENTO VETTORE RESIDUO
      R(I) = S(I) - W*T(I)
   END DO

   ! CALCOLO LA NORMA DEL RESIDUO
   CALL DOTPRODUCTOMP(R,R,D,RN)

   ! CALCOLO Q: RESIDUO RELATIVO
   RN = SQRT(RN)
   Q = RN/NB

   ! AGGIORNO RHO
   RHO0 = RHO1

END DO

END SUBROUTINE BICGSTAB

SUBROUTINE DOTPRODUCTOMP(A,B,NTERMS,X) 

IMPLICIT NONE

! VARIABILI DI INPUT
INTEGER, INTENT(IN) :: NTERMS
REAL(kind=double), DIMENSION(:), INTENT(IN)  :: A, B

! VARIABILI DI OUTPUT
REAL(kind=double), INTENT(OUT) :: X

! VARIABILI LOCALI
INTEGER :: I

! CALCOLO IL PRODOTTO SCALARE TRA DUE ARRAY 
X = A(1)*B(1)
DO I=2,NTERMS
   X = X + A(I)*B(I)
END DO

END SUBROUTINE DOTPRODUCTOMP

end PROGRAM elasticityNeuLagrange
