!>---------------------------------------------------------------------
!> 
!> DESCRIPTION: 
!> Convergence test for Poisson Equation $-\Div K \nabla u  = f(x)$
!> on the unit square $[0,1] \times [0,1]$ with zero Neumann 
!> Boundary conditions. The right hand side vector is projected onto
!> the column space of the stiffness matrix that is positive semi-
!> definite (the nullspace is given by the constant vector). In this
!> case the Conjugate Gradient method can be used to solve the system.
!> The algorithm starts from a given grid and then refines nref times.
!> The user can choose between two different test problems.
!>
!> \author{Enrico Facca and Nicolò Crescenzio}
!>
!> REVISION HISTORY:
!> 20201106 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program laplace

  use omp_lib
  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  
  use TimeInputs, only : write_steady
    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       		    stdin  => input_unit, &
       		    stdout => output_unit, &
       		    stderr => error_unit  

  implicit none
  
  ! 00modGlobals.f90
  type(file)     :: fgrid, fgridout, fctrl, fout

  ! 49modAbstractGeometry.f90
  type(abs_simplex_mesh)     :: grid0, grid, subgrid

  ! 63modP1Galerkin.f90
  type(p1gal), target    :: lapl_p1

  ! dagmg_wrap.f90
  type(agmg_inv) :: multigrid_inverse

  ! Diffusion, right hand side and solution arrays 
  real(kind=double), allocatable :: diffusion_coeff(:), rhs(:), sol(:), f(:), u(:)
  
  ! Dirichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  ! P1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) ,gradsol(:,:)

  ! Exact solution and its gradient
  real(kind=double), allocatable :: pot(:), gradpot(:,:)

  ! Error pot and its gradient
  real(kind=double), allocatable :: err_pot(:), err_gradpot(:,:), err_nrmgradpot(:)

  ! Errror norm
  real(kind=double)  :: l2err_pot, l2err_gradpot ,l2err_pot_before, l2err_gradpot_before,&
	  		h1err_pot, h1err_pot_before, rateL2, rateH1

  ! Euclidian norm
  real(kind=double) :: dnrm2, ddot

  ! Character variables
  character(len=256) :: input, fngrid, fnctrl, fnctrl_in, fname, gridname, tail

  ! 22modSparseMatrix.f90
  type(spmat)    :: stiff_matrix
  
  ! 20modSimpleMatrix.f90
  type(zeromat) :: zeromat1,zeromat2
  type(zeromat) :: eye1,eye2

  type(add_linop) :: sum1,sum2
  
  ! 12modLinearSolver.f90
  type(input_solver) :: ctrl
  type(output_solver):: info,info1
  
  ! 44modStdSparsePrec.f90
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! Local variables
  logical :: rc
  integer :: res,nterm,ndof,i,j,inod,ndir, k
  integer :: nnode, ncell, gridnumber
  integer :: nref
  integer :: igrid,id_test_read,id_test
  real(kind=double), pointer :: nodal_area(:)
  real(kind=double) :: time, sum_rhs, sum_area, ratio_rhs_area, dot_sol_area, ratio_dot_area

  ! --------------------------------------------------------------

  ! Read number of refinements
  call getarg(1,input)
  read(input,*,iostat=res) nref
  if (res.ne.0) nref = 0

  ! Read id starting grid
  call getarg(2,input)
  read(input,*,iostat=res) gridnumber
  if (res.ne.0) gridnumber = 0

  ! Read id_test
  call getarg(3,input)
  read(input,*,iostat=res) id_test
  if ((id_test.lt.1).or.(id_test.gt.2)) then
     id_test = 1
  end if

  print*
  write(stdout,*) '**************** TEST CASE ***********************'
  write(stdout,*) 'Domain: [0,1]x[0,1] unit square'
  write(stdout,*) 'PDE: Poisson Equation'
  write(stdout,*) 'Boundary conditions: zero Neumann BCs'
  write(stdout,*) 'RHS projected onto the column space of the stiffness matrix'
  write(stdout,*) 'Solution projected onto the space of zero average functions'
  write(stdout,'(1X,A,I1)') 'Starting from grid: ', gridnumber
  write(stdout,'(1X,A,I0.2)') 'Test ID = ', id_test
  write(stdout,'(1X,A,I0.2)') 'Total mesh refinements = ',nref
  write(stdout,*) '**************************************************'

  ! --------------------------------------------------------------

  ! File name with linear solver controls
  fnctrl = 'inputs/linear_solver.ctrl'
  ! Read from file 'linear_solver.ctrl'
  call fctrl%init(stderr,etb(fnctrl),8,'in')
  ! read_input_solver
  call ctrl%read(stderr,fctrl)
  ! Linear solver approach
  read(fctrl%lun,*) ctrl%approach 
  ! Kill variable file
  call fctrl%kill(stderr)
  
  print*
  write(stdout,*) '************ INFO LINEAR SOLVER ******************'
  call ctrl%info(0)
  write(stdout,*) '**************************************************'
  print*

  ! --------------------------------------------------------------

  ! Read mesh data from file 
  write(gridname,'(A,I1,A)') 'mesh',gridnumber,'.dat'
  fngrid = etb('inputs/'//etb(gridname))
  call fgrid%init(6,etb(fngrid),20,'in')
  !call fgrid%info(0)
  call grid0%read_mesh(0, fgrid)
  call grid0%build_size_cell(0)
  call grid0%build_normal_cell(0)
  call grid0%build_edge_connection(0)
  call grid0%build_nodebc()
  call grid0%build_bar_cell()
  !call grid0%info(0)

  grid = grid0

  ! --------------------------------------------------------------

  ! At each mesh refinement write data
  print*
  write(*,*) 'nref|   #nodes   #cells       mesh par       &
	  L2-error       H1-error        rate L2        rate H1'
  write(stdout,'(1X,98("-"))') 
 
  ! Loop over nref
  do igrid = 1, nref+1

     write(tail,'(a,I0.3,a)') 'ref',igrid-1,'.dat'
     
     fname = etb('grid'//etb(tail))
     call fgridout%init(stderr,fname,8,'out')
     call grid%write_mesh(stderr, fgridout)
     call fgridout%kill(stderr)

     ! Number of nodes
     nnode = grid%nnode
     ! Number of triangles
     ncell = grid%ncell

     ! Allocate space for diffusion coefficient 
     allocate(diffusion_coeff(ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'array diffusion_coeff',res)
     ! Give a value to cond
     diffusion_coeff = one

     ! P1 galerkin for Laplace Equation
     call lapl_p1%init(6, grid)

     ! Non zero terms in the stiffness matrix
     nterm = lapl_p1%nterm_csr
     ! Number of degrees of freedom
     ndof = lapl_p1%grid%nnode

     ! Initialization stiffness matrix (type SPMAT)
     call stiff_matrix%init(6,&
          ndof, ndof, nterm,&
          storage_system='csr',&
          is_symmetric=.true.)

     ! Initialization matrices of type ZEROMAT
     call zeromat1%init(ndof)
     call zeromat2%init(ndof)
     call eye1%init(ndof)
     call eye2%init(ndof)
     
     ! Build stiffness matrix for Poisson Equation
     call lapl_p1%build_stiff(6, 'csr', diffusion_coeff, stiff_matrix)

  ! --------------------------------------------------------------

     ! Allocate space for rhs, sol, gradsol
     allocate(rhs(ndof),sol(ndof),&
		  gradsol(grid%logical_dimension,ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays rhs,sol,gradsol ',res)

     ! Initialize rhs, sol, gradsol
     rhs = zero
     sol = zero
     gradsol = zero

     ! Create right hand side vector $rhs(i)=\int f \phi_i$
     ! basis_integral: integral of the basis function (computed in the constructor)
     ! forcing: forcing function defined in this file
     do i = 1,ndof
        rhs(i) = lapl_p1%basis_integral(i)* &
             forcing(id_test,grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
     end do
     
     ! Write arrays ia and ja in a file
     !open(9,file='stiffness_preBCs.dat')
     !call stiff_matrix%write(9)
     !close(9)

     ! Write rhs vector in a file
     !open(9,file='rhs_preBCs.dat')
     !do i=1,nnode
     !   write(9,*) i, rhs(i)
     !end do
     !close(9)
     
  ! --------------------------------------------------------------
     
     ! Pointer to nodal area
     nodal_area => lapl_p1%basis_integral

	  sum_rhs = zero
	  sum_area = zero

	  do i = 1,nnode
		  sum_rhs  = sum_rhs  + rhs(i)
		  sum_area = sum_area + nodal_area(i)
	  end do

	  ratio_rhs_area = sum_rhs/sum_area

	  do i = 1,nnode
		  rhs(i) = rhs(i) - ratio_rhs_area*nodal_area(i)
	  end do

  ! --------------------------------------------------------------

     ! Solution of the linear system 
     !write(*,*) ' solve linear system',etb(ctrl%approach)

     ! Choose how to solve the system
     select case (ctrl%approach)
     case('MG') 
        call multigrid_inverse%init(stderr,stiff_matrix,ctrl)
        call multigrid_inverse%Mxv(rhs,sol)
        call multigrid_inverse%info_solver%info(6)
        call multigrid_inverse%kill(stderr)
     case ('ITERATIVE')
        !write(*,*) 'call sum'
        stiff_matrix%name='A'
        !call sum1%info(6)
        !call zeromat1%info(6)
        !write(*,*) sum1%type,sum1%is_symmetric

        !call ctrl_prec%init(stderr,'IC',n_fillin=30,tol_fillin=1d-3)
        call ctrl_prec%init(stderr,'identity')
        call prec_stiff%init(stderr, i,ctrl_prec,ndof, stiff_matrix)
        call info%init()
        call linear_solver(stiff_matrix,rhs,sol,info,ctrl,prec_stiff)
        call ctrl_prec%kill()
        call prec_stiff%kill(stderr)
     !   call info%info(stdout)
     !   call info%time(stdout)
        call info%kill()
     end select
     
  ! --------------------------------------------------------------

     ! Compute projection for the solution
     dot_sol_area = zero

	  do i = 1,nnode
		  dot_sol_area = dot_sol_area + sol(i)*nodal_area(i)
	  end do

	  ratio_dot_area = dot_sol_area/sum_area

	  do i = 1,nnode
		  sol(i) = sol(i) - ratio_dot_area
	  end do

     ! Deassociate pointer to nodal area
     nodal_area => null()

     ! Write solution vector in a file
     open(9,file='solBochev.dat')
	  write(9,*) nnode
     do i=1,nnode
        write(9,'(F25.20)') sol(i)
     end do
     close(9)
     
  ! --------------------------------------------------------------

     ! Allocate space for reference solution and work arrays 
     allocate(&
          pot(ndof),&
          gradpot(grid%logical_dimension,ncell),&
          err_pot(ndof),&
          err_gradpot(grid%logical_dimension,ncell),&
          err_nrmgradpot(ncell),&
          stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays pot gradpot',res)

     ! Compute the gradient of the numerical solution 
     call lapl_p1%eval_grad(sol,gradsol)

     ! Compute the exact solution in the nodes of the grid
     do i=1,ndof
        pot(i) = pot_exact(id_test, grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
     end do

     ! Compute the exact gradient in the cells of the grid
     ! bar_cell contains the coordinates of the center of mass of the cell
     do i = 1,ncell
        call gradpot_exact(id_test,&
             grid%bar_cell(1,i),&
             grid%bar_cell(2,i),&
             grid%bar_cell(3,i),&
             grid%logical_dimension, gradpot(1:grid%logical_dimension,i))
     end do

     ! Compute the difference between numerical and exact solution 
     err_pot = sol - pot
     
     ! Compute the difference between the gradient of numerical and exact solution 
     err_gradpot = gradsol - gradpot

     ! Compute the euclidean norm of the gradient for each cell of the grid
     do i=1,grid%ncell
        err_nrmgradpot(i) = dnrm2(grid%logical_dimension,&
		err_gradpot(1:grid%logical_dimension,i),1)
     end do

     ! L2 error solution and gradient previous iteration (coarse mesh) 
     if (igrid > 1 ) then
        l2err_pot_before = l2err_pot 
        l2err_gradpot_before = l2err_gradpot
        h1err_pot_before = h1err_pot 
     end if

     ! Compute the L2-norm of the solution error
     err_pot = err_pot**2
     l2err_pot = sqrt(ddot(ndof, lapl_p1%basis_integral, 1, err_pot,1))
     
     ! Compute the L2-norm of the gradient of the solution error 
     ! normp_cell computes the p-norm of piecewise constant functions over triangles
     l2err_gradpot = grid%normp_cell(2.0d0,err_nrmgradpot)

     ! Compute the H1-norm of the error 
     h1err_pot = sqrt(l2err_pot**2 + l2err_gradpot**2)

     ! Convergence rate
     if (igrid > 1) then
        ! Rate L2 convergence
        rateL2 = log(l2err_pot_before/l2err_pot)/log(two)
        ! Rate H1 convergence
        rateH1 = log(h1err_pot_before/h1err_pot)/log(two)
     end if

  ! --------------------------------------------------------------

     ! Write output
     if (igrid > 1) then
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,5(3X,1PE12.5))') &
	      igrid-1, grid%nnode, grid%ncell, &
	      grid%meshpar(0),l2err_pot, h1err_pot, rateL2, rateH1 
        print*
     else
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,3(3X,1PE12.5),14X,A1,14X,A1)') &
	      igrid-1, grid%nnode, grid%ncell, &
	      grid%meshpar(0),l2err_pot, h1err_pot, '-', '-'
        print*
     end if

  ! --------------------------------------------------------------

     ! Deallocate arrays
     deallocate(&
          pot,&
          gradpot,&
          err_pot,&
          err_gradpot,&
          err_nrmgradpot,&
          stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays pot gradpot',res)

     ! Deallocate arrays
     deallocate(diffusion_coeff,rhs,sol,gradsol,stat=res)
     if(res .ne. 0) rc = IOerr(6, err_dealloc , 'Laplace main',&
          'arrays diffusion_coeff, rhs, sol',res)
     
     ! Free memory - kill stiffness matrix and lapl_p1 class
     call stiff_matrix%kill(6)
     call lapl_p1%kill(6)

  ! --------------------------------------------------------------

     ! Build a new refinement of the grid     
     if (igrid < nref + 1) then
	
        call subgrid%refine(stderr,grid)!flag_reorder=1)
        call subgrid%RCM_node_reorder(stderr)
        call subgrid%build_size_cell(0)
        call subgrid%build_normal_cell(0)
        call subgrid%build_edge_connection(0)
        call subgrid%build_nodebc()
        call subgrid%build_bar_cell()

        !call fgridout%init(stderr,'subgrid.out',8,'out')
        !call grid%write_mesh(stderr, fgridout)
        !call fgridout%kill(stderr)
     
	! Kill subgrid
        grid = subgrid
        call subgrid%kill(stderr)

     end if

  end do

  ! Kill grid
  call grid%kill(6)
  call grid0%kill(6)
  call fgrid%kill(6)

  ! --------------------------------------------------------------

contains

  ! Description of the problem
  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if ((id_test.eq.1).or.(id_test.eq.2)) then
       str = 'Rectangle [0,1]x[0,1] with homogeneous Neumann BCs \n &
			     Solution is computed projecting the rhs onto the columns space of the matrix'
    else
       STOP "errore"
    end if 
  
  end function example_description

  ! Definition of the forcing function
  function forcing(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    select case (id_test)
    case(1)
	    func = 8 * pigreco**2 * cos(two*pigreco*x) * cos(two*pigreco*y) 
    case(2)
	    func = two*pigreco*cos(two*pigreco*y)*&
		    (two*pigreco*(one+x**2)*cos(pigreco*x**2)+sin(pigreco*x**2))
    end select
  
  end function forcing

  ! Definition of the exact solution
  function pot_exact(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    select case (id_test)
    case(1)
	    func = cos(two*pigreco*x) * cos(two*pigreco*y)
    case(2)
	    func = cos(pigreco*x*x)*cos(two*pigreco*y) 
    end select

  end function pot_exact

  ! Definition of the gradient of the exact solution
  subroutine gradpot_exact(id_test,x,y,z,logical_dimension,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    integer,           intent(in):: logical_dimension
    real(kind=double), intent(out) :: func(logical_dimension)

    select case (id_test)
    case(1)
            func(1) =  - two * pigreco * sin(two*pigreco*x) * cos(two*pigreco*y)
            func(2) =  - two * pigreco * cos(two*pigreco*x) * sin(two*pigreco*y) 
    case(2)
	    func(1) = - two*pigreco*x*sin(pigreco*x*x)*cos(two*pigreco*y)
	    func(2) = - two*pigreco*cos(pigreco*x*x)*sin(two*pigreco*y)
    end select

  end subroutine gradpot_exact
  
  
end PROGRAM laplace
