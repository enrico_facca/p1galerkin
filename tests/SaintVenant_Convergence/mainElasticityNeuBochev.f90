!>---------------------------------------------------------------------
!> 
!> DESCRIPTION: 
!> Convergence test for Saint Venant Equation $-\Div \sigma (u)  = f(x)$
!> on the unit square $[0,1] \times [0,1]$ with Neumann Boundary 
!> conditions. The right hand side vector is projected onto the 
!> column space of the stiffness matrix that is positive semi-definite 
!> (the nullspace is given by rigid motions, dimension=3). In this
!> case the Conjugate Gradient method can be used to solve the system.
!> The algorithm starts from a given grid and then refines nref times.
!>
!> \author{Enrico Facca and Nicolò Crescenzio}
!>
!> REVISION HISTORY:
!> 20201108 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program elasticityNeu

  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  use SaddlePointMatrix
  
  use TimeInputs, only : write_steady
    
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       		        stdin  => input_unit, &
       		        stdout => output_unit, &
       		        stderr => error_unit  

  implicit none
  
  ! 00modGlobals.f90
  type(file) :: fgrid, fgridout, fctrl, fout

  ! 49modAbstractGeometry.f90
  type(abs_simplex_mesh), target :: grid0, grid, subgrid

  ! 63modP1Galerkin.f90
  type(p1gal), target :: P1GALERKIN

  ! dagmg_wrap.f90
  type(agmg_inv) :: multigrid_inverse

  ! Diffusion, right hand side and solution arrays 
  real(kind=double), allocatable :: diffusion_coeff(:) 
  real(kind=double), allocatable :: rhsx(:), rhsy(:), rhs(:), fx(:), fy(:)
  real(kind=double), allocatable :: rotx(:), roty(:) 
  real(kind=double), allocatable :: solx(:), soly(:), sol(:), ux(:), uy(:)
  
  ! P1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) , gradsolx(:,:), gradsoly(:,:)

  ! Exact solution and its gradient
  real(kind=double), allocatable :: potx(:), gradpotx(:,:)
  real(kind=double), allocatable :: poty(:), gradpoty(:,:)

  ! Error pot and its gradient
  real(kind=double), allocatable :: err_potx(:), err_gradpotx(:,:), err_nrmgradpotx(:)
  real(kind=double), allocatable :: err_poty(:), err_gradpoty(:,:), err_nrmgradpoty(:)

  ! Errror norm
  real(kind=double) :: l2errx_pot, l2errx_gradpot ,l2errx_pot_before, l2errx_gradpot_before
  real(kind=double) :: l2erry_pot, l2erry_gradpot ,l2erry_pot_before, l2erry_gradpot_before
  real(kind=double) :: h1errx_pot, h1errx_pot_before, h1erry_pot, h1erry_pot_before
  real(kind=double) :: rateL2x, rateH1x, rateL2y, rateH1y

  ! Euclidian norm
  real(kind=double) :: dnrm2, ddot

  ! Energy density
  real(kind=double), allocatable :: energy2(:), energy(:), gradsol(:,:)

  ! Character variables
  character(len=256) :: input, fngrid, fnctrl, fnctrl_in, fname, gridname, tail

  ! 22modSparseMatrix.f90
  type(spmat) :: stiff_matrix
  
  ! 20modSimpleMatrix.f90
  type(zeromat) :: zeromat1,zeromat2

  ! 09modLinearOperator.f90
  type(add_linop) :: sum1,sum2
  
  ! 12modLinearSolver.f90
  type(input_solver) :: ctrl
  type(output_solver) :: info,info1
  
  ! 44modStdSparsePrec.f90
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! Variabili nuovo pattern
  integer :: nz
  integer :: dofsxnode = 2

  ! Lame' parameters
  real(kind=double), parameter :: mu = 1
  real(kind=double), parameter :: lambda = 1

  ! Local variables
  logical :: rc
  integer :: res, ndof, i, j, k, gridnumber 
  integer :: nref, icell, iloc
  integer :: igrid, id_test
  real(kind=double) :: ledge, q, dotxy
  real(kind=double) :: fext(2), u_ex(2), gradu_ex(2,2), forceboundary(4,2)
  real(kind=double) :: lx, ly, rx, ry, ratio_fx_area, ratio_fy_area, sumfx, sumfy, sum_area
  real(kind=double) :: ex, ey, exy, eyx
  real(kind=double) :: time
  integer :: nthreads

  ! Pointers
  integer, pointer :: nterm, nnode, ncell, nnodeincell
  integer, pointer :: e1, e2, inode
  integer, pointer, dimension(:) :: topol
  real(kind=double), pointer :: xn1, xn2, yn1, yn2
  real(kind=double), dimension(:), pointer :: area, xcoord, ycoord

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

  ! Read number of refinements
  call getarg(1,input)
  read(input,*,iostat=res) nref
  if (res.ne.0) nref = 0

  ! Read id starting grid
  call getarg(2,input)
  read(input,*,iostat=res) gridnumber
  if (res.ne.0) gridnumber = 0

  ! Read id_test
  call getarg(3,input)
  read(input,*,iostat=res) id_test
  if (id_test.ne.1) then
     id_test = 1
  end if

  print*
  write(stdout,*) '**************** TEST CASE ***********************'
  write(stdout,*) 'Domain: [0,1]x[0,1] unit square'
  write(stdout,*) 'PDE: Elasticity Equation'
  write(stdout,*) 'Boundary conditions: Neumann BCs'
  write(stdout,*) 'Solution obtained using the method described by Bochev'
  write(stdout,'(1X,A,I1)') 'Starting from grid: ', gridnumber
  write(stdout,'(1X,A,I0.2)') 'Test ID = ', id_test
  write(stdout,'(1X,A,I0.2)') 'Total mesh refinements = ',nref
  write(stdout,*) '**************************************************'

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

  ! File name with linear solver controls
  fnctrl = 'inputs/linear_solver.ctrl'
  ! Read from file 'linear_solver.ctrl'
  call fctrl%init(stderr,etb(fnctrl),8,'in')
  ! read_input_solver
  call ctrl%read(stderr,fctrl)
  ! Linear solver approach
  read(fctrl%lun,*) ctrl%approach 
  ! Kill variable file
  call fctrl%kill(stderr)
  
  print*
  write(stdout,*) '************ INFO LINEAR SOLVER ******************'
  call ctrl%info(0)
  write(stdout,*) '**************************************************'
  print*

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

  ! Read mesh data from file 
  write(gridname,'(A,I1,A)') 'mesh',gridnumber,'.dat'
  fngrid = etb('inputs/'//etb(gridname))
  call fgrid%init(6,etb(fngrid),20,'in')
  !call fgrid%info(0)
  call grid0%read_mesh(0, fgrid)
  call grid0%build_size_cell(0)
  call grid0%build_normal_cell(0)
  call grid0%build_edge_connection(0)
  call grid0%build_nodebc()
  call grid0%build_bar_cell()
  !call grid0%info(0)

  grid = grid0

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

  ! At each mesh refinement write data
  print*
  write(*,*) 'nref|   #nodes   #cells       mesh par       &
	  rate-L2x       rate-L2y       rate-H1x       rate-H1y'
  write(stdout,'(1X,98("-"))') 
 
  ! Loop over nref
  do igrid = 1, nref+1

     write(tail,'(a,I0.3,a)') 'ref',igrid-1,'.dat'
     
     fname = etb('grid'//etb(tail))
     call fgridout%init(stderr,fname,8,'out')
     call grid%write_mesh(stderr, fgridout)
     call fgridout%kill(stderr)

     ! Number of nodes grid
     nnode => grid%nnode

     ! Number of cells grid
     ncell => grid%ncell

     ! Number of node in cell
     nnodeincell => grid%nnodeincell

     ! Allocate space for diffusion coefficient 
     allocate(diffusion_coeff(grid%ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'array diffusion_coeff',res)
     ! Give a value to cond
     diffusion_coeff = one

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! P1 galerkin for Laplace Equation
     call P1GALERKIN%init(6, grid)

     ! Number of degrees of freedom 
     ! (= matrix rows/cols and unknowns)
     ndof = dofsxnode*nnode

     ! Number of non-zero terms
     nterm => P1GALERKIN%nterm_saint_venant_csr

     ! Initialization stiffness matrix (type SPMAT)
     call stiff_matrix%init(6,ndof, ndof, nterm,&
          storage_system='csr',is_symmetric=.true.)

     ! Build stiffness matrix saint venant equation
     call P1GALERKIN%build_stiff_saint_venant2D(6,&
	     diffusion_coeff,mu,lambda,stiff_matrix)

     !open(9,file='stiffness.dat')
     !call stiff_matrix%write(9,'matlab')
     !close(9)

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Allocate space for rhs
     allocate(rhsx(nnode),rhsy(nnode),stat=res)
     if (res.ne.0) STOP "errore allocazione rhs"

     ! Initialize array
	  rhsx = zero
	  rhsy = zero
     
     ! Create right hand side vector $rhs(i)=\int f \phi_i$
     ! basis_integral: integral of the basis function 
     ! forcing: forcing function defined in this file
     do i = 1,nnode
		  fext = forcing(id_test,mu,lambda,&
			  grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
        rhsx(i) = P1GALERKIN%basis_integral(i)*fext(1)
        rhsy(i) = P1GALERKIN%basis_integral(i)*fext(2)
     end do

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Set Neumann boundary conditions
     ! Loop over boundary edges
     do i = 1,grid%nedge_bc
        e1 => grid%edge_iside(1,i)
        e2 => grid%edge_iside(2,i)
		  xn1 => grid%coord(1,e1)
		  xn2 => grid%coord(1,e2)
		  yn1 => grid%coord(2,e1)
		  yn2 => grid%coord(2,e2)
		  ! Lati orizzontali dominio quadrato
		  if (yn1.eq.yn2) then
	        ledge = abs(xn1-xn2)
	   	  call boundary_force(id_test,mu,lambda,xn1,yn1,forceboundary)
	   	  rhsx(e1) = rhsx(e1) + 0.5*ledge*forceboundary(2,1)
	   	  call boundary_force(id_test,mu,lambda,xn2,yn2,forceboundary)
	   	  rhsx(e2) = rhsx(e2) + 0.5*ledge*forceboundary(2,1)
		  end if
		  ! Lati verticali dominio quadrato
		  if (xn1.eq.xn2) then
		     ledge = abs(yn1-yn2)
			  call boundary_force(id_test,mu,lambda,xn1,yn1,forceboundary)
			  rhsy(e1) = rhsy(e1) + 0.5*ledge*forceboundary(1,2)
			  call boundary_force(id_test,mu,lambda,xn2,yn2,forceboundary)
			  rhsy(e2) = rhsy(e2) + 0.5*ledge*forceboundary(1,2)
		  end if
     end do

     open(9,file='rhs_preBCs.dat')
     do i=1,nnode
        write(9,*) i, rhsx(i), rhsy(i)
     end do
     close(9)

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

	  ! Compute \int_{\Omega} \partial_x \varphi_{i} dx
     ! Compute \int_{\Omega} \partial_y \varphi_{i} dx
     allocate(rotx(nnode),roty(nnode),stat=res)
     if (res.ne.0) STOP "errore allocazione"

     rotx = zero
     roty = zero

     do icell = 1,ncell
	     do iloc = 1,nnodeincell
   	     inode => grid%topol(iloc,icell)
	        rotx(inode) = rotx(inode) + &
		        grid%size_cell(icell)*P1GALERKIN%grad_basis(1,iloc,icell)
	        roty(inode) = roty(inode) - &
		        grid%size_cell(icell)*P1GALERKIN%grad_basis(2,iloc,icell)
        end do
     end do

     ! Pointer to nodal area
     area => P1GALERKIN%basis_integral

     ! Pointer to nodal coordinates
     xcoord => grid%coord(1,:)
     ycoord => grid%coord(2,:)

	  ! Area \Omega
     sum_area = zero

	  do i = 1,nnode
		  sum_area = sum_area + area(i)
	  end do

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Allocate local variables
     allocate(fx(nnode),fy(nnode),stat=res)
     if (res.ne.0) STOP "errore allocazione fx fy"

	  sumfx = zero
	  sumfy = zero

     do i = 1,nnode
		  sumfx = sumfx + rhsx(i)
		  sumfy = sumfy + rhsy(i)
	  end do

	  ratio_fx_area = sumfx/sum_area
	  ratio_fy_area = sumfy/sum_area

	  dotxy = zero

	  do i = 1,nnode
		  fx(i) = rhsx(i) - ratio_fx_area*area(i)
		  fy(i) = rhsy(i) - ratio_fy_area*area(i)
		  dotxy = dotxy + ycoord(i)*fx(i) - xcoord(i)*fy(i)
	  end do

	  do i = 1,nnode
		  rhsx(i) = fx(i) + (0.5/sum_area)*dotxy*roty(i)
		  rhsy(i) = fy(i) + (0.5/sum_area)*dotxy*rotx(i)
	  end do

     open(9,file='rhs_postBCs.dat')
     do i=1,nnode
        write(9,*) i, rhsx(i), rhsy(i)
     end do
     close(9)

	  allocate(rhs(ndof),stat=res)
	  if (res.ne.0) STOP "errore allocazione rhs"

	  do i = 1,nnode
		  rhs(2*i-1) = rhsx(i)
		  rhs(2*i  ) = rhsy(i)
	  end do

     deallocate(rhsx,rhsy,fx,fy,stat=res)
     if (res.ne.0) STOP "errore deallocazione"

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Allocazione arrays soluzione 
     allocate(sol(ndof),ux(nnode),uy(nnode),&
	     solx(nnode),soly(nnode),stat=res)
     if (res.ne.0) STOP "errore allocazione arrays sol"

     ! Initialize sol(x/y)
     sol  = zero
     solx = zero
     soly = zero
     ux   = zero
     uy   = zero

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Solution of the linear system 
     ! Choose how to solve the system
     select case (ctrl%approach)
     case('MG') 
        call multigrid_inverse%init(stderr,stiff_matrix,ctrl)
        call multigrid_inverse%Mxv(rhs,sol)
        call multigrid_inverse%info_solver%info(6)
        call multigrid_inverse%kill(stderr)
     case ('ITERATIVE')
        !write(*,*) 'call sum'
        stiff_matrix%name='A'
        !call sum1%info(6)
        !call zeromat1%info(6)
        !write(*,*) sum1%type,sum1%is_symmetric
        !call ctrl_prec%init(stderr,'IC',n_fillin=30,tol_fillin=1d-3)
        call ctrl_prec%init(stderr,'identity')
        call prec_stiff%init(stderr, i,ctrl_prec,ndof, stiff_matrix)
        call info%init()
        call linear_solver(stiff_matrix,rhs,sol,info,ctrl,prec_stiff)
        call ctrl_prec%kill()
        call prec_stiff%kill(stderr)
     !   call info%info(stdout)
     !   call info%time(stdout)
        call info%kill()
     end select
     
  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Split solution into x and y component
     do i = 1,nnode
	     ux(i) = sol(2*i-1)
	     uy(i) = sol(2*i)
     end do

     open(9,file='sol_pre.dat')
     do i=1,nnode
        write(9,*) i, ux(i), uy(i)
     end do
     close(9)

     lx = zero
     ly = zero
     rx = zero
     ry = zero
     dotxy = zero
     
     do i = 1,nnode
	     dotxy = dotxy + 0.5*roty(i)*ux(i) + 0.5*rotx(i)*uy(i)
	     lx = lx + ux(i)*area(i)
	     ly = ly + uy(i)*area(i)
	     rx = rx + ycoord(i)*area(i)
	     ry = ry + xcoord(i)*area(i)
     end do

	  lx = lx/sum_area
	  ly = ly/sum_area
	  rx = rx/sum_area
	  ry = ry/sum_area

     ! Update solution
     do i = 1,nnode
	     solx(i) = ux(i) - lx + (dotxy/sum_area)*(+ycoord(i) - rx)
	     soly(i) = uy(i) - ly + (dotxy/sum_area)*(-xcoord(i) + ry)
     end do 

     open(9,file='solElBoc.dat')
	  write(9,*) ndof
     do i=1,nnode
        write(9,'(F25.20)') solx(i)
        write(9,'(F25.20)') soly(i)
     end do
     close(9)

     ! Free memory
     deallocate(ux,uy,sol,stat=res)
     if (res.ne.0) STOP "errore deallocazione ux, uy, sol"

     open(9,file='sol_post.dat')
     do i=1,nnode
        write(9,*) i, solx(i), soly(i)
     end do
     close(9)

     ! Deassociate pointers
     area   => null()
     xcoord => null()
     ycoord => null()

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Allocazione arrays gradiente soluzione 
     allocate(gradsolx(grid%logical_dimension,ncell),&
	     gradsoly(grid%logical_dimension,ncell),stat=res)
     if (res.ne.0) STOP "errore allocazione arrays gradsol"

     ! Initializes arrays
     gradsolx = zero
     gradsoly = zero

     ! Compute the gradient of the numerical solution
     call P1GALERKIN%eval_grad(solx,gradsolx)
     call P1GALERKIN%eval_grad(soly,gradsoly)

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

	  ! Allocate array for strain energy density
	  allocate(energy(ncell),stat=res)
	  if (res.ne.0) STOP "errore allocazione energy"

     ! Procedure for the evaluation of  
     ! $\mu|\epsilon (u)|^2 + \frac{\lambda}{2}|\div(u)|^2$
     ! over a cell of functions in $P1(\Tau(\Omega))$
	  call P1GALERKIN%eval_strain_energy(mu,lambda,solx,soly,energy)

	  open(9,file='energy.dat')
	  do i =1,ncell
		  write(9,*) energy(i)
	  end do
	  close(9)

	  ! Free memory
	  deallocate(energy,stat=res)
	  if (res.ne.0) STOP "errore deallocazione energy"

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Allocate space for reference solution and work arrays 
     allocate(potx(nnode),poty(nnode),&
          gradpotx(grid%logical_dimension,ncell),&
          gradpoty(grid%logical_dimension,ncell),&
          err_potx(nnode),err_poty(nnode),&
          err_gradpotx(grid%logical_dimension,ncell),&
          err_gradpoty(grid%logical_dimension,ncell),&
          err_nrmgradpotx(ncell),err_nrmgradpoty(ncell),&
          stat=res)
     if (res.ne.0) STOP "errore allocazione arrays errori locali"

     ! Compute the value of the exact solution in the nodes of the grid
     do i = 1,nnode
    	  u_ex = pot_exact(id_test,mu,lambda,&
		     grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
        potx(i) = u_ex(1)
	     poty(i) = u_ex(2)
     end do

     ! Compute the exact gradient in the cells of the grid
     ! bar_cell contains the coordinates of the center of mass of the cell
     do i = 1,ncell
        call gradpot_exact(id_test,mu,lambda,grid%bar_cell(1,i),&
             grid%bar_cell(2,i),grid%bar_cell(3,i),gradu_ex) 
        gradpotx(:,i) = gradu_ex(1,:)
        gradpoty(:,i) = gradu_ex(2,:)
     end do

     ! Compute the difference between numerical and exact solution 
     err_potx = solx - potx
     err_poty = soly - poty

     ! Compute the difference between the gradient of numerical and exact solution 
     err_gradpotx = gradsolx - gradpotx
     err_gradpoty = gradsoly - gradpoty

     ! Compute the euclidean norm of the gradient for each cell of the grid
     do i=1,grid%ncell
        err_nrmgradpotx(i) = dnrm2(grid%logical_dimension,&
		     err_gradpotx(1:grid%logical_dimension,i),1)
        err_nrmgradpoty(i) = dnrm2(grid%logical_dimension,&
		     err_gradpoty(1:grid%logical_dimension,i),1)
     end do

     ! L2 error solution and gradient previous iteration (coarse mesh) 
     if (igrid > 1 ) then
        l2errx_pot_before = l2errx_pot 
        l2erry_pot_before = l2erry_pot 
        l2errx_gradpot_before = l2errx_gradpot
        l2erry_gradpot_before = l2erry_gradpot
        h1errx_pot_before = h1errx_pot 
        h1erry_pot_before = h1erry_pot 
     end if

     ! Compute the L2-norm of the solution error
     err_potx = err_potx**2
     err_poty = err_poty**2
     l2errx_pot = sqrt(ddot(nnode, P1GALERKIN%basis_integral, 1, err_potx,1))
     l2erry_pot = sqrt(ddot(nnode, P1GALERKIN%basis_integral, 1, err_poty,1))
     
     ! Compute the L2-norm of the gradient of the solution error 
     ! normp_cell computes the p-norm of piecewise constant functions over triangles
     l2errx_gradpot = grid%normp_cell(2.0d0,err_nrmgradpotx)
     l2erry_gradpot = grid%normp_cell(2.0d0,err_nrmgradpoty)

     ! Compute the H1-norm of the error 
     h1errx_pot = sqrt(l2errx_pot**2 + l2errx_gradpot**2)
     h1erry_pot = sqrt(l2erry_pot**2 + l2erry_gradpot**2)

     ! Convergence rate
     if (igrid > 1) then
        ! Rate L2 convergence
        rateL2x = log(l2errx_pot_before/l2errx_pot)/log(two)
        rateL2y = log(l2erry_pot_before/l2erry_pot)/log(two)
        ! Rate H1 convergence
        rateH1x = log(h1errx_pot_before/h1errx_pot)/log(two)
        rateH1y = log(h1erry_pot_before/h1erry_pot)/log(two)
     end if

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Write output
     if (igrid > 1) then
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,3X,E12.5,4(3X,F12.8))') &
        !write(stdout,'(1X,I4.2,2X,I8,1X,I8,5(3X,1PE12.5))') &
	      igrid-1, grid%nnode, grid%ncell, &
	      grid%meshpar(0),rateL2x,rateL2y,rateH1x,rateH1y 
        print*
     else
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,3X,E12.5,4(14X,A1))') &
	      igrid-1, grid%nnode, grid%ncell, &
	      grid%meshpar(0),'-', '-', '-', '-'
        print*
     end if

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Deallocate arrays
     deallocate(rotx,roty,potx,poty,gradpotx,gradpoty,err_potx,err_poty,&
          err_gradpotx,err_gradpoty,err_nrmgradpotx,err_nrmgradpoty,stat=res)
     if (res.ne.0) STOP "errore deallocazione pot, gradpot e errori"

     ! Deallocate arrays
     deallocate(diffusion_coeff,rhs,solx,soly,gradsolx,gradsoly,stat=res)
     if (res.ne.0) STOP "errore deallocazione rhs, sol e gradsol"
     
     ! Free memory - kill stiffness matrix and lapl_p1 class
     call stiff_matrix%kill(6)
     call P1GALERKIN%kill(6)

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

     ! Build a new refinement of the grid     
     if (igrid < nref + 1) then
	
        call subgrid%refine(stderr,grid)!flag_reorder=1)
        call subgrid%RCM_node_reorder(stderr)
        call subgrid%build_size_cell(0)
        call subgrid%build_normal_cell(0)
        call subgrid%build_edge_connection(0)
        call subgrid%build_nodebc()
        call subgrid%build_bar_cell()

	! Kill subgrid
        grid = subgrid
        call subgrid%kill(stderr)

     end if

  end do

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------

  ! Kill grid
  call grid%kill(6)
  call grid0%kill(6)
  call fgrid%kill(6)

contains

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------
  ! Description of the problem
  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if((1.le.id_test).and.(id_test.le.5)) then
	    str = 'Rectangle [0,1]x[0,1] with Neumann BCs'
    else
	    STOP "error: test id must be between 1 and  5"
    end if

  end function example_description

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------
  ! Definition of the forcing function
  function forcing(id_test,mu,lambda,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double), intent(in) :: x,y,z
    real(kind=double) :: func(2)

    select case (id_test)
    case(1) 
       func(1) = 2*(lambda+2*mu)*pigreco*pigreco*sin(pigreco*y)*cos(pigreco*x)
       func(2) = 2*(lambda+2*mu)*pigreco*pigreco*cos(pigreco*y)*sin(pigreco*x)
    case(2) 
       func(1) = 8*mu*pigreco*pigreco*cos(two*pigreco*x)*cos(two*pigreco*y)
       func(2) = 8*mu*pigreco*pigreco*sin(two*pigreco*x)*sin(two*pigreco*y)
    case(3)
       func(1) = pigreco*pigreco*cos(pigreco*x)*(-(lambda+mu)*cos(pigreco*y)+&
	       (lambda+three*mu)*sin(pigreco*y))
       func(2) = pigreco*pigreco*sin(pigreco*x)*(+(lambda+mu)*cos(pigreco*y)+&
	       (lambda+three*mu)*sin(pigreco*y))
    case(4)
       func(1) = -pigreco*pigreco*((lambda+mu)*cos(pigreco*x)*cos(pigreco*y)-&
	       four*(lambda+three*mu)*cos(two*pigreco*x)*cos(two*pigreco*y))
       func(2) = +pigreco*pigreco*((lambda+three*mu)*sin(pigreco*x)*sin(pigreco*y)-&
	       four*(lambda+mu)*sin(two*pigreco*x)*sin(two*pigreco*y))
    case(5) 
       func(1) = + two*(lambda*((y**2)-one)+mu*(two*(y**2)+x**2-three)) -&
	       (lambda+mu)*pigreco*pigreco*cos(pigreco*x)*cos(pigreco*y)
       func(2) = +four*(lambda+mu)*(x*y) +& 
	       (lambda+three*mu)*pigreco*pigreco*sin(pigreco*x)*sin(pigreco*y)
    end select 
  
  end function forcing

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------
  ! Definition of the exact solution
  function pot_exact(id_test,mu,lambda,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: x,y,z
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double) :: func(2)

    select case (id_test)
    case(1) 
       func(1) = cos(pigreco*x) * sin(pigreco*y)
       func(2) = sin(pigreco*x) * cos(pigreco*y)
    case(2) 
       func(1) = cos(two*pigreco*x) * cos(two*pigreco*y)
       func(2) = sin(two*pigreco*x) * sin(two*pigreco*y)
    case(3)
       func(1) = cos(pigreco*x) * sin(pigreco*y)
       func(2) = sin(pigreco*x) * sin(pigreco*y)
    case(4)
       func(1) = cos(two*pigreco*x) * cos(two*pigreco*y)
       func(2) = sin(pigreco*x) * sin(pigreco*y)
    case(5) 
       func(1) = x**2 + y**2 - (x**2) * (y**2) - 1
       func(2) = sin(pigreco*x) * sin(pigreco*y)
    end select

  end function pot_exact

  ! --------------------------------------------------------------
  ! --------------------------------------------------------------
  ! Definition of the gradient of the exact solution
  subroutine gradpot_exact(id_test,mu,lambda,x,y,z,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: x,y,z
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double), intent(out) :: func(2,2)

    select case (id_test)
    case(1) 
       func(1,1) = - pigreco * sin(pigreco*x) * sin(pigreco*y)
       func(1,2) = + pigreco * cos(pigreco*x) * cos(pigreco*y)
       func(2,1) = + pigreco * cos(pigreco*x) * cos(pigreco*y)
       func(2,2) = - pigreco * sin(pigreco*x) * sin(pigreco*y)
    case(2) 
       func(1,1) = - two*pigreco*sin(two*pigreco*x)*cos(two*pigreco*y)
       func(1,2) = - two*pigreco*cos(two*pigreco*x)*sin(two*pigreco*y)
       func(2,1) = + two*pigreco*cos(two*pigreco*x)*sin(two*pigreco*y)
       func(2,2) = + two*pigreco*sin(two*pigreco*x)*cos(two*pigreco*y)
    case(3)
       func(1,1) = - pigreco*sin(pigreco*x)*sin(pigreco*y)
       func(1,2) = + pigreco*cos(pigreco*x)*cos(pigreco*y)
       func(2,1) = + pigreco*cos(pigreco*x)*sin(pigreco*y)
       func(2,2) = + pigreco*sin(pigreco*x)*cos(pigreco*y)
    case(4)
       func(1,1) = - two*pigreco*sin(two*pigreco*x)*cos(two*pigreco*y)
       func(1,2) = - two*pigreco*cos(two*pigreco*x)*sin(two*pigreco*y)
       func(2,1) = + pigreco*cos(pigreco*x)*sin(pigreco*y)
       func(2,2) = + pigreco*sin(pigreco*x)*cos(pigreco*y)
    case(5)
       func(1,1) = + two*x - two*x*(y**2)
       func(1,2) = + two*y - two*(x**2)*y
       func(2,1) = + pigreco*cos(pigreco*x)*sin(pigreco*y)
       func(2,2) = + pigreco*sin(pigreco*x)*cos(pigreco*y)
    end select

  end subroutine gradpot_exact
  
  ! --------------------------------------------------------------
  ! --------------------------------------------------------------
  subroutine boundary_force(id_test,mu,lambda,x,y,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: x,y
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double), intent(out) :: func(4,2)

    func = zero
    select case (id_test)
    case(1) 
       ! Lato x = 0 e x = 1 (verticale)
       func(1,1) = zero
       func(1,2) = - two*mu*pigreco*cos(pigreco*y)
       ! Lato y = 0 e y = 1 (orizzontale)
       func(2,1) = - two*mu*pigreco*cos(pigreco*x)
       func(2,2) = zero
    end select 

  end subroutine boundary_force

end PROGRAM elasticityNeu
