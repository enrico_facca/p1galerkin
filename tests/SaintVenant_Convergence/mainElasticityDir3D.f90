!>---------------------------------------------------------------------
!> 
!> DESCRIPTION: 
!> Convergence test for Saint Venant Equation $-\Div \sigma (u)  = f(x)$
!> on $[0,1] \times [0,1] \times [0,1]$ with and Dirichlet Boundary 
!> conditions. The algorithm starts from a given grid and then refines 
!> nref times. The user can choose between three different test problems.
!>
!> \author{Enrico Facca and Nicolò Crescenzio}
!>
!> REVISION HISTORY:
!> 20190327 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program elasticityDir

  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  
  use TimeInputs, only : write_steady
    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       		    stdin  => input_unit, &
       		    stdout => output_unit, &
       		    stderr => error_unit  

  implicit none
  
  ! 00modGlobals.f90
  type(file)     :: fgrid, fgridout, fctrl, fout

  ! 49modAbstractGeometry.f90
  type(abs_simplex_mesh), target :: grid0, grid, subgrid

  ! 63modP1Galerkin.f90
  type(p1gal)    :: P1GALERKIN

  ! dagmg_wrap.f90
  type(agmg_inv) :: multigrid_inverse

  ! Diffusion, right hand side and solution arrays 
  real(kind=double), allocatable :: diffusion_coeff(:), rhs(:), solx(:), soly(:), solz(:), sol(:)
  
  ! Dirichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  ! P1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) , gradsolx(:,:), gradsoly(:,:), gradsolz(:,:)

  ! Exact solution and its gradient
  real(kind=double), allocatable :: potx(:), gradpotx(:,:)
  real(kind=double), allocatable :: poty(:), gradpoty(:,:)
  real(kind=double), allocatable :: potz(:), gradpotz(:,:)

  ! Error pot and its gradient
  real(kind=double), allocatable :: err_potx(:), err_gradpotx(:,:), err_nrmgradpotx(:)
  real(kind=double), allocatable :: err_poty(:), err_gradpoty(:,:), err_nrmgradpoty(:)
  real(kind=double), allocatable :: err_potz(:), err_gradpotz(:,:), err_nrmgradpotz(:)

  ! Errror norm
  real(kind=double)  :: l2errx_pot, l2errx_gradpot ,l2errx_pot_before, l2errx_gradpot_before
  real(kind=double)  :: l2erry_pot, l2erry_gradpot ,l2erry_pot_before, l2erry_gradpot_before
  real(kind=double)  :: l2errz_pot, l2errz_gradpot ,l2errz_pot_before, l2errz_gradpot_before
  real(kind=double)  :: rateL2x, rateH1x, rateL2y, rateH1y, rateL2z, rateH1z
  real(kind=double)  :: h1errx_pot, h1errx_pot_before
  real(kind=double)  :: h1erry_pot, h1erry_pot_before 
  real(kind=double)  :: h1errz_pot, h1errz_pot_before 

  ! Euclidian norm
  real(kind=double) :: dnrm2, ddot

  ! Character variables
  character(len=256) :: input, fngrid, fnctrl, fnctrl_in, fname, tail

  ! 22modSparseMatrix.f90
  type(spmat)    :: stiff_matrix
  
  ! 20modSimpleMatrix.f90
  type(zeromat) :: zeromat1,zeromat2

  !type(add_linop) :: sum1,sum2
  
  ! 12modLinearSolver.f90
  type(input_solver) :: ctrl
  type(output_solver):: info,info1
  
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! Variabili nuovo pattern
  integer :: nz
  integer :: dofsxnode = 3

  ! Local variables
  logical :: rc
  integer :: res,nterm,ndof,i,j,inod,ndir, k
  integer :: nnode, ncell
  integer :: nref
  integer :: igrid,id_test_read,id_test
  real(kind=double) :: mu=1, lambda=1
  real(kind=double) :: f(3), u_ex(3), gradu_ex(3,3)
  real(kind=double), allocatable :: size_edge(:)
  integer, pointer :: ne1, ne2
  real(kind=double) :: meshpar, meshpar_old
  real(kind=double), pointer :: xe1, xe2, ye1, ye2, ze1, ze2

  ! --------------------------------------------------------------

  ! Number of refinements
  nref = 3

  !id_test = 1
  
  ! Read id_test
  call getarg(1,input)
  read(input,*,iostat=res) id_test
  if ((id_test.lt.1).or.(id_test.gt.3)) then
     id_test = 1
  end if


  print*
  write(stdout,*) '**************** TEST CASE ***********************'
  write(stdout,*) etb(example_description(id_test))
  write(stdout,'(1X,A,I0.2)') 'Test ID = ', id_test
  write(stdout,'(1X,A,I0.2)') 'Total mesh refinements = ',nref
  write(stdout,*) '**************************************************'

  ! --------------------------------------------------------------

  ! File name with linear solver controls
  fnctrl = 'inputs/linear_solver.ctrl'
  ! Read from file 'linear_solver.ctrl'
  call fctrl%init(stderr,etb(fnctrl),8,'in')
  ! read_input_solver
  call ctrl%read(stderr,fctrl)
  ! Linear solver approach
!  read(fctrl%lun,*) ctrl%approach 
  ! Kill variable file
  call fctrl%kill(stderr)
  
  print*
  write(stdout,*) '************ INFO LINEAR SOLVER ******************'
  call ctrl%info(0)
  write(stdout,*) '**************************************************'
  print*

  ! --------------------------------------------------------------

!	  ! Read mesh data from file 
!	  fngrid = 'inputs/mesh3D_1.dat'
!	  call fgrid%init(6,etb(fngrid),20,'in')
!!	  call fgrid%info(0)
!	  call grid0%read_mesh(0, fgrid)
!	  call grid0%build_size_cell(0)
!	  call grid0%build_normal_cell(0)
!	  call grid0%build_edge_connection(0)
!	  call grid0%build_face_connection(0)
!	  call grid0%build_nodebc()
!	  call grid0%build_bar_cell()
!!	  call grid0%info(0)
!
!	  grid = grid0

  ! --------------------------------------------------------------

  ! At each mesh refinement write data
  print*
  write(*,*) 'nref|   #nodes   #cells       mesh par       &
	  rate-L2x       rate-L2y       rate-L2z       rate-H1x       &
	  rate-H1y       rate-H1z'
  write(stdout,'(1X,128("-"))') 
 
  ! --------------------------------------------------------------

  ! Loop over nref
  do igrid = 1, nref+1

	  ! Read mesh data from file 
!     write(fngrid,'(a,i1,a)') './mesh',igrid,'.dat'
     write(fngrid,'(a,i1,a)') './inputs/mesh3D_',igrid,'.dat'
	  call fgrid%init(6,etb(fngrid),20,'in')
!	  call fgrid%info(0)
	  call grid%read_mesh(0, fgrid)
	  call grid%build_size_cell(0)
	  call grid%build_normal_cell(0)
	  call grid%build_edge_connection(0)
	  call grid%build_face_connection(0)
	  call grid%build_nodebc()
	  call grid%build_bar_cell()
!	  call grid0%info(0)

!     write(tail,'(a,I0.3,a)') 'ref',igrid,'.dat'
!     
!     fname = etb('grid'//etb(tail))
!     call fgridout%init(stderr,fname,8,'out')
!     call grid%write_mesh(stderr, fgridout)
!     call fgridout%kill(stderr)

     ! Number of nodes grid
     nnode = grid%nnode

     ! Number of cells grid
     ncell = grid%ncell

     ! Allocate space for diffusion coefficient 
     allocate(diffusion_coeff(grid%ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'array diffusion_coeff',res)
     ! Give a value to cond
     diffusion_coeff = one

     ! P1 galerkin for Laplace Equation
     call P1GALERKIN%init(6, grid)

     ! Compute new sparsity pattern and assembler 
     ! for a given value of dofsxnode
     !call topol_csr_multi_dofs(dofsxnode,P1GALERKIN)

     ! write ia 
!     open(9,file='ia_sv.dat')
!     write(9,*) (P1GALERKIN%ia_saint_venant_csr(i), i=1,size(P1GALERKIN%ia_saint_venant_csr,1))
!     close(9)

     ! write ia 
     !open(9,file='ia.dat')
     !write(9,*) (P1GALERKIN%ia_csr(i), i=1,size(P1GALERKIN%ia_csr,1))
     !close(9)

     ! write ja
!     open(9,file='ja_sv.dat')
!     write(9,*) (P1GALERKIN%ja_saint_venant_csr(i), i=1,size(P1GALERKIN%ja_saint_venant_csr,1))
!     close(9)

     ! write ja
     !open(9,file='ja.dat')
     !write(9,*) (P1GALERKIN%ja_csr(i), i=1,size(P1GALERKIN%ja_csr,1))
     !close(9)

     ! write trija
     !open(9,file='trija.dat')
     !do i=1,grid%ncell
     !	     do j = 1,6     
     !           write(9,*) (P1GALERKIN%assembler_csr(j,k,i), k=1,6)
     !	     end do
     !end do
     !close(9)

     ! Number of degrees of freedom 
     ! (= matrix rows/cols and unknowns)
     ndof = dofsxnode*nnode

     ! Number of non-zero terms
     nterm = P1GALERKIN%nterm_saint_venant_csr

     ! Initialization stiffness matrix (type SPMAT)
     call stiff_matrix%init(6,ndof, ndof, nterm,&
          storage_system='csr',is_symmetric=.true.)

     ! Build stiffness matrix saint venant equation
     call P1GALERKIN%build_stiff_saint_venant(6,&
	     diffusion_coeff,mu,lambda,stiff_matrix)

     ! write ia 
     !open(9,file='iastiff.dat')
     !write(9,*) (stiff_matrix%ia(i), i=1,size(stiff_matrix%ia,1))
     !close(9)

     ! write ja
     !open(9,file='jastiff.dat')
     !write(9,*) (stiff_matrix%ja(i), i=1,size(stiff_matrix%ja,1))
     !close(9)

     ! write diag
     !open(9,file='diagstiff.dat')
     !write(9,*) (stiff_matrix%ind_diag(i), i=1,size(stiff_matrix%ind_diag,1))
     !close(9)

     ! Write arrays ia and ja in a file
     !open(9,file='stiffness_preBC.dat')
     !call stiff_matrix%write(9)
     !close(9)

  ! --------------------------------------------------------------

     ! Allocate space for rhs
     allocate(rhs(ndof),stat=res)
     if (res.ne.0) STOP "errore allocazione rhs"

     ! Initialize array
     rhs  = zero
     
     ! Create right hand side vector $rhs(i)=\int f \phi_i$
     ! basis_integral: integral of the basis function (computed in the constructor)
     ! forcing: forcing function defined in this file
     do i = 1,nnode
		  f = forcing(id_test,mu,lambda,grid%coord(1,i),&
			  			  grid%coord(2,i),grid%coord(3,i))
		  do j = 1,dofsxnode
			  rhs(dofsxnode*i-(dofsxnode-j)) = P1GALERKIN%basis_integral(i)*f(j)
		  end do
     end do

     ! Write rhs vector in a file
     !open(9,file='rhs_preBCs.dat')
     !do i=1,ndof
     !   write(9,*) i, rhs(i)
     !end do
     !close(9)

  ! --------------------------------------------------------------

     ! Allocazione arrays soluzione 
     allocate(sol(ndof),solx(nnode),soly(nnode),solz(nnode),stat=res)
     if (res.ne.0) STOP "errore allocazione arrays sol"

     ! Initialize sol(x/y)
     sol  = one
     solx = one
     soly = one

  ! --------------------------------------------------------------

     ! Set dirichlet boundary conditions
     ! Number Dirichlet nodes
     ndir = dofsxnode*grid%nnode_bc

     ! Allocate space for noddir, soldir
     allocate(noddir(ndir),soldir(ndir),stat=res)
     if (res.ne.0) STOP "errore allocazione noddir e soldir"

     ! Array with the boundary nodes
     do i = 1,grid%nnode_bc
		  do j = 1,dofsxnode
			  noddir(dofsxnode*i-(dofsxnode-j)) = &
				  dofsxnode*grid%node_bc(i) - (dofsxnode-j)
		  end do
     end do

     !open(9,file='nodeDir.dat')
     !do i = 1,ndir
     !	write(9,*) noddir(i)
     !end do
     !close(9)

     ! Compute the value of the exact solution at the boundary nodes
     do i = 1,grid%nnode_bc
        j = grid%node_bc(i)
	     u_ex = pot_exact(id_test,mu,lambda,&
		  			grid%coord(1,j),grid%coord(2,j),grid%coord(3,j))
		  do j = 1,dofsxnode
			  soldir(dofsxnode*i-(dofsxnode-j)) = u_ex(j)
		  end do
     end do

     !open(9,file='solDir.dat')
     !do i = 1,ndir
     !	write(9,*) soldir(i)
     !end do
     !close(9)

     ! Preprocess stiffness_matrix, rhs and solution 
     ! to obtain dirichlet boundary condition
     call P1GALERKIN%dirichlet_bc(stderr,&
          stiff_matrix,rhs,sol,&
          ndir,noddir,soldir)
     
     ! Write rhs vector in a file
     !open(9,file='rhs_postBCs.dat')
     !do i=1,ndof
     !   write(9,*) i, rhs(i)
     !end do
     !close(9)

     ! Write arrays ia and ja in a file
     !open(9,file='stiffness_postBC.dat')
     !call stiff_matrix%write(9)
     !close(9)

     ! Initialize solution array
     sol = zero

  ! --------------------------------------------------------------

     ! Solution of the linear system 
     !write(*,*) ' solve linear system',etb(ctrl%approach)

     ! Initialization matrices of type ZEROMAT
     call zeromat1%init(ndof)
     call zeromat2%init(ndof)
     
     ! Choose how to solve the system
     select case (ctrl%approach)
     case('MG') 
        call multigrid_inverse%init(stderr,stiff_matrix,ctrl)
        call multigrid_inverse%Mxv(rhs,sol)
        call multigrid_inverse%info_solver%info(6)
        call multigrid_inverse%kill(stderr)
     case ('ITERATIVE')
        !write(*,*) 'call sum'
        stiff_matrix%name='A'
        !call sum1%info(6)
        !call zeromat1%info(6)
        !write(*,*) sum1%type,sum1%is_symmetric
        call ctrl_prec%init(stderr,'IC',n_fillin=30,tol_fillin=1d-3)
        call prec_stiff%init(stderr, i,ctrl_prec,ndof, stiff_matrix)
        call info%init()
        call linear_solver(stiff_matrix,rhs,sol,info,ctrl,prec_stiff)
        call ctrl_prec%kill()
        call prec_stiff%kill(stderr)
        call info%info(stdout)
     !   call info%time(stdout)
        call info%kill()
     end select
     
     ! Write solution array in a file
     !open(9,file='sol.dat')
     !do i=1,ndof
     !   write(9,*) sol(i)
     !end do
     !close(9)

  ! --------------------------------------------------------------

     ! Split solution into x and y component
     do i = 1,nnode
		  solx(i) = sol(dofsxnode*i-2)
		  soly(i) = sol(dofsxnode*i-1)
		  solz(i) = sol(dofsxnode*i  )
     end do

     ! Free memory
     deallocate(sol,stat=res)
     if (res.ne.0) STOP "errore deallocazione sol"

     ! Write solution array in a file
     !open(9,file='solxy.dat')
     !do i=1,nnode
     !   write(9,*) solx(i), soly(i)
     !end do
     !close(9)

  ! --------------------------------------------------------------

     ! Allocazione arrays gradiente soluzione 
     allocate(gradsolx(grid%logical_dimension,ncell),&
				  gradsoly(grid%logical_dimension,ncell),&
	     		  gradsolz(grid%logical_dimension,ncell),stat=res)
     if (res.ne.0) STOP "errore allocazione arrays gradsol"

     ! Initializes arrays
     gradsolx = zero
     gradsoly = zero
     gradsolz = zero

     ! Compute the gradient of the numerical solution
     call P1GALERKIN%eval_grad(solx,gradsolx)
     call P1GALERKIN%eval_grad(soly,gradsoly)
     call P1GALERKIN%eval_grad(solz,gradsolz)

  ! --------------------------------------------------------------

     ! Allocate space for reference solution and work arrays 
     allocate(potx(nnode),poty(nnode),potz(nnode),&
          gradpotx(grid%logical_dimension,ncell),&
          gradpoty(grid%logical_dimension,ncell),&
          gradpotz(grid%logical_dimension,ncell),&
          err_potx(nnode),err_poty(nnode),err_potz(nnode),&
          err_gradpotx(grid%logical_dimension,ncell),&
          err_gradpoty(grid%logical_dimension,ncell),&
          err_gradpotz(grid%logical_dimension,ncell),&
          err_nrmgradpotx(ncell),&
			 err_nrmgradpoty(ncell),&
			 err_nrmgradpotz(ncell),&
          stat=res)
     if (res.ne.0) STOP "errore allocazione arrays errori locali"

     ! Compute the value of the exact solution in the nodes of the grid
     do i = 1,nnode
	     u_ex = pot_exact(id_test,mu,lambda,&
		  			grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
        potx(i) = u_ex(1)
	     poty(i) = u_ex(2)
	     potz(i) = u_ex(3)
     end do

     ! Write solution array in a file
     !open(9,file='solxy_ex.dat')
     !do i=1,nnode
     !   write(9,*) potx(i), poty(i)
     !end do
     !close(9)

     ! Compute the exact gradient in the cells of the grid
     ! bar_cell contains the coordinates of the center of mass of the cell
     do i = 1,ncell
        call gradpot_exact(id_test,mu,lambda,grid%bar_cell(1,i),&
             grid%bar_cell(2,i),grid%bar_cell(3,i),gradu_ex) 
			    gradpotx(:,i) = gradu_ex(1,:)
			    gradpoty(:,i) = gradu_ex(2,:)
			    gradpotz(:,i) = gradu_ex(3,:)
     end do

     ! Compute the difference between numerical and exact solution 
     err_potx = solx - potx
     err_poty = soly - poty
     err_potz = solz - potz

     ! Write solution array in a file
     !open(9,file='errxy.dat')
     !do i=1,nnode
     !   write(9,*) err_potx(i), err_poty(i)
     !end do
     !close(9)
     
     ! Compute the difference between the gradient of numerical and exact solution 
     err_gradpotx = gradsolx - gradpotx
     err_gradpoty = gradsoly - gradpoty
     err_gradpotz = gradsolz - gradpotz

     ! Compute the euclidean norm of the gradient for each cell of the grid
     do i=1,grid%ncell
        err_nrmgradpotx(i) = dnrm2(grid%logical_dimension,&
				err_gradpotx(1:grid%logical_dimension,i),1)
        err_nrmgradpoty(i) = dnrm2(grid%logical_dimension,&
				err_gradpoty(1:grid%logical_dimension,i),1)
        err_nrmgradpotz(i) = dnrm2(grid%logical_dimension,&
				err_gradpotz(1:grid%logical_dimension,i),1)
     end do

     ! L2 error solution and gradient previous iteration (coarse mesh) 
     if (igrid > 1 ) then
        l2errx_pot_before = l2errx_pot 
        l2erry_pot_before = l2erry_pot 
        l2errz_pot_before = l2errz_pot 
        l2errx_gradpot_before = l2errx_gradpot
        l2erry_gradpot_before = l2erry_gradpot
        l2errz_gradpot_before = l2errz_gradpot
        h1errx_pot_before = h1errx_pot 
        h1erry_pot_before = h1erry_pot 
        h1errz_pot_before = h1errz_pot 
     end if

     ! Compute the L2-norm of the solution error
     err_potx = err_potx**2
     err_poty = err_poty**2
     err_potz = err_potz**2
     l2errx_pot = sqrt(ddot(nnode, P1GALERKIN%basis_integral, 1, err_potx,1))
     l2erry_pot = sqrt(ddot(nnode, P1GALERKIN%basis_integral, 1, err_poty,1))
     l2errz_pot = sqrt(ddot(nnode, P1GALERKIN%basis_integral, 1, err_potz,1))
     
     ! Compute the L2-norm of the gradient of the solution error 
     ! normp_cell computes the p-norm of piecewise constant functions over triangles
     l2errx_gradpot = grid%normp_cell(2.0d0,err_nrmgradpotx)
     l2erry_gradpot = grid%normp_cell(2.0d0,err_nrmgradpoty)
     l2errz_gradpot = grid%normp_cell(2.0d0,err_nrmgradpotz)

     ! Compute the H1-norm of the error 
     h1errx_pot = sqrt(l2errx_pot**2 + l2errx_gradpot**2)
     h1erry_pot = sqrt(l2erry_pot**2 + l2erry_gradpot**2)
     h1errz_pot = sqrt(l2errz_pot**2 + l2errz_gradpot**2)

  ! --------------------------------------------------------------

     allocate(size_edge(grid%nedge),stat=res)
	  if (res.ne.0) STOP "errore allocazione size_edge"

	  do i = 1,grid%nedge
		  ne1 => grid%edge_iside(1,i)
		  ne2 => grid%edge_iside(2,i)
		  xe1 => grid%coord(1,ne1)
		  ye1 => grid%coord(2,ne1)
		  ze1 => grid%coord(3,ne1)
		  xe2 => grid%coord(1,ne2)
		  ye2 => grid%coord(2,ne2)
		  ze2 => grid%coord(3,ne2)
		  size_edge(i) = sqrt((xe1-xe2)**2+(ye1-ye2)**2+(ze1-ze2)**2)
	  end do
     
	  meshpar = grid%meshpar(0)
!	  meshpar = maxval(size_edge)

	  deallocate(size_edge,stat=res)
	  if (res.ne.0) STOP "errore deallocazione size_edge"

  ! --------------------------------------------------------------

     ! Convergence rate
     if (igrid > 1) then
        ! Rate L2 convergence
        rateL2x = log(l2errx_pot_before/l2errx_pot)/log(two)
        rateL2y = log(l2erry_pot_before/l2erry_pot)/log(two)
        rateL2z = log(l2errz_pot_before/l2errz_pot)/log(two)
!        rateL2x = log(l2errx_pot_before/l2errx_pot)/log(meshpar_old/meshpar)
!        rateL2y = log(l2erry_pot_before/l2erry_pot)/log(meshpar_old/meshpar)
!        rateL2z = log(l2errz_pot_before/l2errz_pot)/log(meshpar_old/meshpar)
        ! Rate H1 convergence
        rateH1x = log(h1errx_pot_before/h1errx_pot)/log(meshpar_old/meshpar)
        rateH1y = log(h1erry_pot_before/h1erry_pot)/log(meshpar_old/meshpar)
        rateH1z = log(h1errz_pot_before/h1errz_pot)/log(meshpar_old/meshpar)
     end if

	  meshpar_old = meshpar

     ! Write output
     if (igrid > 1) then
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,3X,1PE12.5,6(3X,0PF12.8))') &
        !write(stdout,'(1X,I4.2,2X,I8,1X,I8,5(3X,1PE12.5))') &
	      igrid-1, grid%nnode, grid%ncell, &
	      meshpar,rateL2x,rateL2y,rateL2z,rateH1x,rateH1y,rateH1z 
        print*
     else
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,3X,1PE12.5,6(14X,A1))') &
	      igrid-1, grid%nnode, grid%ncell, &
	      meshpar,'-', '-', '-', '-', '-', '-'
        print*
     end if

  ! --------------------------------------------------------------

     ! Free memory 
     deallocate(noddir,soldir,stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays noddir soldir',res)

     ! Deallocate arrays
     deallocate(potx,poty,potz,&
		  			 gradpotx,gradpoty,gradpotz,&
					 err_potx,err_poty,err_potz,&
          		 err_gradpotx,err_gradpoty,err_gradpotz,&
					 err_nrmgradpotx,err_nrmgradpoty,err_nrmgradpotz,&
					 stat=res)
     if (res.ne.0) STOP "errore deallocazione pot, gradpot e errori"

     ! Deallocate arrays
     deallocate(diffusion_coeff,rhs,&
		  			 solx,soly,solz,&
					 gradsolx,gradsoly,gradsolz,&
					 stat=res)
     if (res.ne.0) STOP "errore deallocazione rhs, sol e gradsol"
     
     ! Free memory - kill stiffness matrix and lapl_p1 class
     call stiff_matrix%kill(6)
     call P1GALERKIN%kill(6)

	  call grid%kill(6)
	  call fgrid%kill(6)
  ! --------------------------------------------------------------

     ! Build a new refinement of the grid     
!     if (igrid < nref + 1) then
!	
!        call subgrid%refine(stderr,grid)!flag_reorder=1)
!        call subgrid%RCM_node_reorder(stderr)
!        call subgrid%build_size_cell(0)
!        call subgrid%build_normal_cell(0)
!        call subgrid%build_edge_connection(0)
!		  call subgrid%build_face_connection(0)
!        call subgrid%build_nodebc()
!        call subgrid%build_bar_cell()
!
!	     ! Kill subgrid
!        grid = subgrid
!        call subgrid%kill(stderr)
!
!     end if

  end do

  ! --------------------------------------------------------------

  ! Kill grid
!  call grid%kill(6)
!  call grid0%kill(6)
!  call fgrid%kill(6)

  ! --------------------------------------------------------------

contains

  ! Description of the problem
  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if((1.le.id_test).and.(id_test.le.5)) then
	    str = 'Rectangle [0,1]x[0,1] with Dirichlet BCs'
    else
	    STOP "error: test id must be between 1 and  5"
    end if

  end function example_description

  ! Definition of the forcing function
  function forcing(id_test,mu,lambda,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double), intent(in) :: x,y,z
    real(kind=double) :: func(3)

    select case (id_test)
    case(1) 
       func(1) = 3*(lambda+2*mu)*pigreco*pigreco*cos(pigreco*x)*sin(pigreco*y)*sin(pigreco*z)
       func(2) = 3*(lambda+2*mu)*pigreco*pigreco*sin(pigreco*x)*cos(pigreco*y)*sin(pigreco*z)
       func(3) = 3*(lambda+2*mu)*pigreco*pigreco*sin(pigreco*x)*sin(pigreco*y)*cos(pigreco*z)
    case(2) 
       func(1) = 12*(lambda+2*mu)*pigreco*pigreco*&
			 sin(two*pigreco*x)*cos(two*pigreco*y)*cos(two*pigreco*z)
       func(2) = 12*(lambda+2*mu)*pigreco*pigreco*&
			 cos(two*pigreco*x)*sin(two*pigreco*y)*cos(two*pigreco*z)
       func(3) = 12*(lambda+2*mu)*pigreco*pigreco*&
			 cos(two*pigreco*x)*cos(two*pigreco*y)*sin(two*pigreco*z)
	 case(3)
		 func(1) = 4*(lambda+mu)*x*(y**2)*z + pigreco*pigreco*sin(pigreco*x)*sin(pigreco*y)*&
			 (-(lambda+mu)*cos(pigreco*z)+(lambda+4*mu)*sin(pigreco*z))
		 func(2) = 4*(lambda+mu)*(x**2)*y*z + pigreco*pigreco*cos(pigreco*x)*cos(pigreco*y)*&
			 (+(lambda+4*mu)*cos(pigreco*z)-(lambda+mu)*sin(pigreco*z))
		 func(3) = 2*(lambda*(-1+(x**2)*(y**2))+mu*(-4+2*(x**2)*(y**2)+(x**2+y**2)*(z**2))) - &
			 (lambda+mu)*(pigreco**2)*cos(pigreco*x)*sin(pigreco*y)*(cos(pigreco*z)+sin(pigreco*z))
    end select 
  
  end function forcing

  ! Definition of the exact solution
  function pot_exact(id_test,mu,lambda,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: x,y,z
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double) :: func(3)

    select case (id_test)
    case(1) 
       func(1) = cos(pigreco*x) * sin(pigreco*y) * sin(pigreco*z)
       func(2) = sin(pigreco*x) * cos(pigreco*y) * sin(pigreco*z)
       func(3) = sin(pigreco*x) * sin(pigreco*y) * cos(pigreco*z)
    case(2) 
       func(1) = sin(two*pigreco*x) * cos(two*pigreco*y) * cos(two*pigreco*z)
       func(2) = cos(two*pigreco*x) * sin(two*pigreco*y) * cos(two*pigreco*z)
       func(3) = cos(two*pigreco*x) * cos(two*pigreco*y) * sin(two*pigreco*z)
	 case(3) 
		 func(1) = sin(pigreco*x) * sin(pigreco*y) * sin(pigreco*z)
		 func(2) = cos(pigreco*x) * cos(pigreco*y) * cos(pigreco*z)
		 func(3) = x**2 + y**2 + z**2 - (x**2) * (y**2) * (z**2) -1
    end select

  end function pot_exact

  ! Definition of the gradient of the exact solution
  subroutine gradpot_exact(id_test,mu,lambda,x,y,z,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in) :: x,y,z
    real(kind=double), intent(in) :: mu, lambda
    real(kind=double), intent(out) :: func(3,3)

    select case (id_test)
    case(1) 
       func(1,1) = - pigreco * sin(pigreco*x) * sin(pigreco*y) * sin(pigreco*z)
       func(1,2) = + pigreco * cos(pigreco*x) * cos(pigreco*y) * sin(pigreco*z)
       func(1,3) = + pigreco * cos(pigreco*x) * sin(pigreco*y) * cos(pigreco*z)
       func(2,1) = + pigreco * cos(pigreco*x) * cos(pigreco*y) * sin(pigreco*z)
       func(2,2) = - pigreco * sin(pigreco*x) * sin(pigreco*y) * sin(pigreco*z)
       func(2,3) = + pigreco * sin(pigreco*x) * cos(pigreco*y) * cos(pigreco*z)
       func(3,1) = + pigreco * cos(pigreco*x) * sin(pigreco*y) * cos(pigreco*z)
       func(3,2) = + pigreco * sin(pigreco*x) * cos(pigreco*y) * cos(pigreco*z)
       func(3,3) = - pigreco * sin(pigreco*x) * sin(pigreco*y) * sin(pigreco*z)
    case(2) 
       func(1,1) = +two*pigreco * cos(two*pigreco*x) * cos(two*pigreco*y) * cos(two*pigreco*z)
       func(1,2) = -two*pigreco * sin(two*pigreco*x) * sin(two*pigreco*y) * cos(two*pigreco*z)
       func(1,3) = -two*pigreco * sin(two*pigreco*x) * cos(two*pigreco*y) * sin(two*pigreco*z)
       func(2,1) = -two*pigreco * sin(two*pigreco*x) * sin(two*pigreco*y) * cos(two*pigreco*z)
       func(2,2) = +two*pigreco * cos(two*pigreco*x) * cos(two*pigreco*y) * cos(two*pigreco*z)
       func(2,3) = -two*pigreco * cos(two*pigreco*x) * sin(two*pigreco*y) * sin(two*pigreco*z)
       func(3,1) = -two*pigreco * sin(two*pigreco*x) * cos(two*pigreco*y) * sin(two*pigreco*z)
       func(3,2) = -two*pigreco * cos(two*pigreco*x) * sin(two*pigreco*y) * sin(two*pigreco*z)
       func(3,3) = +two*pigreco * cos(two*pigreco*x) * cos(two*pigreco*y) * cos(two*pigreco*z)
    case(3) 
       func(1,1) = + pigreco * cos(pigreco*x) * sin(pigreco*y) * sin(pigreco*z)
       func(1,2) = + pigreco * sin(pigreco*x) * cos(pigreco*y) * sin(pigreco*z)
       func(1,3) = + pigreco * sin(pigreco*x) * sin(pigreco*y) * cos(pigreco*z)
       func(2,1) = - pigreco * sin(pigreco*x) * cos(pigreco*y) * cos(pigreco*z)
       func(2,2) = - pigreco * cos(pigreco*x) * sin(pigreco*y) * cos(pigreco*z)
       func(2,3) = - pigreco * cos(pigreco*x) * cos(pigreco*y) * sin(pigreco*z)
		 func(3,1) = + two * x - two * x * (y**2) * (z**2)
		 func(3,2) = + two * y - two * y * (x**2) * (z**2)
		 func(3,3) = + two * z - two * z * (x**2) * (y**2)
    end select

  end subroutine gradpot_exact
  
end PROGRAM elasticityDir
