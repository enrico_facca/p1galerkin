!>---------------------------------------------------------------------
!> 
!> Check if the stiffness matrix that arises from the discretization
!> of - \div \sigma = f with P1 Galerkin Finite Elements is correct.
!> This program writes the non-zero coefficients in an external file.
!> The result is compared with the one given by MATLAB code.
!> The subroutine that implements the assembly procedure is called
!> build_stiff_saint_venant (63modP1Galerkin.f90)
!>
!> \author{Enrico Facca and Nicolò Crescenzio}
!>
!> DESCRIPTION: 
!> The main program 
!>
!> REVISION HISTORY:
!> 20190327 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program elasticityMatCheck

  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  
  use TimeInputs, only : write_steady
    
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       		    stdin  => input_unit, &
       		    stdout => output_unit, &
       		    stderr => error_unit  

  implicit none
  
  ! 00modGlobals.f90
  type(file) :: fgrid 

  ! 49modAbstractGeometry.f90
  type(abs_simplex_mesh), target :: grid

  ! 63modP1Galerkin.f90
  type(p1gal), target :: lapl_p1

  ! Diffusion, right hand side and solution arrays 
  real(kind=double), allocatable :: diffusion_coeff(:)
  
  ! Character variables
  character(len=256) :: fngrid

  ! 22modSparseMatrix.f90
  type(spmat) :: stiffxx, stiffxy, stiffyy
  type(TransposeMatrix) :: stiffyx
  type(spmat) :: stiffyx_explicit
  type(block_linop) :: elasticity_matrix
  type(array_linop) :: list_operators(4)
  integer ::  block_structure(3,4) 
  real(kind=double), parameter :: mu = 1, lambda = 1

  ! Local variables
  logical :: rc
  integer :: res, i, j, k
  integer, pointer :: nnode, ncell, nterm, ndof

  ! --------------------------------------------------------------

  ! Read mesh data from file 
  fngrid = 'inputs/grid000.dat'
  call fgrid%init(6,etb(fngrid),20,'in')
  !call fgrid%info(0)
  call grid%read_mesh(0, fgrid)
  call grid%build_size_cell(0)
  call grid%build_normal_cell(0)
  call grid%build_edge_connection(0)
  call grid%build_nodebc()
  call grid%build_bar_cell()
  !call grid%info(0)

  ! --------------------------------------------------------------

  ! Number of nodes
  nnode => grid%nnode
  ! Number of triangles
  ncell => grid%ncell

  ! Allocate space for diffusion coefficient 
  allocate(diffusion_coeff(ncell),stat=res)
  if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
       'array diffusion_coeff',res)

  ! Give a value to cond
  diffusion_coeff = one

  ! P1 Galerkin 
  call lapl_p1%init(6, grid)

  ! Non zero terms in the stiffness matrix
  nterm => lapl_p1%nterm_csr
  ! Number of degrees of freedom
  ndof => lapl_p1%grid%nnode

  ! Initialization stiffness matrix (type SPMAT)
  call stiffxx%init(6,ndof, ndof, nterm,&
       storage_system='csr',is_symmetric=.true.)
  call stiffyy%init(6,ndof, ndof, nterm,&
       storage_system='csr',is_symmetric=.true.)
  call stiffxy%init(6,ndof, ndof, nterm,&
       storage_system='csr',is_symmetric=.false.)

  ! Build stiffness matrix for Poisson Equation
  call lapl_p1%build_stiff_saint_venant(6, mu, lambda, &
       diffusion_coeff, stiffxx, stiffxy, stiffyy)

  ! implicit definition of Syx
  ! (only matrix vector product is defined)
  call stiffyx%init(stiffxy,info)

  ! explicit transposition of Sxy
  ! first copy, then transpose
  stiffyx_explicit = stiffxy
  call stiffyx_explicit%transpose(lun)
  

  ! create a "list" of operators
  list_operators(1)%linop => stiffxx
  list_operators(2)%linop => stiffxy
  list_operators(3)%linop => stiffyx_explicit
  list_operators(4)%linop => stiffyy

  
  ! create clock structure for
  ! (Sxx, Sxy )
  ! (Syx  Syy )
  block_structure(:,1) = (/1,1,1/)
  block_structure(:,2) = (/2,1,2/)
  block_structure(:,3) = (/3,2,1/)
  block_structure(:,4) = (/4,2,2/)

  ! init. block matrix
  call elasticity_matrix%init(6, &
       3, list_operators,&
       2, 2, &
       4, block_structure, &
       is_symmetric=.True.)
  

  ! Write non zero coefficients in a file
  open(9,file='sxx.dat')
  call stiffxx%write(9)
  close(9)
  open(9,file='sxy.dat')
  call stiffxy%write(9)
  close(9)
  open(9,file='syy.dat')
  call stiffyy%write(9)
  close(9)

  ! --------------------------------------------------------------

  ! Deallocate array
  deallocate(diffusion_coeff,stat=res)
  if(res .ne. 0) rc = IOerr(6, err_dealloc , 'Laplace main',&
       'arrays diffusion_coeff, rhs, sol',res)
     
  ! Free memory - kill stiffness matrix and lapl_p1 class
  call stiffxx%kill(6)
  call stiffxy%kill(6)
  call stiffyy%kill(6)
  call lapl_p1%kill(6)

  ! --------------------------------------------------------------

end PROGRAM elasticityMatCheck
