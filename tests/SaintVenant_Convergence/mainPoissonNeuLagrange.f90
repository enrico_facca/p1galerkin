!>---------------------------------------------------------------------
!> 
!> DESCRIPTION: 
!> Convergence test for Poisson Equation $-\Div K \nabla u  = f(x)$
!> on the unit square $[0,1] \times [0,1]$ with homogeneous Neumann 
!> Boundary conditions. The zero average constraint on the solution is
!> imposed using the method of Lagrange multipliers. The saddle point
!> system that arise from the discretization of the continuous mixed 
!> formulation is solved using the BiConjugate Stabilized iterative 
!> method. The user can choose between two different problems.
!> The algorithm starts from a given grid and then refines nref times.
!>
!> \author{Enrico Facca and Nicolò Crescenzio}
!>
!> REVISION HISTORY:
!> 20201106 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

program laplace

  use Globals
  use AbstractGeometry
  use LinearOperator
  use SimpleMatrix
  use Matrix
  use SparseMatrix
  use SaddlePointMatrix
  
  use TimeInputs, only : write_steady
    
!  use Timing
  use StdSparsePrec
  use LinearSolver
  use P1Galerkin
  use dagmg_wrap
  use, intrinsic :: iso_fortran_env, only : &
       		    stdin  => input_unit, &
       		    stdout => output_unit, &
       		    stderr => error_unit  

  implicit none
  
  ! 00modGlobals.f90
  type(file)     :: fgrid, fgridout, fctrl, fout

  ! 49modAbstractGeometry.f90
  type(abs_simplex_mesh)     :: grid0, grid, subgrid

  ! 63modP1Galerkin.f90
  type(p1gal)    :: lapl_p1

  ! dagmg_wrap.f90
  type(agmg_inv) :: multigrid_inverse

  ! Diffusion, right hand side and solution arrays 
  real(kind=double), allocatable :: diffusion_coeff(:), rhs(:), sol(:), sol2(:), uno(:), due(:)
  
  ! Dirichlet boundary condition arrays
  integer, allocatable :: noddir(:)
  real(kind=double), allocatable :: soldir(:)

  ! P1 gradient soof the solution
  real(kind=double), allocatable :: temp(:) ,gradsol(:,:)

  ! Exact solution and its gradient
  real(kind=double), allocatable :: pot(:), gradpot(:,:)

  ! Error pot and its gradient
  real(kind=double), allocatable :: err_pot(:), err_gradpot(:,:), err_nrmgradpot(:)

  ! Errror norm
  real(kind=double)  :: l2err_pot, l2err_gradpot ,l2err_pot_before, l2err_gradpot_before,&
	  		h1err_pot, h1err_pot_before, rateL2, rateH1

  ! Euclidian norm
  real(kind=double) :: dnrm2, ddot

  ! Character variables
  character(len=256) :: input, fngrid, fnctrl, fnctrl_in, fname, gridname, tail

  ! 22modSparseMatrix.f90
  type(spmat)    :: stiff_matrix
  type(spmat)    :: B
  type(spmat)    :: BT
  
  ! 57modSaddlePoint.f90
  type(saddlemat) :: saddle

  ! 20modSimpleMatrix.f90
  type(zeromat) :: zeromat1,zeromat2
  type(zeromat) :: eye1,eye2

  type(add_linop) :: sum1,sum2
  
  ! 12modLinearSolver.f90
  type(input_solver) :: ctrl
  type(output_solver):: info,info1
  
  type(input_prec) :: ctrl_prec
  type(stdprec) :: prec_stiff

  ! Local variables
  logical :: rc
  integer :: res,nterm,ndof,i,j,inod,ndir, k, icell
  integer :: nnode, ncell
  integer :: nref, gridnumber
  integer :: iter
  integer :: igrid,id_test_read,id_test
  real(kind=double) :: q

  ! --------------------------------------------------------------

  ! Read number of refinements
  call getarg(1,input)
  read(input,*,iostat=res) nref
  if (res.ne.0) nref = 0

  ! Read id starting grid
  call getarg(2,input)
  read(input,*,iostat=res) gridnumber
  if (res.ne.0) gridnumber = 0

  ! Read id_test
  call getarg(3,input)
  read(input,*,iostat=res) id_test
  if ((id_test.lt.1).or.(id_test.gt.2)) then
     id_test = 1
  end if

  print*
  write(stdout,*) '**************** TEST CASE ***********************'
  write(stdout,*) 'Domain: [0,1]x[0,1] unit square'
  write(stdout,*) 'PDE: Poisson Equation'
  write(stdout,*) 'Boundary conditions: zero Neumann BCs'
  write(stdout,*) 'Null average constrain is imposed using Lagrange multiplier'
  write(stdout,'(1X,A,I1)') 'Starting from grid: ', gridnumber
  write(stdout,'(1X,A,I0.2)') 'Test ID = ', id_test
  write(stdout,'(1X,A,I0.2)') 'Total mesh refinements = ',nref
  write(stdout,*) '**************************************************'

  ! --------------------------------------------------------------

  ! Read mesh data from file 
  write(gridname,'(A,I1,A)') 'mesh',gridnumber,'.dat'
  fngrid = etb('inputs/'//etb(gridname))
  call fgrid%init(6,etb(fngrid),20,'in')
  !call fgrid%info(0)
  call grid0%read_mesh(0, fgrid)
  call grid0%build_size_cell(0)
  call grid0%build_normal_cell(0)
  call grid0%build_edge_connection(0)
  call grid0%build_nodebc()
  call grid0%build_bar_cell()
  !call grid0%info(0)

  grid = grid0

  ! --------------------------------------------------------------

  ! At each mesh refinement write data
  print*
  write(*,*) 'nref|   #cells   #nodes       mesh par       &
	  L2-error       H1-error        rate L2        rate H1'
  write(stdout,'(1X,98("-"))') 
 
  ! Loop over nref
  do igrid = 1, nref+1

     write(tail,'(a,I0.3,a)') 'ref',igrid-1,'.dat'
     
     fname = etb('grid'//etb(tail))
     call fgridout%init(stderr,fname,8,'out')
     call grid%write_mesh(stderr, fgridout)
     call fgridout%kill(stderr)

     ! Number of nodes
     nnode = grid%nnode
     ! Number of triangles
     ncell = grid%ncell

     ! Allocate space for diffusion coefficient 
     allocate(diffusion_coeff(ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'array diffusion_coeff',res)
     ! Give a value to cond
     diffusion_coeff = one

     ! P1 galerkin for Laplace Equation
     call lapl_p1%init(6, grid)

     ! Non zero terms in the stiffness matrix
     nterm = lapl_p1%nterm_csr
     ! Number of degrees of freedom
     ndof = lapl_p1%grid%nnode

     ! Initialization stiffness matrix (type SPMAT)
     call stiff_matrix%init(6,&
          ndof, ndof, nterm,&
          storage_system='csr',&
          is_symmetric=.true.)

     ! Build stiffness matrix for Poisson Equation
     call lapl_p1%build_stiff(6, 'csr', diffusion_coeff, stiff_matrix)

  ! --------------------------------------------------------------

     ! Allocate space for rhs, sol, gradsol
     allocate(rhs(ndof+1),sol(ndof+1),&
	     gradsol(grid%logical_dimension,ncell),stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays rhs,sol,gradsol ',res)

     ! Initialize rhs, sol, gradsol
     rhs = zero
     sol = zero
     gradsol = zero

     ! Create right hand side vector $rhs(i)=\int f \phi_i$
     ! basis_integral: integral of the basis function (computed in the constructor)
     ! forcing: forcing function defined in this file
     do i = 1,ndof
        rhs(i) = lapl_p1%basis_integral(i)* &
             forcing(id_test,grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
     end do
     
     ! Write arrays ia and ja in a file
     !open(9,file='stiffness.dat')
     !call stiff_matrix%write(9,'matlab')
     !close(9)

     ! Write rhs vector in a file
     !open(9,file='rhs.dat')
     !do i=1,nnode+1
     !   write(9,*) i, rhs(i)
     !end do
     !close(9)

  ! --------------------------------------------------------------

     ! Definition saddle point matrix
     call B%init(6, 1, ndof, ndof, &
          storage_system='csr',is_symmetric=.false.)

     call BT%init(6, 1, ndof, ndof, &
          storage_system='csr',is_symmetric=.false.)

     B%ia(1) = 1
     B%ia(2) = ndof+1

     do i = 1,ndof
	B%ja(i) = i
	B%coeff(i) = lapl_p1%basis_integral(i)
     end do

     BT = B

     call BT%transpose(6)

     ! Definition saddle point matrix
     call saddle%init(6,stiff_matrix,BT,B,B1equalB2=.true.)

     open(9,file='matrixb.dat')
     call b%write(9,'matlab')
     close(9)

     open(9,file='matrixbt.dat')
     call bt%write(9,'matlab')
     close(9)

  ! --------------------------------------------------------------

     ! Solution of the linear system 

     !call bicgstab(ndof,stiff_matrix,rhs,1.0d-10,1000,iter,q,sol)
     call bicgstab(ndof,saddle,rhs,1.0d-12,1000,iter,q,sol)
     
     ! Write solution array in a file
     open(9,file='solLagrange.dat')
	  write(9,*) nnode
     do i=1,nnode
        write(9,'(F25.20)') sol(i)
     end do
     close(9)

  ! --------------------------------------------------------------

     allocate(sol2(ndof+1),stat=res)
     if (res.ne.0) STOP "errore allocazione"
     do i = 1,ndof+1
     	sol2(i) = sol(i)
     end do
     deallocate(sol,stat=res)
     if (res.ne.0) STOP "errore deallocazione"
     allocate(sol(ndof),stat=res)
     if (res.ne.0) STOP "errore allocazione"
     do i = 1,ndof
	     sol(i) = sol2(i)
     end do
     deallocate(sol2,stat=res)
     if (res.ne.0) STOP "errore deallocazione"


     ! Allocate space for reference solution and work arrays 
     allocate(&
          pot(ndof),&
          gradpot(grid%logical_dimension,ncell),&
          err_pot(ndof),&
          err_gradpot(grid%logical_dimension,ncell),&
          err_nrmgradpot(ncell),&
          stat=res)
     if(res .ne. 0) rc = IOerr(6, err_alloc , 'Laplace main',&
          'arrays pot gradpot',res)

     ! Compute the gradient of the numerical solution 
     call lapl_p1%eval_grad(sol,gradsol)

     ! Compute the exact solution in the nodes of the grid
     do i=1,ndof
        pot(i) = pot_exact(id_test, grid%coord(1,i),grid%coord(2,i),grid%coord(3,i))
     end do

     ! Compute the exact gradient in the cells of the grid
     ! bar_cell contains the coordinates of the center of mass of the cell
     do i = 1,ncell
        call gradpot_exact(id_test,&
             grid%bar_cell(1,i),&
             grid%bar_cell(2,i),&
             grid%bar_cell(3,i),&
             grid%logical_dimension, gradpot(1:grid%logical_dimension,i))
     end do

     ! Compute the difference between numerical and exact solution 
     err_pot = sol - pot
     
     ! Compute the difference between the gradient of numerical and exact solution 
     err_gradpot = gradsol - gradpot

     ! Compute the euclidean norm of the gradient for each cell of the grid
     do i=1,grid%ncell
        err_nrmgradpot(i) = dnrm2(grid%logical_dimension,&
		err_gradpot(1:grid%logical_dimension,i),1)
     end do

     ! L2 error solution and gradient previous iteration (coarse mesh) 
     if (igrid > 1 ) then
        l2err_pot_before = l2err_pot 
        l2err_gradpot_before = l2err_gradpot
        h1err_pot_before = h1err_pot 
     end if

     ! Compute the L2-norm of the solution error
     err_pot = err_pot**2
     l2err_pot = sqrt(ddot(ndof, lapl_p1%basis_integral, 1, err_pot,1))
     
     ! Compute the L2-norm of the gradient of the solution error 
     ! normp_cell computes the p-norm of piecewise constant functions over triangles
     l2err_gradpot = grid%normp_cell(2.0d0,err_nrmgradpot)

     ! Compute the H1-norm of the error 
     h1err_pot = sqrt(l2err_pot**2 + l2err_gradpot**2)

     ! Convergence rate
     if (igrid > 1) then
        ! Rate L2 convergence
        rateL2 = log(l2err_pot_before/l2err_pot)/log(two)
        ! Rate H1 convergence
        rateH1 = log(h1err_pot_before/h1err_pot)/log(two)
     end if

  ! --------------------------------------------------------------

     ! Write output
     if (igrid > 1) then
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,5(3X,1PE12.5))') &
	      igrid, grid%nnode, grid%ncell, &
	      grid%meshpar(0),l2err_pot, h1err_pot, rateL2, rateH1 
        print*
     else
        write(stdout,'(1X,I4.2,2X,I8,1X,I8,3(3X,1PE12.5),14X,A1,14X,A1)') &
	      igrid, grid%nnode, grid%ncell, &
	      grid%meshpar(0),l2err_pot, h1err_pot, '-', '-'
        print*
     end if

  ! --------------------------------------------------------------

     ! Deallocate arrays
     deallocate(&
          pot,&
          gradpot,&
          err_pot,&
          err_gradpot,&
          err_nrmgradpot,&
          stat=res)
     if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'Laplace main',&
          'arrays pot gradpot',res)

     ! Deallocate arrays
     deallocate(diffusion_coeff,rhs,sol,gradsol,stat=res)
     if(res .ne. 0) rc = IOerr(6, err_dealloc , 'Laplace main',&
          'arrays diffusion_coeff, rhs, sol',res)
     
     ! Free memory - kill stiffness matrix and lapl_p1 class
     call saddle%kill(6)
     call stiff_matrix%kill(6)
     call B%kill(6)
     call BT%kill(6)
     call lapl_p1%kill(6)

  ! --------------------------------------------------------------

     ! Build a new refinement of the grid     
     if (igrid < nref + 1) then
	
        call subgrid%refine(stderr,grid)!flag_reorder=1)
        call subgrid%RCM_node_reorder(stderr)
        call subgrid%build_size_cell(0)
        call subgrid%build_normal_cell(0)
        call subgrid%build_edge_connection(0)
        call subgrid%build_nodebc()
        call subgrid%build_bar_cell()

	! Kill subgrid
        grid = subgrid
        call subgrid%kill(stderr)

     end if

  end do

  ! Kill grid
  call grid%kill(6)
  call grid0%kill(6)
  call fgrid%kill(6)

  ! --------------------------------------------------------------

contains

  ! Description of the problem
  function example_description(id_test) result(str)

    implicit none
    integer, intent(in) :: id_test
    character(len=256) :: str

    if ((id_test.eq.1).or.(id_test.eq.2)) then
       str = 'Rectangle [0,1]x[0,1] with homogeneous Neumann BCs'
    else
       STOP "errore"
    end if 
  
  end function example_description

  ! Definition of the forcing function
  function forcing(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    select case (id_test)
    case(1)
	    func = 8 * pigreco**2 * cos(two*pigreco*x) * cos(two*pigreco*y) 
    case(2)
	    func = two*pigreco*cos(two*pigreco*y)*&
		    (two*pigreco*(one+x**2)*cos(pigreco*x**2)+sin(pigreco*x**2))
    end select
  
  end function forcing

  ! Definition of the exact solution
  function pot_exact(id_test,x,y,z) result(func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    real(kind=double) :: func

    select case (id_test)
    case(1)
	    func = cos(two*pigreco*x) * cos(two*pigreco*y)
    case(2)
	    func = cos(pigreco*x*x)*cos(two*pigreco*y) 
    end select

  end function pot_exact

  ! Definition of the gradient of the exact solution
  subroutine gradpot_exact(id_test,x,y,z,logical_dimension,func)

    implicit none
    integer, intent(in) :: id_test
    real(kind=double), intent(in):: x,y,z
    integer,           intent(in):: logical_dimension
    real(kind=double), intent(out) :: func(logical_dimension)

    select case (id_test)
    case(1)
            func(1) =  - two * pigreco * sin(two*pigreco*x) * cos(two*pigreco*y)
            func(2) =  - two * pigreco * cos(two*pigreco*x) * sin(two*pigreco*y) 
    case(2)
	    func(1) = - two*pigreco*x*sin(pigreco*x*x)*cos(two*pigreco*y)
	    func(2) = - two*pigreco*cos(pigreco*x*x)*sin(two*pigreco*y)
    end select

  end subroutine gradpot_exact
  
  
SUBROUTINE BICGSTAB(NROW,SADDLE,F,TOLL,ITERMAX,ITER,Q,X)

IMPLICIT NONE

! VARIABILI DI INPUT
INTEGER, INTENT(IN) :: NROW
INTEGER, INTENT(IN) :: ITERMAX
real(kind=double), INTENT(IN) :: TOLL
real(kind=double), DIMENSION(:), INTENT(IN) :: F
!type(spmat), target :: matrix
type(saddlemat) :: saddle

! VARIABILI DI OUTPUT
INTEGER, INTENT(OUT) :: ITER 
REAL(kind=double), INTENT(OUT)  :: Q
REAL(kind=double), DIMENSION(:), INTENT(INOUT) :: X 

! VARIABILI LOCALI
INTEGER :: STATO, I, D
real(kind=double) :: RHO0, RHO1, NB, ALPHA, BETA
real(kind=double) :: W, RN, TT, TS, VR0
real(kind=double), DIMENSION(NROW+1) :: AX0, R0, R, P, V, S, T
real(kind=double) :: TIME, TIMEITER
! PUNTATORI LOCALI
INTEGER, POINTER :: IA(:), JA(:)
real(kind=double), POINTER :: SYSMAT(:)

D = NROW+1
! ----------------------------------------------------------------
! INIZIALIZZO IL VETTORE SOLUZIONE
DO I = 1,D
   X(I) = 0.0
END DO

! PRODOTTO MATRICE - VETTORE
CALL saddle%mxv(X,AX0)

DO I = 1,D
   R(I) = F(I) - AX0(I)
   R0(I) = R(I)
   P(I) = 0.
   V(I) = 0.
END DO

! CALCOLO NORMA^2 RESIDUO INIZIALE
CALL DOTPRODUCTOMP(R0,R,D,RN)
! NORMA^2 DEL RHS
CALL DOTPRODUCTOMP(F,F,D,NB)

RN = SQRT(RN)
NB = SQRT(NB)
Q = RN/NB
RHO0 = 1.
ALPHA = 1.
W = 1.
ITER = 0

! CALCOLO LA SOLUTIONE DEL SISTEMA LINEARE
DO WHILE ((ITER.LT.ITERMAX).AND.(Q.GT.TOLL))

   ! CONTO LE ITERAZIONI
   ITER = ITER + 1
  
   ! PRODOTTO SCALARE (rho)
   CALL DOTPRODUCTOMP(R0,R,D,RHO1)

   ! CALCOLO BETA
   BETA = (RHO1/RHO0)*(ALPHA/W)

   ! AGGIORNAMENTO VETTORE: CALCOLO P
   DO I = 1,D
      P(I) = R(I) + BETA*(P(I) - W*V(I))
   END DO

   ! PRODOTO MATRICE VETTORE (v=A*p)
   CALL saddle%mxv(P,V)

   ! PRODOTTO SCALARE
   CALL DOTPRODUCTOMP(V,R0,D,VR0)
   
   ! CALCOLO ALPHA
   ALPHA = RHO1/VR0

   ! AGGIORNAMENTO VETTORE
   DO I = 1,D
      S(I) = R(I) - ALPHA*V(I)
   END DO

   ! PRODOTTO MATRICE VETTORE (t=A*s)
   CALL saddle%mxv(S,T)

   ! DUE PRODDOTTI SCALARI
   CALL DOTPRODUCTOMP(T,T,D,TT)
   CALL DOTPRODUCTOMP(T,S,D,TS)

   ! OMEGA
   W = TS/TT

   DO I = 1,D
      ! AGGIORNAMENTO VETTORE SOLUZIONE NUMERICA
      X(I) = X(I) + ALPHA*P(I) + W*S(I)
      ! AGGIORNAMENTO VETTORE RESIDUO
      R(I) = S(I) - W*T(I)
   END DO

   ! CALCOLO LA NORMA DEL RESIDUO
   CALL DOTPRODUCTOMP(R,R,D,RN)

   ! CALCOLO Q: RESIDUO RELATIVO
   RN = SQRT(RN)
   Q = RN/NB

   ! AGGIORNO RHO
   RHO0 = RHO1

END DO

END SUBROUTINE BICGSTAB

SUBROUTINE DOTPRODUCTOMP(A,B,NTERMS,X) 

IMPLICIT NONE

! VARIABILI DI INPUT
INTEGER, INTENT(IN) :: NTERMS
REAL(kind=double), DIMENSION(:), INTENT(IN)  :: A, B

! VARIABILI DI OUTPUT
REAL(kind=double), INTENT(OUT) :: X

! VARIABILI LOCALI
INTEGER :: I

! CALCOLO IL PRODOTTO SCALARE TRA DUE ARRAY 
X = A(1)*B(1)
DO I=2,NTERMS
   X = X + A(I)*B(I)
END DO

END SUBROUTINE DOTPRODUCTOMP

end PROGRAM laplace
