!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> Program to compute and save the gradient $\nabla u_h$ of P1Galerkin function 
!> given a potential $u_h$ and a grid $\tau_h$.
!> 
!>
!> REVISION HISTORY:
!> 20180326 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM eval_principal_components
  use Globals
  use AbstractGeometry
  use P1Galerkin
  use TimeInputs
  use TimeOutputs
  implicit none

  type(file) :: fgrid
  type(file) :: fpot
  type(file) :: file_stress_min  
  type(file) :: file_stress_max    

  type(abs_simplex_mesh), target :: grid
  
  type(p1gal):: p1
  
  type(TimeData) :: pot
  
  type(TDOut)  :: s_max
  type(TDOut)  :: s_min

  logical  :: rc,endfile1,endreading
  integer  :: res
  character(len=256) :: input, path_grid, path_pot, &
	  path_max_stress, path_min_stress, mu_input, lambda_input

  integer :: stderr,stdout,debug , narg
  integer :: i,j, itria,ninputs,dim,itime, iloc, icell

  real(kind=double) :: time
  real(kind=double) :: mu_lame, lambda_lame, trace

  real(kind=double), allocatable :: stress(:,:,:), strain(:,:,:), identity(:,:)

  ! Local pointers
  integer, pointer :: ndata, ncell, nnode, dimdata

  ! lapack variables
  integer :: info, lwork
  real(kind=double), allocatable :: eig(:), work(:)

  stderr=6
  stdout=6


  !>----------------------------------------------------------------------------
  
  !>----------------------------------------------------------------------------
  !> Geometry info
  !> Read original grid
  ! no renumbering
  ! no connection of second level
  !call getarg(1,input)
  !read(input,'(a)') path_grid 
  
  !
  ! 1: path grid
  !
  narg=1
  call  get_command_argument(narg, path_grid,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  !
  ! 2: path pot file
  !
  narg=narg+1
  call  get_command_argument(narg, path_pot,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  !
  ! 3: path min stress file (output)
  !
  narg=narg+1
  call  get_command_argument(narg, path_min_stress,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  !
  ! 4: path max stress file (output)
  !
  narg=narg+1
  call  get_command_argument(narg, path_max_stress,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  !
  ! 5: mu lame' coefficient
  !
  narg=narg+1
  call  get_command_argument(narg, mu_input,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  
  !
  ! 6: lambda lame' coefficient
  !
  narg=narg+1
  call  get_command_argument(narg, lambda_input,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)

  ! 
  ! convert lame constant from char to real
  !
  read(mu_input,*) mu_lame
  read(lambda_input,*) lambda_lame

  !
  ! open files (grid and pot)
  !
  write(*,*) 'open files'
  call fgrid%init(stderr,etb(path_grid),10,'in')
  call fpot%init(stderr,etb(path_pot),11,'in')
  
  !
  ! init grid
  !
  write(*,*) 'open grid'
  call grid%read_mesh(stderr,fgrid)
  call grid%build_size_cell(stderr)
  call grid%build_normal_cell(stderr)
  call grid%build_edge_connection(stderr)
  call grid%build_nodebc()
  call grid%build_bar_cell()
  call grid%info(stdout)

  !
  ! Local pointers to grid data
  !
  ndata   => grid%ncell
  ncell   => grid%ncell
  nnode   => grid%nnode
  dimdata => grid%ambient_dimension

  !
  ! init p1_galerkin variable
  !
  write(*,*) 'init p1'
  call p1%init(stderr,grid)
  
  !
  ! int pot varaible
  !
  write(*,*) 'init pot'
  call pot%init(stderr,fpot,dimdata,nnode)
  
  !
  ! init file_stress_min and file_stress_max associated filed
  !
  write(*,*) 'init stress min stress max'
  call file_stress_min%init(stderr,etb(path_min_stress),13,'out')
  call file_stress_max%init(stderr,etb(path_max_stress),14,'out')

  !
  ! init TDOut variables
  !
  write(*,*) 'init smin smax'
  call s_min%init(stderr,dimdata,ndata)
  call s_max%init(stderr,dimdata,ndata)
  
  !
  ! write head output files
  !
  write(*,*) 'write head'
  call write2file(stderr,'head',dimdata,ndata,s_min%TDactual,time,file_stress_min)
  call write2file(stderr,'head',dimdata,ndata,s_max%TDactual,time,file_stress_max)

  !
  ! allocate matrices for strain, stress and identity
  !
  write(*,*) 'allocate stress, strain, identity'
  allocate(strain(dimdata,dimdata,ncell),&
	  		  stress(dimdata,dimdata,ncell),&
		     identity(dimdata,dimdata),stat=res)
  if(res.ne.0) rc = IOerr(stderr, err_alloc, 'main', &
            ' strain stress identity ',res)

  !
  ! allocate lapack variables
  !
  write(*,*) 'allocate lapack variables'
  lwork = 3*dimdata-1
  allocate(eig(dimdata),work(3*dimdata-1),stat=res)
  if(res.ne.0) rc = IOerr(stderr, err_alloc, 'main', &
            ' lapack variables ',res)
  !
  ! Build identity matrix
  !
  identity = zero
  do i = 1,dimdata
	  identity(i,i) = one
  end do

!  identity(1,1) = 7.0d0
!  identity(1,2) = 6.0d0
!  identity(2,1) = 6.0d0
!  identity(2,2) = 2.0d0
!  identity(1,1) = 5.0d0
!  identity(1,2) = 2.0d0
!  identity(2,1) = 2.0d0
!  identity(2,2) = 2.0d0
!  call dsyev('V','U',dimdata,identity(1:2,1:2),dimdata,eig,work,lwork,info)
!  if (info.ne.0) STOP "error computation eigenvalues and eigenvectors"

!  write(*,*) 'eig1: ', eig(1)
!  write(*,*) 'eig2: ', eig(2)
!  write(*,*) 'row1: ', identity(1,1), identity(1,2)
!  write(*,*) 'row2: ', identity(2,1), identity(2,2)

  !
  ! read data and write output files
  !
  endreading=.false.
  time  = pot%TDtime(1)
  itime = 0
  do while ( .not. endreading )

     !
     ! read potential at each time step
     !
     call pot%set(stderr, fpot, time, endfile1)

     !
     ! compute strain tensor for each cell of the grid
     !
     call p1%eval_strain(mu_lame, lambda_lame, pot%TDactual(1,:),&
		  pot%TDactual(2,:), strain)

	  !
	  ! compute principal stresses for each cell of the grid
	  !
	  stress = zero
	  do icell = 1,ncell

		  !
		  ! compute the trace of the strain tensor
		  !
		  trace = zero
		  do iloc = 1,dimdata
			  trace = trace + strain(iloc,iloc,icell)
		  end do
		  
		  !
		  ! compute the stress tensor
		  !
		  stress(:,:,icell) = two*mu_lame*strain(:,:,icell) + &
			  lambda_lame*trace*identity

		  !
		  ! compute eigenvalues and eigenvectors using lapack subroutine
		  ! the eigenvalues are given in ascending order
		  ! in output matrix columns are eigevectors associated to eigenvalues
		  ! the order of the columns is the same of the eigenvalues
		  !
		  call dsyev('V','U',dimdata,stress(:,:,icell),dimdata,eig,work,lwork,info)
		  if (info.ne.0) STOP "error computation eigenvalues and eigenvectors"

		  s_min%TDactual(1:dimdata,icell) = eig(1)*      stress(1:dimdata,1,      icell)
		  s_max%TDactual(1:dimdata,icell) = eig(dimdata)*stress(1:dimdata,dimdata,icell)

		  write(*,*) 'smin: ', sqrt(s_min%TDactual(1,icell)**2+s_min%TDactual(2,icell)**2), ' - smax: ', &
			     sqrt(s_max%TDactual(1,icell)**2+s_max%TDactual(2,icell)**2)
		  write(*,*) 'eig min: ', eig(1), ' - eig max: ', eig(dimdata)
	  end do

	  !
	  ! set current time
	  !
     s_min%time=time
     s_max%time=time
          
     !
     ! if both file are in steady state 
     !
     if ( pot%steadyTD ) then
        endreading=.True.
        s_min%steadyTD=.True.
        s_max%steadyTD=.True.
     end if

     !
     ! write output files
     ! 
	  call s_min%write2dat(file_stress_min%lun)
	  call s_max%write2dat(file_stress_max%lun)

     !
	  ! exit if file pot reached the end
     !
     if ( endfile1 ) then
        exit
     end if

     !
     ! next time step
     !
     time = pot%TDtime(2)
   end do

	!
	! write tail output files
	!
	call write2file(stderr,'tail',dimdata,ndata,s_min%TDactual,time,file_stress_min)
	call write2file(stderr,'tail',dimdata,ndata,s_max%TDactual,time,file_stress_max)

	!
	! deallocate strain, stress, identity
	!
   deallocate(strain,stress,identity,stat=res)
	if(res.ne.0) rc = IOerr(stderr, err_dealloc, 'main', &
            ' strain stress identity ',res)

	call pot%kill(stderr)
   call s_min%kill(stderr)
   call s_max%kill(stderr)

	call p1%kill(stderr)
	call grid%kill(stderr)
   call fgrid%kill(stderr)
	call fpot%kill(stderr)
	call file_stress_min%kill(stderr)
	call file_stress_max%kill(stderr)

end PROGRAM eval_principal_components

subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./eval_gradient.out <grid> <pot> <stress_min> <stress_max> <mu_lame> <lambda_lame>'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid        (in ) : triangulation in ascii'
  write(lun_err,*) ' pot         (in ) : displacement solution of Saint-venant equations'
  write(lun_err,*) ' stress_min  (out) : principal stress component associated to smallest eigenvalue'
  write(lun_err,*) ' stress_max  (out) : principal stress component associated to largest eigenvalue'
  write(lun_err,*) ' mu_lame     (in ) : first lame constant'
  write(lun_err,*) ' lambda_lame (in ) : second lame constant'  
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)

end subroutine err_handle
