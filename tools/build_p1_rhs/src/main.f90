!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> The main program 
!> 
!>
!> REVISION HISTORY:
!> 22 Lug 2016 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM subgrid_var
  use Globals
  use AbstractGeometry
  !use SparseMatrix
  use TimeInputs
  use TimeOutputs
  implicit none
  
  type(abs_simplex_mesh)     :: grid,subgrid
  type(TimeData) :: forcing,dirac,dirichlet,neumann
  type(TDOut)    :: forcing_subgrid,dirac_subgrid
  type(TDOut)    :: rhs_subgrid,rhs_grid,dirichlet_subgrid

  integer  :: i,j,inode,icell,count,nstr
  logical  :: rc
  integer  :: res
  ! input files
  character(len=1024) :: input
  character(len=1024) :: clean
  character(len=1024) :: grid_fname
  character(len=1024) :: forcing_fname
  character(len=1024) :: dirac_fname
  character(len=1024) :: dirichlet_fname
  character(len=1024) :: neumann_fname
  character(len=1024) :: output_folder_fname
  ! output files
  character(len=1024) :: subgrid_fname
  character(len=1024) :: parent_fname
  character(len=1024) :: dirac_subgrid_fname
  character(len=1024) :: forcing_subgrid_fname
  character(len=1024) :: dirichlet_subgrid_fname
  character(len=1024) :: rhs_subgrid_fname
  character(len=1024) :: rhs_grid_fname
  
  

  integer :: stderr,stdout,debug !,res
  integer :: flag_correction
  integer :: dimdata
  real(kind=double) :: imbalance_tolerance
  
  integer, allocatable  :: perm(:), iperm(:)
  
  real(kind=double), allocatable  :: boundary_flux(:,:)
  real(kind=double), allocatable  :: tdens(:)
  real(kind=double) :: time,imbalance

  logical :: steady
  logical :: end_reached
  logical :: end_files
  logical :: end_reached_forcing
  logical :: end_reached_dirac
  logical :: end_reached_dirichlet
  logical :: end_reached_neumann
  
  type(file) :: stiff
  !type(spmat) :: connection_matrix

  type(file) :: fgrid
  type(file) :: fforcing
  type(file) :: fdirac
  type(file) :: fdirichlet
  type(file) :: fneumann
  !
  type(file) :: foutput_folder
  type(file) :: fsubgrid
  type(file) :: fparent
  type(file) :: fforcing_subgrid
  type(file) :: fdirac_subgrid
  type(file) :: frhs_grid
  type(file) :: frhs_subgrid
  type(file) :: fdirichlet_subgrid

  stderr=0
  stdout=6

  count = command_argument_count()
  !
  ! read path for
  ! grid
  ! forcing
  ! dirac
  ! dirichlet
  ! outputfolder
  
  call get_command_argument(1,grid_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,1)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument subgrid wrong res= ',res)
  end if

  call get_command_argument(2,forcing_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,2)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument subgrid wrong res= ',res)
  end if

  call get_command_argument(3,dirac_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,3)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument subgrid wrong res= ',res)
  end if

  call get_command_argument(4,dirichlet_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,4)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument subgrid wrong res= ',res)
  end if

  call get_command_argument(5,neumann_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,5)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument subgrid wrong res= ',res)
  end if

  call get_command_argument(6,rhs_grid_fname, status=res)
  if ( res.ne.0) then
     call print_help_message(stderr,6)
     rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
          '  argument subgrid wrong res= ',res)
  end if

  if (count==7) then
     call get_command_argument(7,input, status=res)
     if ( res.ne.0) then
        read (input,*) imbalance_tolerance
        call print_help_message(stderr,7)
        rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
             '  argument subgrid wrong res= ',res)
     end if
  else
     imbalance_tolerance=1.0d-13
  end if
     
  if (count==8) then
     call get_command_argument(8,input, status=res)
     if ( res.ne.0) then
        read (input,*) flag_correction
        call print_help_message(stderr,8)
        rc = IOerr(stderr, err_inp , 'interpolate_timedata', &
             '  argument subgrid wrong res= ',res)
     end if
  else
     flag_correction=0
  end if

  !
  ! rhs_integrated_grid.dat
  ! subgrid.dat
  ! parent.dat
  ! rhs_integrated_subgrid.dat
  ! dirichlet_subgrid.dat
  ! forcing_subgrid.dat
  

  call fgrid%init(stderr,etb(grid_fname),10,'in')
  call fforcing%init(stderr,etb(forcing_fname),11,'in')
  call fdirac%init(stderr,etb(dirac_fname),12,'in')
  call fdirichlet%init(stderr,etb(dirichlet_fname),13,'in')
  call fneumann%init(stderr,etb(neumann_fname),14,'in')
  call frhs_grid%init(stderr,etb(rhs_grid_fname),21,'out')

  !
  ! init grid
  !
  write(*,*)'READING grid from ', etb(fgrid%fn)
  call grid%read_mesh(stderr,fgrid)
  write(*,*)'DONE'  
  

    
  
  !
  ! build sizecell for integration
  !
  call grid%build_size_cell(stderr)
  call grid%build_normal_cell(stderr)
  call grid%build_edge_connection(stderr)

  ! inputs defined on grid
  write(stdout,*) 'Forcing   - ',etb(fforcing%fn)
  call forcing%init(stderr, fforcing)
  write(stdout,*) 'Dirac     - ',etb(fdirac%fn)
  call dirac%init(stderr, fdirac)
  write(stdout,*) 'Dirichlet - ',etb(fdirichlet%fn)
  call dirichlet%init(stderr, fdirichlet)
  write(stdout,*) 'Neumann   - ',etb(fneumann%fn)
  call neumann%init(stderr, fneumann)

  dimdata=forcing%dimdata
  if (forcing%dimdata .eq. dirac%dimdata ) then
     write(*,*) 'Not consistent data'
  end if

  ! init output rhs_subgrid
  call rhs_grid%init(stderr, dimdata,grid%nnode)


  ! build
  end_files=.false.
  time=forcing%TDtime(1)
  steady=.false.
  write(frhs_grid%lun,*) dimdata,grid%nnode,' ! dim data' 
  do while ( (time .eq. forcing%TDtime(1) ) .or. &
       ( (.not. steady ) .and. ( .not. end_files  )  ) ) 
     ! read inputs
     call forcing%set(stderr, fforcing,time,end_reached_forcing)
     call dirac%set(stderr, fdirac,time,end_reached_dirac)
     call dirichlet%set(stderr, fdirichlet,time,end_reached_dirichlet)     
     call neumann%set(stderr, fneumann,time,end_reached_neumann)     
     end_files = ( &
          end_reached_forcing  .and. &
          end_reached_dirac    .and. &
          end_reached_dirichlet.and. &
			 end_reached_neumann)
     steady = ( &
          forcing%steadyTD  .and. &
          dirac%steadyTD    .and. &
          dirichlet%steadyTD.and. &
			 neumann%steadyTD)

     !
     ! assembly rhs grid
     !
     rhs_grid%time=time
     call assembly_rhs_grid_integrated(grid,&
          dimdata,&
          forcing%TDactual,&
          dirac%TDactual,  &
			 neumann%TDactual,&
          rhs_grid%TDactual)
     rhs_grid%steadyTD=steady

     !
     ! check imbalance
     !
     if ( dirichlet%nnonzeros == 0 ) then
		  write(stdout,*) 'CHECK IMBALANCE'
        do j=1,dimdata
           imbalance = sum(rhs_grid%TDactual(j,:))
           if (abs(imbalance) .gt. imbalance_tolerance) then
              if ( flag_correction == 0) then
                 write(*,*) 'Imbalance =', imbalance
                 rc = IOerr(stderr, err_inp, 'main', 'inbalanced inputs')
              else if ( flag_correction == 1) then
                 call balance(dimdata,grid%nnode,rhs_grid%TDactual(j,:))
              end if
           end if
        end do
     end if
     write(stdout,'(a,1pe8.2,a,1pe9.2,a,I6,a,I6,a,I6,a,I6)') &
          'time= ',time,' imbalance = ',imbalance, &
          ' NForcing   = ',forcing%nnonzeros,&
          ' NDirac     = ',dirac%nnonzeros,&
          ' NDirichlet = ',dirichlet%nnonzeros,&
          ' NNeumann   = ',neumann%nnonzeros
     !
     ! write rhs_subgrid_integrated
     !
     call rhs_grid%write2dat(frhs_grid%lun)

     ! next time
     time=min(forcing%TDtime(2),  &
              dirac%TDtime(2),    &
              dirichlet%TDtime(2),&
			     neumann%TDtime(2))
  end do

  call rhs_grid%kill(stderr)
  !call rhs_subgrid%kill(stderr)
    
  call forcing%kill(stderr)
  call dirac%kill(stderr)
  call dirichlet%kill(stderr)
  call neumann%kill(stderr)

  !call subgrid%kill(stderr)
  call grid%kill(stderr)

end PROGRAM subgrid_var

subroutine balance(dim,nrhs,rhs)
  use Globals
  implicit none
  integer,           intent(in ) :: dim
  integer,           intent(in ) :: nrhs
  real(kind=double), intent(inout) :: rhs(dim,nrhs)
  !local
  integer :: i,j
  real(kind=double) :: plus,minus,correction

  do j=1,dim
     plus=zero
     minus=zero
     do i=1,nrhs
        if ( rhs(1,i) > zero ) then
           plus=plus+rhs(j,i)
        else
           minus=minus+abs(rhs(j,i))
        end if
     end do
     
     correction=plus/minus
     do i=1,nrhs
        if ( rhs(j,i) < zero ) then
           rhs(j,i)=rhs(j,i)*correction
        end if
     end do
  end do
end subroutine balance

  
!>---------------------------------------------------------
!> Assembly rhs_subgrid of elliptic equation given forcing and
!> Dirac 
!>----------------------------------------------------------
subroutine assembly_rhs_grid_integrated(grid,&
     dim,forcing,dirac,neumann,rhs_grid_forcing)
    use Globals
    use AbstractGeometry
    implicit none
    type(abs_simplex_mesh),        intent(in ) :: grid
    integer,           intent(in ) :: dim
    real(kind=double), intent(in ) :: forcing(dim,grid%ncell)
    real(kind=double), intent(in ) :: dirac(dim,grid%nnode)
    real(kind=double), intent(in ) :: neumann(dim+grid%logical_dimension,grid%nedge)
    real(kind=double), intent(out) :: rhs_grid_forcing(dim,grid%nnode)

    !local 
    integer :: inode, icell, iedge, iloc,ifather,inode_parent,j, node1, node2
    real(kind=double) :: factor, size_edge

    !
    ! scal factor for integration
    !
    factor=one/grid%nnodeincell
    rhs_grid_forcing  = zero
    do icell = 1, grid%ncell
       do iloc = 1,grid%nnodeincell
          inode = grid%topol(iloc,icell)
          do j=1,dim
             rhs_grid_forcing(j,inode) =  rhs_grid_forcing(j,inode) + &
               factor * forcing(j,icell) * grid%size_cell(icell)
          end do
       end do
    end do
	 
	 !
	 ! add neumann contribution
	 !
	 do iedge = 1,grid%nedge
		 node1 = int(neumann(1,iedge))
		 node2 = int(neumann(2,iedge))
		 if ((node1+node2).ne.0) then
			 size_edge = sqrt((grid%coord(1,node1)-grid%coord(1,node2))**2 + &
									(grid%coord(2,node1)-grid%coord(2,node2))**2)
			 do j = 1,dim
				 rhs_grid_forcing(j,node1) = rhs_grid_forcing(j,node1) + &
					 onehalf * size_edge * neumann(2+j,iedge)
				 rhs_grid_forcing(j,node2) = rhs_grid_forcing(j,node2) + &
					 onehalf * size_edge * neumann(2+j,iedge)
			 end do
		 end if
	 end do

    !
    ! add dirac contribution
    !
    do inode = 1, grid%nnode
       do j=1,dim
          if ( abs(dirac(j,inode) ) .gt. small ) then
             rhs_grid_forcing(j,inode) =  dirac(j,inode)
          end if
       end do
    end do
    
  end subroutine assembly_rhs_grid_integrated


subroutine print_help_message(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./dmk_rhs_preprocess.out <grid> <forcing> <dirac> <dirichlet> <neumann> <rhs>]'
  write(lun_err,*) ' where '
  write(lun_err,*) ' grid                (in ) : triangulation in ascii'
  write(lun_err,*) ' forcing             (in ) : timedata to be converted defined on cell (dimdata=1, ndata=grid%ncell)'
  write(lun_err,*) ' dirac               (in ) : timedata to be converted defined on cell (dimdata=1, ndata=grid%ncell)'
  write(lun_err,*) ' dirichlet           (in ) : timedata to be converted defined on cell (dimdata=1, ndata=grid%ncell)'
  write(lun_err,*) ' neumann             (in ) : timedata to be converted defined on cell (dimdata=1, ndata=grid%ncell)'
  write(lun_err,*) ' rhs                 (out) : timedata with integrated rhs'
  write(lun_err,*) ' imbalance_tolerance (in,optional) : tolerance for rhs imbalance( used only if no dirichlet points are given)'
  write(lun_err,*) '                                   : Default=1e-13'
  write(lun_err,*) ' flag_corecction     (in,optional) : 0 ==no correction, 1==scale negative part to balance rhs'
  write(lun_err,*) '          (optional)'
  write(lun_err,*) '                                   : Default=0'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine print_help_message
