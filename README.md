# What is this repository for? #

* Quick summary
This repository contains the modules containg all varibles for the desciption of the Linear Galerkin approximation 
of fucntions an grids(2d, 3d and surface).
* Version 2.0

# How do I get set up? #

## Dependencies
  * PROGRAMS
    * GNU Fortran compiler compatible with FORTRAN 2003 (tested with gfortran v>=4.8).
    Default path is assumed to be "/usr/bin/gfortran"
    * Cmake ( version >= 2.8)  
  * EXTERNAL LIBRARIES
    * Blas library
    * Lapack library
  * EXTERNAL REPOSITORIES; clone the following MANDATORY repositories
      in the parent directory
```
git clone https://gitlab.com/enrico_facca/globals.git
git clone https://gitlab.com/enrico_facca/linear_algebra.git
git clone https://gitlab.com/enrico_facca/geometry.git
```

The results should be
```
.
├── geometry
├── globals
└── linear_algebra
```

# Compile #
## Compile pure Fortran sources

In Unix-base dsystem use the classical Cmake commands to compile
libraries and programs:
```
  mkdir build/;
  cd build;
  cmake ../; 
  make
```

Compiling options are:
* RELEASE (default)
* DEBUG
* IEEE
* USER
that can be invoked adding the cmake flag

```
cmake -DBUILD_TYPE="compiling option" .. ;  
```


## Troubleshooting ##

In case your gfortran compiler path is not "/usr/bin/gfortran" pass the absoulte path 

```
cmake -DMy_Fortran_Compiler="absolute path to gfortran" .. ;
```

In case of errors try to pass the path of the directories containg the
blas and lapack libraries using the cmake flag:

```
cmake -DBLAS_DIR="absolute path for blas library" -DLAPACK_DIR="absolute path for lapack library" ..; 
```

In order to diagnose the error sources use also 

```
cmake .. VERBOSE=1;
```

* Summary of set up 
* Configuration
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact